/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filter.authentication;


import com.model.hibernate.connect.HibernateUtil;
import com.model.hibernate.connect.exception.JWTExpiration;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Filter check User Permissions for all user.
 * @author Tran Ngoc Hai
 * @since 11/02/2017
 */
public class AdminrFilter implements Filter{
   
    private final String ENCODE = "8v4u9wrvhwa234cansfbr39wcrc9w";
   
    private final String ADMIN = "admin";
    
    @Override
     public void init(FilterConfig fConfig) throws ServletException {
         System.out.println("LogFilter init!");
     }

     @Override
     public void destroy() {
         System.out.println("LogFilter destroy!");
     }
     
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) sr;        
        try {
            if (HibernateUtil.checkJWT(req.getHeader("jwt"), ENCODE,
                    req.getHeader("username"), ADMIN)) {
                fc.doFilter(sr, sr1);
            } else{
                req.getRequestDispatcher("/error").forward(sr, sr1);
                
            }
        } catch (JWTExpiration ex) {
            req.getRequestDispatcher("/expired").forward(sr, sr1);
        }
    }
    
}
