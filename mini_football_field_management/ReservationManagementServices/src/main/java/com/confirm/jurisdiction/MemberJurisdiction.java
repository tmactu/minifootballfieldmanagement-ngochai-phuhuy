/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confirm.jurisdiction;

import com.exception.InvalidRequestException;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.connect.HibernateUtil;
import org.hibernate.Session;

/**
 * Function check member jurisdiction
 * @author Tran Ngoc Hai
 */
public class MemberJurisdiction {

    public static boolean check(String username, int idReservation) throws InvalidRequestException {
        //Session from Hibernate
        Session session = HibernateUtil.getSessionFactory().openSession();
        //Begin transaction
        session.beginTransaction();
        //Check the reservation is set by the member user if type of user is member.
        Member member = (Member) session.get(Member.class, username);
        if (member != null) {
            Reservation reservation = (Reservation) session.get(Reservation.class, idReservation);
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
            if (!reservation.getMember().getUsername().equals(username)) {
                throw new InvalidRequestException("Reservation is not by user!");
            }
        }
        return true;
    }
}
