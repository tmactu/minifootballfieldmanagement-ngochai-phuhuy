/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confirm.jurisdiction;

import com.exception.InvalidRequestException;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.HibernateUtil;
import org.hibernate.Session;

/**
 * Function check manager jurisdiction
 * @author Tran Ngoc Hai
 */
public class ManagerJurisdiction {

    public static boolean check(String username, int idReservation) throws InvalidRequestException {
        //Session from Hibernate
        Session session = HibernateUtil.getSessionFactory().openSession();
        //Begin transaction
        session.beginTransaction();
        User manager = (User) session.get(User.class, username);
        Reservation reservation = (Reservation) session.get(Reservation.class, idReservation);
        session.getTransaction().commit();
        if (manager.getClubs().getIdClub() != reservation.getFields().getClubs().getIdClub()) {
            throw new InvalidRequestException("Club is not managed by the user!");
        }
        return false;
    }
}
