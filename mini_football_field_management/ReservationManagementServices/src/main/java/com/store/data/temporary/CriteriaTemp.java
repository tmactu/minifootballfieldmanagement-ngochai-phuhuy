/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.store.data.temporary;

/**
 *
 * @author Hai
 */
public class CriteriaTemp {
    String typeOfField;
    long sum;

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    public void setTypeOfField(String typeOfField) {
        this.typeOfField = typeOfField;
    }

    public String getTypeOfField() {
        return typeOfField;
    }
    
}
