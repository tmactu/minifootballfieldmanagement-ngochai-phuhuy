package com.services.management.reservation.user;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.TypeOfField;
import com.services.management.reservation.rest.RestInterface;
import static com.services.management.reservation.rest.RestInterface.session;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tran Ngoc Hai
 * @since 01/03/2017
 */
@RestController
@RequestMapping("/member/findField")
public class MemberFindReservationServices implements RestInterface {

    @Override
    public String doGet(HttpServletRequest request) throws ParseException {
        //get ytpe of field from request.
        String typeOfField = request.getParameter("typeOfField");
        //Get and parse idclubto integer number.
        int idClub = Integer.parseInt(request.getHeader("club"));
        //Get and convert string date, time to Date value.
        Date date = new Date();
        Date startTime = new SimpleDateFormat("HH:mm:ss").parse(request.getParameter("startTimeValue"));
        Date endTime = new SimpleDateFormat("HH:mm:ss").parse(request.getParameter("endTimeValue"));

        //Json object contain result.
        JSONArray arr_resultList = new JSONArray();
        //Begin transaction
        session.beginTransaction();
        try {
            
            /*
            select * FROM fields WHERE 
            (`ID_CLUB` = 1) and (`TYPE_OF_FIELD` = '5x5') and 
            `ID_FIELD` not in
            (
                SELECT `ID_FIELD` FROM reservation 
                WHERE (`START_TIME` between '10:00:00' and '16:00:00') or 
                      (`END_TIME` between '10:00:00' and '16:00:00')
            )
            */
            
            DetachedCriteria subquery = DetachedCriteria.forClass(Reservation.class, "r")
                    .add(Restrictions.eq("r.date", date));             
            Criterion cr_startTime = Restrictions.between("r.startTime", startTime, endTime);
            Criterion cr_endTime = Restrictions.between("r.endTime", startTime, endTime);             
            LogicalExpression orExp = Restrictions.or(cr_startTime, cr_endTime);               
            subquery.add(orExp)
                    .setProjection(Projections.property("r.fields.idField"));
            
            Criteria criteria = session.createCriteria(Fields.class, "f")
                            .add(Restrictions.eq("f.clubs.idClub", idClub))
                            .add(Restrictions.eq("f.typeOfField.typeOfField", typeOfField))
                            .add(Subqueries.propertyNotIn("f.idField", subquery));
            
           
            Iterator<Fields> reservations = criteria.list().iterator();
            while (reservations.hasNext()) {                
                JSONObject ob = new JSONObject();
                Fields field = reservations.next();
                //set result into json
                ob.put("typeOfField", field.getTypeOfField().getTypeOfField()); 
                ob.put("price", field.getPrice());
                ob.put("idField", field.getIdField());               
                arr_resultList.put(ob);
            }
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
        }

        return arr_resultList.toString();
    }

}
