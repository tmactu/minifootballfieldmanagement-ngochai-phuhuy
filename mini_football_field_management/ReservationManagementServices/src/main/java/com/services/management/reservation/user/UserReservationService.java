package com.services.management.reservation.user;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.store.data.temporary.CriteriaTemp;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.services.management.reservation.rest.RestInterface;
import org.json.JSONArray;

/**
 *
 * @author Tran Ngoc Hai
 * @since 01/03/2017
 */
@RestController
@RequestMapping("/")
public class UserReservationService implements RestInterface {

    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, NumberFormatException {
        String idClub = request.getParameter("idClub");
        //Get and parse string id of club to integer value.
        int id = Integer.parseInt(idClub);
        //Create avairible contain result.
        JSONObject result = new JSONObject();
        
        //begin transaction
        session.getTransaction().begin();
        try {
            //Get sum of field number of club.
            Criteria criteria = session.createCriteria(Fields.class)
                    .add(Restrictions.eq("clubs", new Clubs(1)))
                    .add(Restrictions.eq("used", false))
                    .setProjection(Projections.projectionList()
                            .add(Projections.alias(Projections.rowCount(), "sum"))
                            .add(Projections.alias(Projections.groupProperty("typeOfField.typeOfField"), "typeOfField")
                            ))
                    .setResultTransformer(Transformers.aliasToBean(CriteriaTemp.class));
            Iterator<CriteriaTemp> list = criteria.list().iterator();
            
            while (list.hasNext()){                    
                CriteriaTemp temp = list.next();
                    result.append("typeOfField", temp.getTypeOfField());
                    result.append("sum", temp.getSum());
            }                 
        }
              
        finally {
            // Flush the associated Session and end the unit of work.
            session.getTransaction().commit();
        }
        return result.toString();
    }

}
