package com.services.management.reservation.user;

import com.confirm.jurisdiction.MemberJurisdiction;
import com.exception.InvalidRequestException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.HibernateUtil;
import com.services.management.reservation.rest.RestInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tran Ngoc Hai
 */
@RestController
@RequestMapping("/member")
public class MemberReservationServices implements RestInterface {

    /**
     * Service get information reservation of club.
     *
     * @param request: request contain id club value.
     * @return Json string contain total and used of field group by type of
     * fields.
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, NumberFormatException {
        String username = request.getParameter("username");              
                
        JSONArray result = new JSONArray();
        //begin transaction
        session.getTransaction().begin();
        try {
            //Get field of club by date, sort by end time.
            Criteria criteria = session.createCriteria(Reservation.class)
                    .add(Restrictions.eq("member", new Member(username)))
                    .add(Restrictions.ge("date", new Date()))
                    .addOrder(Order.asc("endTime"));
            Iterator<Reservation> reservations = criteria.list().iterator();
            //Get value from field list and set into json result
            while (reservations.hasNext()) {
                Reservation reservation = reservations.next();
                JSONObject temp = new JSONObject();
                temp.put("idReservation", reservation.getIdReservation());
                temp.put("field", reservation.getFields().getIdField());
                temp.put("typeOfField", reservation.getFields().getTypeOfField().getTypeOfField());
                temp.put("member", reservation.getMember().getUsername());
                temp.put("date", reservation.getDate());
                temp.put("startTime", reservation.getStartTime());
                temp.put("endTime", reservation.getEndTime());
                temp.put("rates", reservation.getRates());
                result.put(temp);
            }
        } finally {
            // Flush the associated Session and end the unit of work
            session.getTransaction().commit();
        }
        return result.toString();
    }

    /**
     * Service add new reservation of club.
     *
     * @param request: request contain information of field need set.
     * @return Json success string.
     * @throws java.text.ParseException
     */
    @Override
    public String doPost(HttpServletRequest request) throws HibernateException, NumberFormatException, ParseException {
        String club = request.getHeader("club");
        String username = request.getParameter("username");        
        String field = request.getParameter("field");
        String dateValue = request.getParameter("dateValue");
        String startTimeValue = request.getParameter("startTimeValue");
        String endTimeValue = request.getParameter("endTimeValue");
        //create new id of reservation table.
        int idReservation = HibernateUtil.createID(new Reservation(), "idReservation");
        //Parse idField to integer number.
        int idField = Integer.parseInt(field);
        int idClub = Integer.parseInt(club);
        //Convert string date, time to Date value.
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateValue);
        Date startTime = new SimpleDateFormat("HH:mm:ss").parse(startTimeValue);
        Date endTime = new SimpleDateFormat("HH:mm:ss").parse(endTimeValue);
        //Get price of field by id club and type of field.
        Fields fieldUsed = (Fields) session.get(Fields.class, idField);
        //Compute rates by use time.
        int rateByHour = (endTime.getHours() - startTime.getHours()) * fieldUsed.getPrice();
        float rateByMinute = (endTime.getMinutes() - startTime.getMinutes()) * fieldUsed.getPrice() / 60;
        int rates = rateByHour + (int) rateByMinute;
        //begin transaction
        session.beginTransaction();
        try {
            Query query = session.createSQLQuery("SELECT * FROM mini_football_field_management.reservation where \n" +
"	((START_TIME >= '" + startTime.getHours() + ":" + startTime.getMinutes()+ ":00"  + "' and START_TIME <= '" + endTime.getHours() + ":" + endTime.getMinutes()+ ":00" + "')"
        + " or (END_TIME >= '" + startTime.getHours() + ":" + startTime.getMinutes()+ ":00" + "' and END_TIME <= '" + endTime.getHours() + ":" + endTime.getMinutes() + ":00" + "'))\n" +
"		and ID_FIELD = " + idField + ";");
            List reservationTest = query.list();
            if(!reservationTest.isEmpty()) {
                return new JSONObject().put("error", "error").toString();
            }
            Member member = (Member) session.get(Member.class, username);
            int percent = member.getTypeOfMember().getPercentDiscount();
            rates = rates - (rates * percent / 100);
            //Create new reservation instance.
            Reservation reservation = new Reservation();
            reservation.setIdReservation(idReservation);
            reservation.setFields(new Fields(idField));
            reservation.setMember(new Member(username));
            reservation.setDate(date);
            reservation.setStartTime(startTime);
            reservation.setEndTime(endTime);
            reservation.setRates(rates);
            //save reservation
            session.save(reservation);
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
            session.clear();
        }
        return SUCCESS;
    }

    /**
     * Service delete reservation.
     *
     * @param body: list contain id reservation.
     * @param request: contain id reservation need delete.
     * @return success string.
     * @throws InvalidRequestException: when Reservation is not by user.
     */
    @Override
    public String doDelete(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
            throws HibernateException, NumberFormatException, InvalidRequestException {
        String username = request.getHeader("username");      
  
        //Get id reservation and type of user from body of request.
        int id = Integer.parseInt(body.getFirst("idReservation"));
        //Check the reservation is set by the member user if type of user is member.
        MemberJurisdiction.check(username, id);
        //Begin transaction
        session.beginTransaction();
        try {
            //Load reservation from database by id reservation.
            Reservation reservation = (Reservation) session.get(Reservation.class, id);
            //Delete reservation
            session.delete(reservation);
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
        }
        return SUCCESS;
    }

}
