package com.services.management.reservation.user;

import com.confirm.jurisdiction.ManagerJurisdiction;
import com.confirm.jurisdiction.MemberJurisdiction;
import com.exception.InvalidRequestException;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.User;
import com.services.management.reservation.rest.RestInterface;
import static com.services.management.reservation.rest.RestInterface.SUCCESS;
import static com.services.management.reservation.rest.RestInterface.session;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tran Ngoc Hai
 * @since 01/03/2007
 */
@RestController
@RequestMapping("/manager")
public class ManagerReservationService implements RestInterface {

    public Date createDate(Date current) {
        Date temp = new Date();
        temp.setHours(current.getHours());
        temp.setMinutes(current.getMinutes());
        temp.setSeconds(current.getSeconds());
        return temp;
    }

    public String checkDate(Date check) {
        Date current = new Date();
        if ((current.getTime() > check.getTime())) {
            return "used";
        }else{
            if(check.getTime() - current.getTime() < 3600000)
                return "waiting";
        }
        return "free";
    }
    
    public int createTimeline(Date start, Date end){
        return (end.getHours() - start.getHours()) * 3600 + (end.getMinutes() - start.getMinutes()) * 60;
    }

    /**
     * Service get information of reservation.
     *
     * @param request: contain username.
     * @return success string.
     * @throws java.text.ParseException
     */
    @Override
    public String doGet(HttpServletRequest request) throws ParseException, HibernateException {
        Date current = new Date();
        //Create json contain result.
        JSONArray returnValue = new JSONArray();
        //Begin transaction
        session.beginTransaction();
        try {
            User manager = (User) session.get(User.class, request.getHeader("username"));
            Criteria criteria = session.createCriteria(Reservation.class)
                    .createAlias("fields", "f")
                    .add(Restrictions.eq("f.clubs", manager.getClubs()))
                    .addOrder(Order.asc("f.idField"))
                    .addOrder(Order.asc("startTime"));
            Iterator<Reservation> reservations = criteria.list().iterator();
            Criteria cr = session.createCriteria(Fields.class)
                    .add(Restrictions.eq("clubs", manager.getClubs()));
            Iterator<Fields> fields = cr.list().iterator();
            ArrayList<Integer> distinctField = new ArrayList<>();
            while (reservations.hasNext()) {
                JSONObject result = new JSONObject();
                Reservation reservation = reservations.next();
                if (!distinctField.contains(reservation.getFields().getIdField())) {
                    String status = checkDate(createDate(reservation.getStartTime()));
                    result.put("status", status);
                    result.put("idReservation", reservation.getIdReservation());
                    result.put("fields", reservation.getFields().getIdField());
                    result.put("username", reservation.getMember().getUsername());
                    result.put("name", reservation.getMember().getUser().getName());
                    result.put("date", reservation.getDate());
                    result.put("startTime", reservation.getStartTime());
                    result.put("endTime", reservation.getEndTime());
                    result.put("rates", reservation.getRates());
                    if(status.equals("used")){
                       result.put("timeline", createTimeline(current, reservation.getEndTime()));
                    }else if(status.equals("waiting")){
                        result.put("timeline", createTimeline(current, reservation.getStartTime()));
                    }else{
                        result.put("timeline", 0);
                    }
                    distinctField.add(reservation.getFields().getIdField());
                    System.out.println(result.toString());
                    returnValue.put(result);
                }
            }
            while(fields.hasNext()){       
                Fields field = fields.next();
                if (!distinctField.contains(field.getIdField())) {
                    JSONObject result = new JSONObject();
                    result.put("status", "free");
                    result.put("idReservation", "");
                    result.put("fields", field.getIdField());
                    result.put("username", "");
                    result.put("name", "");
                    result.put("date", "");
                    result.put("startTime", "");
                    result.put("endTime", "");
                    result.put("rates", "");
                    result.put("timeline", 0);
                    System.out.println(result.toString());
                    returnValue.put(result);
                }
            }
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
        }
        return returnValue.toString();
    }

    /**
     * Service set true for received reservation.
     *
     * @param body: list contain id reservation.
     * @param request: contain username.
     * @return success string.
     * @throws InvalidRequestException: when Reservation is not by user.
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
            throws InvalidRequestException, HibernateException, NumberFormatException {
        String username = request.getHeader("username");
        //Get string id reservation from request and convert to integer value.
        int idReservation = Integer.parseInt(body.getFirst("idReservation"));
        //Check is manager of club.
        ManagerJurisdiction.check(username, idReservation);
        //Begin transaction
        session.beginTransaction();
        try {
            //Get information of reservation from database.
            Reservation reservation = (Reservation) session.get(Reservation.class, idReservation);
            //State transition of field.
            Fields field = (Fields) session.load(Fields.class, reservation.getFields().getIdField());
            field.setUsed(true);
            session.update(field);
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
        }
        return SUCCESS;
    }

    @Override
    public String doDelete(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
            throws HibernateException, NumberFormatException, InvalidRequestException {
        String username = request.getHeader("username");      
        System.out.println(body.getFirst("idReservation"));
        //Get id reservation and type of user from body of request.
        int id = Integer.parseInt(body.getFirst("idReservation"));
       //Check is manager of club.
        ManagerJurisdiction.check(username, id);
        //Begin transaction
        session.beginTransaction();
        try {
            //Load reservation from database by id reservation.
            Reservation reservation = (Reservation) session.get(Reservation.class, id);
            //Delete reservation
            session.delete(reservation);
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
        }
        return SUCCESS;
    }
}
