package com.services.management.reservation.user;

import com.confirm.jurisdiction.ManagerJurisdiction;
import com.exception.InvalidRequestException;
import com.model.hibernate.POJO.Bill;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.TypeOfField;
import com.model.hibernate.POJO.TypeOfMember;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.HibernateUtil;
import com.services.management.reservation.rest.RestInterface;
import static com.services.management.reservation.rest.RestInterface.session;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tran Ngoc Hai
 * @since 01/03/2017
 */
@RestController
@RequestMapping("/manager/pay")
public class ManagerPayReservationService implements RestInterface {

    @Override
    public String doGet(HttpServletRequest request) throws ParseException, HibernateException {
        int idField = Integer.parseInt(request.getParameter("idField"));
        //Create json contain result.
        JSONArray returnValue = new JSONArray();
        //Begin transaction
        session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Reservation.class)
                    .add(Restrictions.eq("fields", new Fields(idField)));
            Iterator<Reservation> reservations = criteria.list().iterator();
            while (reservations.hasNext()) {
                JSONObject result = new JSONObject();
                Reservation reservation = reservations.next();
                result.put("idReservation", reservation.getIdReservation());
                result.put("fields", reservation.getFields().getIdField());
                result.put("username", reservation.getMember().getUsername());
                result.put("name", reservation.getMember().getUser().getName());
                result.put("date", reservation.getDate());
                result.put("startTime", reservation.getStartTime());
                result.put("endTime", reservation.getEndTime());
                result.put("rates", reservation.getRates());
                System.out.println(result);
                returnValue.put(result);
            }
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
        }
        return returnValue.toString();
    }

    /**
     * Service pay reservation.
     *
     * @param request
     * @param body: list contain id reservation.
     * @return success string.
     * @throws InvalidRequestException: when Reservation is not by user.
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
            throws InvalidRequestException, NumberFormatException, HibernateException {
        String username = request.getHeader("username");
        int club = Integer.parseInt(request.getHeader("club"));
        //Get string id reservation from request and convert to integer value.
        int idReservation = Integer.parseInt(body.getFirst("idReservation"));
        //Create new id bill.
        int idBill = HibernateUtil.createID(new Bill(), "idBill");
        //Check is manager of club.
        ManagerJurisdiction.check(username, idReservation);
        //Begin transaction
        session.beginTransaction();
        try {
            //Get information of reservation from database.
            Reservation reservation = (Reservation) session.get(Reservation.class, idReservation);
            
            reservation.getMember().setTotalHours((int) ((reservation.getEndTime().getTime() - reservation.getStartTime().getTime())/3600000) + reservation.getMember().getTotalHours());
            reservation.getMember().setTotalPayments(reservation.getMember().getTotalPayments() + reservation.getRates());
            if(reservation.getMember().getTypeOfMember().getHourCondition() < reservation.getMember().getTotalHours()
                    || (reservation.getMember().getTypeOfMember().getPaymentCondition() < reservation.getMember().getTotalPayments())) {
                Criteria cri = session.createCriteria(TypeOfMember.class)
                        .add(Restrictions.eq("clubs", new Clubs(club)))
                        .add(Restrictions.gt("percentDiscount", reservation.getMember().getTypeOfMember().getPercentDiscount()))
                        .addOrder(Order.desc("percentDiscount"))
                        .setMaxResults(1);
                TypeOfMember type = (TypeOfMember) cri.uniqueResult();
                if(type != null){
                    reservation.getMember().setTypeOfMember(type);
                }
            }
            //Create and copy information from reservation to the new Bill.
            Bill bill = new Bill();
            bill.setIdBill(idBill);
            bill.setFields(reservation.getFields());
            bill.setMember(reservation.getMember());
            bill.setDate(reservation.getDate());
            bill.setStartTime(reservation.getStartTime());
            bill.setEndTime(reservation.getEndTime());
            bill.setTotalPay(reservation.getRates());
            
            Fields field = (Fields) session.get(Fields.class, reservation.getFields().getIdField());
            field.setUsed(false);
            session.saveOrUpdate(reservation);
            session.update(field);
            //Delete reservation 
            session.delete(reservation);
            //Save the new bill.
            session.save(bill);
        } finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
        }
        return SUCCESS;
    }

    @Override
    public String doDelete(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request) throws InvalidRequestException {
        //Get string id reservation from request and convert to integer value.
        int id = Integer.parseInt(request.getHeader("club"));       
        //Begin transaction
        session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Reservation.class)
                    .createAlias("fields", "f")
                    .add(Restrictions.eq("f.clubs",new Clubs(id)));
            Iterator<Reservation> reservations = criteria.list().iterator();
            while(reservations.hasNext()){
                Reservation rs = reservations.next();
                Date temp = new Date();
                temp.setHours(rs.getEndTime().getHours());
                temp.setMinutes(rs.getEndTime().getMinutes());
                temp.setSeconds(rs.getEndTime().getSeconds());
                
                Date temp1 = new Date();
                temp1.setHours(rs.getStartTime().getHours());
                temp1.setMinutes(rs.getStartTime().getMinutes());
                temp1.setSeconds(rs.getStartTime().getSeconds());
                if(!temp.equals(new Date())) {
                    session.delete(rs);
                }else if(temp.getTime() + 5000 < new Date().getTime()){
                    session.delete(rs);
                }else
                if(temp.getTime() > new Date().getTime() && temp1.getTime() < new Date().getTime()){
                    if(!rs.getFields().isUsed()) session.delete(rs);
                }
            }
            
            Criteria cri = session.createCriteria(Fields.class)
                    .add(Restrictions.eq("clubs", new Clubs(id)));
            Iterator<Fields> ir = cri.list().iterator();
            while(ir.hasNext()){
                Fields field = ir.next();
                if(field.isUsed()){
                    Criteria temp = session.createCriteria(Reservation.class)
                            .add(Restrictions.eq("fields", field))
                            .addOrder(Order.desc("endTime"))
                            .setMaxResults(1);
                    Reservation re = (Reservation) temp.uniqueResult();
					if(re != null){
						Date temp1 = new Date();
						temp1.setHours(re.getEndTime().getHours());
						temp1.setMinutes(re.getEndTime().getMinutes());
						temp1.setSeconds(re.getEndTime().getSeconds());
						if((temp1.getTime() < new Date().getTime())){
							field.setUsed(false);
						}
					}else {
                                            field.setUsed(false);
                                        }
                }
            }
        }finally {
            // Flush the associated Session and end the unit of work 
            session.getTransaction().commit();
            session.clear();
        }        
        return SUCCESS;
    }
    
    

}
