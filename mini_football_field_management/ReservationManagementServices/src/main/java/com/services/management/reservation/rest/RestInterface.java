package com.services.management.reservation.rest;

import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.connect.HibernateUtil;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Session;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Tran Ngoc Hai
 * @since 1/3/2017
 */
public interface RestInterface {
    //Session from Hibernate
    public final Session session = HibernateUtil.getSessionFactory().openSession();
    //String is returned when processing success.
    public final String SUCCESS = new JSONObject().append("result", "success").toString();
    /**
     * Get service.
     * @param request: request contain value.
     * @return Json string contain total and used of field.
     */
    @RequestMapping(method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    default public @ResponseBody String doGet(HttpServletRequest request) throws ParseException{
        throw new RequestNotFoundException();
    }
    
    /**
     * Post service.
     * @param request: request contain value.
     * @return Json string contain total and used of field.
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    default public @ResponseBody String doPost(HttpServletRequest request) throws ParseException{
        throw new RequestNotFoundException();
    }
    
    /**
     * Delete service.
     * @param body: list contain value.
     * @param request: request contain value.
     * @return success string.
     */
    @RequestMapping(method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    default public @ResponseBody
    String doDelete(@RequestBody MultiValueMap<String, String> body,
            HttpServletRequest request) throws InvalidRequestException{
        throw new RequestNotFoundException();

    }
    
    /**
     * Put service.
     * @param body: list contain id reservation.
     * @param request: request contain value.
     * @return success string.
     */
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    default public @ResponseBody
    String doPut(@RequestBody MultiValueMap<String, String> body,
            HttpServletRequest request) throws InvalidRequestException, NumberFormatException{
        throw new RequestNotFoundException();
    }
}
