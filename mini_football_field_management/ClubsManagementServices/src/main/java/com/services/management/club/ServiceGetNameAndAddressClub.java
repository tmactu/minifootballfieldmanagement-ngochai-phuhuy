/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.User;
import com.services.management.club.rest.restInterface;
import static com.services.management.club.rest.restInterface.session;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 */
@RestController
@RequestMapping(value = "/club/nameAndAdrressClub")
public class ServiceGetNameAndAddressClub implements restInterface{
    /**
     * Service get name and address in club
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of manager belong to club
     *                  - club: id club
     * @return an instance of java.lang.String, result return JSONArray with content is list
     * name and address in club
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, NumberFormatException,
                                                                                InvalidManagerException{
        
        //Get data intput
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        //Create JSONArray to return
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not
            User user = (User) session.get(User.class, username);
                if(user.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            Clubs club = (Clubs) session.get(Clubs.class, Integer.parseInt(idClub));
            
            result.put("name", club.getName());
            result.put("address", club.getAddress());
            
            
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return result.toString();
        
    }
}
