/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Bill;
import com.model.hibernate.POJO.User;
import com.services.management.club.rest.restInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 01-03-2017
 */
@RestController
@RequestMapping(value = "/club/statistical")
public class ServiceStatisticalClub implements restInterface{
    
    /**
     * Service statistical club quarterly or monthly basis
     *
     * @param request
     * Data input include:
     *              - jwt
     *              - username: username of manager belong to club
     *              - club: id club
     *              - startMonth
     *              - endMonth
     *              - year
     * @return an instance of java.lang.String result return JSONArray with content is list statistical
     * @throws java.text.ParseException
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doGet(HttpServletRequest request) throws  HibernateException, ParseException, 
                                                NumberFormatException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String startMonth = request.getParameter("startMonth");
        String endMonth = request.getParameter("endMonth");
        String year = request.getParameter("year");
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
        
        
        //Convert data input
        int int_endMonth = Integer.parseInt(endMonth);
        int int_year = Integer.parseInt(year);
        String endDay = null;
        
        //Get last day of endMonth
        if(int_endMonth == 1 || int_endMonth == 3 || int_endMonth == 5 || int_endMonth == 7 
                             || int_endMonth == 8 || int_endMonth == 10 || int_endMonth == 12){
            endDay = "31";
        }else if (int_endMonth == 2){
            if((int_year%400) == 0)
                endDay = "29";
            else
                endDay = "28";
        }else{
            endDay = "30";
        }
        
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/" + startMonth + "/" + year);
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(endDay + "/" + endMonth + "/" + year);
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not
            User user = (User) session.get(User.class, username);
                if(user.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            //--------- Start: statistical for type field 5x5 --------- //

            /*
                SELECT  b.`DATE`, f.`TYPE_OF_FIELD`, SUM(`TOTAL_PAY`) FROM bill b, fields fields 
                WHERE (b.ID_FIELD = f.ID_FIELD) AND (f.`ID_CLUB` = ...) 
                    AND (b.`DATE` BETWEEN '...' AND '...')
                GROUP BY b.`DATE`, f.`TYPE_OF_FIELD`
            */

            Criteria cr_billField = session.createCriteria(Bill.class, "b")
                                .createAlias("fields", "fields")

            //Get field with if bill is belong to id field and between start date and end date
            .add(Restrictions.eq("fields.clubs.idClub", Integer.parseInt(idClub)))
            .add(Restrictions.between("b.date", startDate, endDate))
            //Create projections
            .setProjection(Projections.projectionList()
                    // Using groupProperty() same GROUP BY in sql
                    .add(Projections.alias(Projections.groupProperty("b.date"),"date")) 
                    .add(Projections.alias(Projections.groupProperty("fields.typeOfField"),"typeOfField")) 
                    //Using alias() same AS in sql
                    .add(Projections.alias(Projections.sum("b.totalPay"),"sum")) 
                    //Cast data into Statistical
                    ).setResultTransformer(Transformers.aliasToBean(Statistical.class));
            
            List<Statistical> listBillField = cr_billField.list();
            for(int i = 0; i < listBillField.size() - 3; i++){
                JSONObject result = new JSONObject();
                Statistical statistical = listBillField.get(i);
                result.put("date", statistical.getDate());
                result.append("sum", listBillField.get(i).getSum());
                if(listBillField.get(i+1).getDate().equals(listBillField.get(i).getDate())){    
                    i++;
                    result.append("sum", listBillField.get(i).getSum());
                    
                }
                if(listBillField.get(i+1).getDate().equals(listBillField.get(i).getDate())){
                    i++;
                    result.append("sum", listBillField.get(i).getSum());
                    
                }                
                arr_listResult.put(result);     
            }
        }finally{
            //Close transaction
            session.getTransaction().commit();
        }        
        
        return arr_listResult.toString();
    }
}
