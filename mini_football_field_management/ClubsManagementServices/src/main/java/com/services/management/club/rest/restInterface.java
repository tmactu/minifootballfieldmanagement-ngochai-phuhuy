/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club.rest;

import com.exception.InvalidAdminException;
import com.exception.InvalidManagerException;
import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.connect.HibernateUtil;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Session;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Phu Huy
 */
public interface restInterface {
    
    //Session from Hibernate
    public final Session session = HibernateUtil.getSessionFactory().openSession();
    //String is returned when processing success.  
    public final JSONObject SUCCESS = new JSONObject().put("result", "success");
    /**
     * Get service.
     * @param request: request contain value.
     * @return Json string contain total and used of field.
     * @throws com.exception.InvalidRequestException
     * @throws java.text.ParseException
     * @throws com.exception.InvalidAdminException
     * @throws com.exception.InvalidManagerException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    default public @ResponseBody String doGet(HttpServletRequest request) throws InvalidRequestException, RequestNotFoundException,
                                InvalidAdminException, ParseException, InvalidManagerException{
        throw new RequestNotFoundException();
    }
    
    /**
     * Post service.
     * @param request: request contain value.
     * @return Json string contain total and used of field.
     * @throws java.text.ParseException
     * @throws com.exception.InvalidManagerException
     * @throws com.exception.InvalidRequestException
     * @throws com.exception.InvalidAdminException
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    default public @ResponseBody String doPost(HttpServletRequest request) throws ParseException, 
            RequestNotFoundException, InvalidRequestException, InvalidAdminException, InvalidManagerException{
        throw new RequestNotFoundException();
    }
    
    /**
     * Delete service.
     * @param body: list contain value.
     * @param request: request contain value.
     * @return success string.
     * @throws java.text.ParseException
     * @throws com.exception.InvalidAdminException
     * @throws com.exception.InvalidManagerException
     */
    @RequestMapping(method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    default public @ResponseBody String doDelete(@RequestBody MultiValueMap<String, String> body,
            HttpServletRequest request) throws ParseException, InvalidAdminException, RequestNotFoundException, InvalidManagerException{
        throw new RequestNotFoundException();

    }
    
    /**
     * Put service.
     * @param body: list contain id reservation.
     * @param request: request contain value.
     * @return success string.
     * @throws com.exception.InvalidRequestException
     * @throws java.text.ParseException
     * @throws com.exception.InvalidManagerException
     */
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    default public @ResponseBody String doPut(@RequestBody MultiValueMap<String, String> body,
            HttpServletRequest request) throws InvalidRequestException, ParseException, NumberFormatException, InvalidManagerException, RequestNotFoundException{
        throw new RequestNotFoundException();
    }
}
