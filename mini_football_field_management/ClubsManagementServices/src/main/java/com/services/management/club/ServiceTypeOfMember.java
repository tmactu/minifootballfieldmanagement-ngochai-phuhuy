/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.TypeOfMember;
import com.model.hibernate.POJO.User;
import com.services.management.club.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 */
@RestController
@RequestMapping(value = "/typeOfMember")
public class ServiceTypeOfMember implements restInterface{
    /**
     * Service get type of member in club
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of manager belong to club
     *                  - club: id club
     *                  - typeOfMember
     *                  - percentDiscount
     *                  - hourCondition
     *                  - paymentCondition
     * @return an instance of java.lang.String, result return JSONArray with content success.
     */
    @Override
    public String doPost(HttpServletRequest request) throws HibernateException, NumberFormatException,
                                                                                InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String typeOfMember = request.getParameter("typeOfMember");
        String percentDiscount = request.getParameter("percentDiscount");
        String hourCondition = request.getParameter("hourCondition");
        String paymentCondition = request.getParameter("paymentCondition");
        System.out.println(percentDiscount);
		System.out.println(hourCondition);
		System.out.println(paymentCondition);
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            TypeOfMember type = new TypeOfMember();
                type.setTypeOfMember(typeOfMember + "_" + Integer.parseInt(idClub)); 
                type.setPercentDiscount(Short.parseShort(percentDiscount));
                type.setClubs(new Clubs(Integer.parseInt(idClub)));
                type.setHourCondition(Integer.parseInt(hourCondition)); 
                type.setPaymentCondition(Integer.parseInt(paymentCondition));
            
            //Save object
            session.save(type);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }
    /**
     * Service update information type of member in club
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of manager belong to club
     *                  - club: id club
     * @param body
     * Data input include:
     *                  - percentDiscount
     *                  - hourCondition
     *                  - paymentCondition
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
           throws  HibernateException, NumberFormatException, InvalidManagerException{    
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String typeOfMember = body.getFirst("typeOfMember");
        String percentDiscount = body.getFirst("percentDiscount");
        String hourCondition = body.getFirst("hourCondition");
        String paymentCondition = body.getFirst("paymentCondition");
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            TypeOfMember type = (TypeOfMember) session.load(TypeOfMember.class, typeOfMember + "_" + Integer.parseInt(idClub));
                type.setPercentDiscount(Short.parseShort(percentDiscount));
                type.setHourCondition(Integer.parseInt(hourCondition)); 
                type.setPaymentCondition(Integer.parseInt(paymentCondition));
            
            //Update object
            session.update(type); 
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }
    /**
     * Service delete type of member in club
     *
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager belong to club
     *                    - club: id club
     * @param body
     * Data input include:
     *                    - typeOfMember
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doDelete(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
                        throws HibernateException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String typeOfMember = body.getFirst("typeOfMember");
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            TypeOfMember type  = (TypeOfMember) session.load(TypeOfMember.class, typeOfMember + "_" + Integer.parseInt(idClub));
            
            //Delete object
            session.delete(type); 
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }    
    /**
     * Service get type of member by club
     *
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager belong to club
     *                    - club: id club
     * @return an instance of java.lang.String, result return JSONArray with content is list 
     * type of member belong to this club
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doGet(HttpServletRequest request)
                  throws HibernateException, NumberFormatException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();

        //Begin transaction
        session.beginTransaction();
            
        try{
            //Checking username of manager which belong to club or not
            User manager = (User) session.get(User.class, username);
            if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                throw new InvalidManagerException("Account manager not belong to club");
            
            //Create criteria
            Criteria cr = session.createCriteria(TypeOfMember.class)
                    .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                    .addOrder(Order.asc("percentDiscount"));
            List list = cr.list();
            Iterator iterator = list.iterator();
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                TypeOfMember type = (TypeOfMember) iterator.next();
                String typeOfMember = type.getTypeOfMember();
                int index = typeOfMember.indexOf("_");           

                    result.put("typeOfMember",typeOfMember.substring(0, index));
                    result.put("percentDiscount", type.getPercentDiscount());
                    result.put("hourCondition", type.getHourCondition());
                    result.put("paymentCondition", type.getPaymentCondition());
                arr_listResult.put(result);
            }
        }
        finally{
            //Close transaction 
            session.getTransaction().commit();
        }
        
        return arr_listResult.toString();
    }
}    
