/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Provinces;
import com.model.hibernate.POJO.User;
import com.services.management.club.rest.restInterface;
import static com.services.management.club.rest.restInterface.session;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 13-03-2017
 */
@RestController
@RequestMapping(value = "/club/update")
public class ServiceUpdateClub implements restInterface{
    
    /**
     * Service update club
     *
     * @param request
     * Data input include:  
     *                  - jwt                
     *                  - username: username of manager belong to club                 
     *                  - club: id club               
     * @param body
     * Data input include:
     *                    - province
     *                    - address
     *                    - name
     * @return success string.
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
                    throws  HibernateException, NumberFormatException, InvalidManagerException{
        
       //Get data input
        String username = request.getHeader("username");
        String idClub = body.getFirst("idClub");
      
        int province = Integer.parseInt(body.getFirst("province"));
        String address = body.getFirst("address");
        String name = body.getFirst("name");
        
        //Begin transaction
        session.beginTransaction();
        
        try{          
            
            //Get data from Club table by idClub
            Clubs clubs = (Clubs) session.load(Clubs.class, Integer.parseInt(idClub));
            Provinces provinces = new Provinces(province);
                clubs.setProvinces(provinces);
                clubs.setAddress(address);
                clubs.setName(name);
                //Update object
                session.update(clubs);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }
    
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
        JSONArray arr_resultList = new JSONArray();
        
        
        session.beginTransaction();
        
        try{
            Criteria cr = session.createCriteria(Provinces.class); 
            List list = cr.list();
            Iterator<Provinces> iterator = list.iterator();
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                Provinces provinces = iterator.next();
                result.put("province", provinces.getProvince());
                arr_resultList.put(result);
            }
        }
        finally{
            session.getTransaction().commit();
        }
        return arr_resultList.toString();
    }
}
