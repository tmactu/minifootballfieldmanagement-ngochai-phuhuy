/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.services.management.club.rest.restInterface;
import static com.services.management.club.rest.restInterface.session;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 14-04-2017
 */
@RestController
@RequestMapping(value = "/club/countEachTypeFieldOfClub")
public class ServiceCountEachTypeFieldOfClub implements restInterface{
    /**
     * Service count each type field of club with id club
     *
     * @param request
     * @return an instance of java.lang.String, result return JSONObject with content is count each type field in this club
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
        
        String idClub = request.getParameter("club");
        
        //Create JSONObject object to return
        JSONArray arr_listResult = new JSONArray();
        JSONObject result = new JSONObject();
        JSONObject resultField5x5 = new JSONObject();
        JSONObject resultField7x7 = new JSONObject();
        JSONObject resultField11x11 = new JSONObject();
        
        
        
        long countField5x5 = 0;
        long countField7x7 = 0;
        long countField11x11 = 0;
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            
            Criteria criteriaField5x5 = session.createCriteria(Fields.class)
                        .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                        .add(Restrictions.eq("typeOfField.typeOfField", "5x5"))
                        .setProjection(Projections.rowCount());
                List listField5x5 = criteriaField5x5.list();
                Iterator<Fields> iteratorField5x5 = listField5x5.iterator();
                
                if(iteratorField5x5.hasNext()){
                    //Count field 5x5
                    long l_countField5x5 = (long) listField5x5.get(0);
                    countField5x5 = countField5x5 + l_countField5x5; 
                }
            
            Criteria criteriaField7x7 = session.createCriteria(Fields.class)
                        .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                        .add(Restrictions.eq("typeOfField.typeOfField", "7x7"))
                        .setProjection(Projections.rowCount());
                List listField7x7 = criteriaField7x7.list();
                Iterator<Fields> iteratorField7x7 = listField7x7.iterator();
                
                if(iteratorField7x7.hasNext()){
                    //Count field 7x7
                    long l_countField7x7 = (long) listField7x7.get(0);
                    countField7x7 = countField7x7 + l_countField7x7; 
                }
                
            Criteria criteriaField11x11 = session.createCriteria(Fields.class)
                        .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                        .add(Restrictions.eq("typeOfField.typeOfField", "5x5"))
                        .setProjection(Projections.rowCount());
                List listField11x11 = criteriaField11x11.list();
                Iterator<Fields> iteratorField11x11 = listField11x11.iterator();
                
                if(iteratorField11x11.hasNext()){
                    //Count field 11x11
                    long l_countField11x11 = (long) listField11x11.get(0);
                    countField11x11 = countField11x11 + l_countField11x11; 
                }      
                resultField5x5.put("label", "Sân 5x5");
                resultField5x5.put("countField", countField5x5);
                arr_listResult.put(resultField5x5);
                resultField5x5.put("label", "Sân 5x5");
                resultField5x5.put("countField", countField5x5);
                arr_listResult.put(resultField5x5);
                resultField5x5.put("label", "Sân 5x5");
                resultField5x5.put("countField", countField5x5);
                arr_listResult.put(resultField5x5);
//                result.put("dataField", arr_listResult);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return result.toString();
    }
}
