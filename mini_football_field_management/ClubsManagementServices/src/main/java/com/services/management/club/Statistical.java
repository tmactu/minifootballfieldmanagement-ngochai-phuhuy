/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.model.hibernate.POJO.TypeOfField;
import java.util.Date;

/**
 * Use for function statisticalClub in clubServices  
 * @author Phu Huy
 * @Since: 25-02-2017
 */
public class Statistical {
    Date date;
    TypeOfField typeOfField;
    Long sum;

    public Statistical() {
    }

    public Date getDate() {
        return date;
    }

    public Long getSum() {
        return sum;
    }

    public TypeOfField getTypeOfField() {
        return typeOfField;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public void setTypeOfField(TypeOfField typeOfField) {
        this.typeOfField = typeOfField;
    }

    public Statistical(Date date, TypeOfField typeOfField, Long sum) {
        this.date = date;
        this.typeOfField = typeOfField;
        this.sum = sum;
    }

    
    
}
