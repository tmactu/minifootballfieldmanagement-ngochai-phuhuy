/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.User;
import com.services.management.club.rest.restInterface;
import static com.services.management.club.rest.restInterface.session;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 13-03-2017
 */
@RestController
@RequestMapping(value = "/club/getSumOfField")
public class ServiceGetSumOfField implements restInterface {
    
    /**
     * Service get sum of field with each type of field in club
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of manager belong to club
     *                  - club: id club
     * @return an instance of java.lang.String, result return JSONArray with content is list
     * sum of field in club
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, NumberFormatException,
                                                                                InvalidManagerException{
        
        //Get data intput
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not
            User user = (User) session.get(User.class, username);
                if(user.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            Criteria cr_5x5 = session.createCriteria(Fields.class)
                     .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                     .add(Restrictions.eq("typeOfField.typeOfField", "5x5"))
                     .setProjection(Projections.rowCount());
            List list_5x5 = cr_5x5.list();
                result.put("s5x5", list_5x5.get(0));
            Criteria cr_7x7 = session.createCriteria(Fields.class)
                     .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                     .add(Restrictions.eq("typeOfField.typeOfField", "7x7"))
                     .setProjection(Projections.rowCount());
            List list_7x7 = cr_7x7.list();
                result.put("s7x7", list_7x7.get(0));
            Criteria cr_11x11 = session.createCriteria(Fields.class)
                     .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                     .add(Restrictions.eq("typeOfField.typeOfField", "11x11"))
                     .setProjection(Projections.rowCount());
            List list_11x11 = cr_11x11.list();
                result.put("s11x11", list_11x11.get(0));
            
            arr_listResult.put(result);
        }
        finally{
            session.getTransaction().commit();
        }
        
        return arr_listResult.toString();
    }
}
