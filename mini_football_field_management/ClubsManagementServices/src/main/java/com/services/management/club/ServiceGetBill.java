/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.POJO.Bill;
import com.model.hibernate.POJO.User;
import com.services.management.club.rest.restInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 01-03-2017
 */
@RestController
@RequestMapping(value = "/club/getBill")
public class ServiceGetBill implements restInterface{
    
    /**
     * Service get information of bill belong to club
     *
     * @param request   
     * @return an instance of java.lang.String result return is information bill 
     * @throws com.exception.InvalidManagerException
     * @throws java.text.ParseException
     * @throws com.exception.InvalidRequestException
     */
    @Override
    public String doGet(HttpServletRequest request)
                       throws HibernateException, ParseException, NumberFormatException,
                            InvalidManagerException, InvalidRequestException, RequestNotFoundException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String idBill = request.getParameter("idBill");
        
        //Create JSON to return 
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            User user = (User) session.get(User.class, username);
                if(user.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            //Create criteria
            Bill bill = (Bill) session.get(Bill.class, Integer.parseInt(idBill)); 
                        
            result.append("dateOfInvoice", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
            result.append("name", bill.getMember().getUser().getName());
            result.append("typeOfField", bill.getFields().getTypeOfField().getTypeOfField());
            result.append("Date", bill.getDate().toString());
            result.append("startTime", bill.getStartTime().toString());
            result.append("endTime", bill.getEndTime().toString());
            result.append("totalPay", bill.getTotalPay());
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        } 
        
        return result.toString();     
    }
}
