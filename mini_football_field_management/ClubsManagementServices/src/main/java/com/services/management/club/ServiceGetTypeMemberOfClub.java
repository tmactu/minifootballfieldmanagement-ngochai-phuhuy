/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.TypeOfMember;
import com.model.hibernate.POJO.User;
import com.services.management.club.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 18-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/club/GetTypeMemberOfClub")
public class ServiceGetTypeMemberOfClub implements restInterface{
    /**
     * Service get type of member by club
     *
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager belong to club
     *                    - club: id club
     * @return an instance of java.lang.String, result return JSONArray with content is list 
     * type of member belong to this club
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doGet(HttpServletRequest request)
                  throws HibernateException, NumberFormatException{
        
        //Get data input
        String idClub = request.getParameter("club");
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
        JSONObject result = new JSONObject();

        //Begin transaction
        session.beginTransaction();
            
        try{            
            Criteria cr_club = session.createCriteria(Clubs.class)
                    .add(Restrictions.eq("idClub", Integer.parseInt(idClub)));
            List list_club = cr_club.list();
            Iterator iterator_club = list_club.iterator();
            Clubs club = (Clubs) iterator_club.next();
                    result.put("name", club.getName()); 
                    result.put("address", club.getAddress());
            
            //Create criteria
            Criteria cr_type = session.createCriteria(TypeOfMember.class)
                    .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                    .addOrder(Order.asc("percentDiscount"));
            
            List list_type = cr_type.list();
            Iterator iterator_type = list_type.iterator();
            
            while(iterator_type.hasNext()){
                JSONObject ob = new JSONObject();
                TypeOfMember typeOfMember = (TypeOfMember) iterator_type.next();
                String type = typeOfMember.getTypeOfMember();
                int index = type.indexOf("_");
                    ob.put("type", type.substring(0, index)); 
                    ob.put("percent", typeOfMember.getPercentDiscount());
                arr_listResult.put(ob);
            }
                    result.put("typeMember", arr_listResult);
        }
        finally{
            //Close transaction 
            session.getTransaction().commit();
        }
        
        return result.toString();
    }
}
