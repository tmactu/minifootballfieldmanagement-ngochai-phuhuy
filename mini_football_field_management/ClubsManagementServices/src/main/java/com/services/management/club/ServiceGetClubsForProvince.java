/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.model.hibernate.POJO.Clubs;
import com.services.management.club.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 11-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/club/getAllClubForProvince")
public class ServiceGetClubsForProvince implements restInterface{
    
    /**
     * Service get all club belong to province
     *
     * @param request
     * @return an instance of java.lang.String, result return JSONArray with content is id and name of all club
     */
    
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
        
        String province = request.getParameter("province");
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
        
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Create criteria
            Criteria cr = session.createCriteria(Clubs.class)
                    .add(Restrictions.eq("provinces.idProvince", Integer.parseInt(province)));
            
            List list = cr.list();      
            Iterator<Clubs> iterator = list.iterator();
            
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                Clubs club = iterator.next();
                result.put("name", club.getName());
                result.put("id", club.getIdClub());
                arr_listResult.put(result);
            }
            
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return arr_listResult.toString();
    }
}
