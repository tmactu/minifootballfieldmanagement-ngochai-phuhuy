/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.model.hibernate.POJO.Clubs;
import com.services.management.club.rest.restInterface;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 10-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/club/countClub")
public class ServiceCountAllClub implements restInterface{
    
    /**
     * Service count all club 
     *
     * @param request
     * @return an instance of java.lang.String, result return JSONObject with content is number all club
     */
    
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
      
        //Create JSONArray to return
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Create criteria
            Criteria cr = session.createCriteria(Clubs.class)
                    .setProjection(Projections.rowCount());
            
            List list = cr.list();            
            result.put("count", list.get(0));                   
            
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return result.toString();
    }
}
