/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.TypeOfField;
import com.services.management.club.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 21-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/club/getTypeFieldOfClub")
public class ServiceGetTypeFieldOfClub implements restInterface{
    /**
     * Service get type field of this club
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - club: id club
     * @return an instance of java.lang.String, result return JSONArray with content is list
     * type of field in club
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, NumberFormatException {
        
        int idClub = Integer.parseInt(request.getParameter("club"));
        
        JSONArray arr_resultList = new JSONArray();
        
        //Begin tracsaction
        session.beginTransaction();
        
        try{
            //Create criteria
            Criteria cr = session.createCriteria(Fields.class)
                   .add(Restrictions.eq("clubs.idClub", idClub))
                   .setProjection(Projections.alias(Projections.distinct(Projections.property("typeOfField.typeOfField")), "type"))
                   .setResultTransformer(Transformers.aliasToBean(TypeFieldOfClub.class)); 
            
            List list = cr.list();
            Iterator<TypeFieldOfClub> iterator = list.iterator();
            
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                TypeFieldOfClub typeOfField = iterator.next();
                if(typeOfField.getType().equals("5x5")){
                    result.put("typeContent", "Sân loại 10 người");
                    result.put("typeField", typeOfField.getType()); 
                    result.put("flagField", true);
                    arr_resultList.put(result);
                }
                if(typeOfField.getType().equals("7x7")){
                    result.put("typeContent", "Sân loại 14 người");
                    result.put("typeField", typeOfField.getType()); 
                    result.put("flagField", true);
                    arr_resultList.put(result);                 
                }
                if(typeOfField.getType().equals("11x11")){
                    result.put("typeContent", "Sân loại 22 người");
                    result.put("typeField", typeOfField.getType()); 
                    result.put("flagField", true);
                    arr_resultList.put(result);                   
                }
            }         
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        return arr_resultList.toString();
    }
}
