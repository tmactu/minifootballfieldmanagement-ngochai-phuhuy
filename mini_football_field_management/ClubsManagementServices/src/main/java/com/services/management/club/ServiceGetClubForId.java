/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.services.management.club.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 13-04-2017
 */
@RestController
@RequestMapping("/club/getClubForId")
public class ServiceGetClubForId implements restInterface{
    /**
     * Service get information club with id club
     *
     * @param request
     * @return an instance of java.lang.String, result return JSONObject with content is information this club
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
        
        String idClub = request.getParameter("club");
        
        //Create object JSON to return
        JSONObject result = new JSONObject();
        int price5x5 = 0;
        int price7x7 = 0;
        int price11x11 = 0;
        
        long countField5x5 = 0;
        long countField7x7 = 0;
        long countField11x11 = 0;
        
        //Create JSONObject object to for dataChart
        JSONArray arr_listResult = new JSONArray();
        JSONObject dataField = new JSONObject();
        JSONObject resultField5x5 = new JSONObject();
        JSONObject resultField7x7 = new JSONObject();
        JSONObject resultField11x11 = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();  
        
        try{
            Clubs clubs = (Clubs) session.get(Clubs.class, Integer.parseInt(idClub)); 
            result.put("name", clubs.getName());
            result.put("address", clubs.getAddress());
            
    //--            Field 5x5

                // Get price field 5x5
                Criteria criteriaPrice5x5 = session.createCriteria(Fields.class)
                        .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                        .add(Restrictions.eq("typeOfField.typeOfField", "5x5"));
                List listPrice5x5 = criteriaPrice5x5.list();
                Iterator<Fields> iteratorPrice5x5 = listPrice5x5.iterator();
                
                if(iteratorPrice5x5.hasNext()){
                    Fields field5x5 = iteratorPrice5x5.next();
                    price5x5 = price5x5 + field5x5.getPrice();
                                   
                    // Count number field 5x5 using
                    Criteria citeria5x5_Use = session.createCriteria(Fields.class)
                            .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                            .add(Restrictions.eq("typeOfField.typeOfField", "5x5"))
                            .add(Restrictions.eq("used", true))
                            .setProjection(Projections.rowCount());
                    List list_5x5_Use = citeria5x5_Use.list(); 

                    //Count fied 5x5
                    long countField5x5Use = (long) list_5x5_Use.get(0);
                    countField5x5 = countField5x5 + countField5x5Use;
              
                    // Count number field 5x5 not using
                    Criteria citeria5x5_NotUse = session.createCriteria(Fields.class)
                            .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                            .add(Restrictions.eq("typeOfField.typeOfField", "5x5"))
                            .add(Restrictions.eq("used", false))
                            .setProjection(Projections.rowCount());
                    List list_5x5_NotUse = citeria5x5_NotUse.list();

                    //Count fied 5x5
                    long countField5x5NotUse = (long) list_5x5_NotUse.get(0);
                    countField5x5 = countField5x5 + countField5x5NotUse;
                }
                result.put("price5x5", price5x5);
                
    //--            Field7x7

                // Get price field 7x7
                Criteria criteriaPrice7x7 = session.createCriteria(Fields.class)
                        .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                        .add(Restrictions.eq("typeOfField.typeOfField", "7x7"));
                List listPrice7x7 = criteriaPrice7x7.list();
                Iterator<Fields> iteratorPrice7x7 = listPrice7x7.iterator();
                
                if(iteratorPrice7x7.hasNext()){
                    Fields field7x7 = iteratorPrice7x7.next();
                    price7x7 = price7x7 + field7x7.getPrice();

                    // Count number field 7x7 using
                    Criteria citeria7x7_Use = session.createCriteria(Fields.class)
                            .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                            .add(Restrictions.eq("typeOfField.typeOfField", "7x7"))
                            .add(Restrictions.eq("used", true))
                            .setProjection(Projections.rowCount());
                    List list_7x7_Use = citeria7x7_Use.list();

                    //Count fied 7x7
                    long countField7x7Use = (long) list_7x7_Use.get(0);
                    countField7x7 = countField7x7 + countField7x7Use;

                    // Count number field 7x7 not using
                    Criteria citeria7x7_NotUse = session.createCriteria(Fields.class)
                            .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                            .add(Restrictions.eq("typeOfField.typeOfField", "7x7"))
                            .add(Restrictions.eq("used", false))
                            .setProjection(Projections.rowCount());
                    List list_7x7_NotUse = citeria7x7_NotUse.list();

                    //Count fied 7x7
                    long countField7x7NotUse = (long) list_7x7_NotUse.get(0);
                    countField7x7 = countField7x7 + countField7x7NotUse;
                    
                }
                result.put("price7x7", price7x7);

    //--            Field11x11

                // Get price field 5x5
                Criteria criteriaPrice11x11 = session.createCriteria(Fields.class)
                        .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                        .add(Restrictions.eq("typeOfField.typeOfField", "11x11"));
                List listPrice11x11 = criteriaPrice11x11.list();
                Iterator<Fields> iteratorPrice11x11 = listPrice11x11.iterator();
                
                if(iteratorPrice11x11.hasNext()){
                    
                    Fields field11x11 = iteratorPrice11x11.next();
                    price11x11 = price11x11 + field11x11.getPrice();

                    // Count number field 11x11 using
                    Criteria citeria11x11_Use = session.createCriteria(Fields.class)
                            .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                            .add(Restrictions.eq("typeOfField.typeOfField", "11x11"))
                            .add(Restrictions.eq("used", true))
                            .setProjection(Projections.rowCount());
                    List list_11x11_Use = citeria11x11_Use.list();
                    
                    //Count fied 11x11
                    long countField11x11Use = (long) list_11x11_Use.get(0);
                    countField11x11 = countField11x11 + countField11x11Use;

                    // Count number field 11x11 not using
                    Criteria citeria11x11_NotUse = session.createCriteria(Fields.class)
                            .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                            .add(Restrictions.eq("typeOfField.typeOfField", "11x11"))
                            .add(Restrictions.eq("used", false))
                            .setProjection(Projections.rowCount());
                    List list_11x11_NotUse = citeria11x11_NotUse.list();

                    //Count fied 11x11
                    long countField11x11NotUse = (long) list_11x11_NotUse.get(0);
                    countField11x11 = countField11x11 + countField11x11NotUse;
                }
                result.put("price11x11", price11x11);
                
                resultField5x5.put("label", "Sân 5x5");
                resultField5x5.put("countField", countField5x5);
                arr_listResult.put(resultField5x5);
                resultField7x7.put("label", "Sân 7x7");
                resultField7x7.put("countField", countField7x7);
                arr_listResult.put(resultField7x7);
                resultField11x11.put("label", "Sân 11x11");
                resultField11x11.put("countField", countField11x11);
                arr_listResult.put(resultField11x11);
                result.put("dataField", arr_listResult);
                
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return result.toString();
    }
            
}
