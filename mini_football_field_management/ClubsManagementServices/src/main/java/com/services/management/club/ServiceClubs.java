/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.club;

import com.exception.InvalidAdminException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Employee;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Provinces;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.HibernateUtil;
import com.services.management.club.rest.restInterface;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 23-02-2017
 */
@RestController
@RequestMapping(value = "/club")
public class ServiceClubs implements restInterface{
    
    /**
     * Service create new club, this services for admin
     *
     * @param request
     * * Data input include:
     *              - jwt
     *              - username: username of admin
     *              - province
     *              - address
     *              - name
     * @return an instance of java.lang.String, return JSONArray result with content success.
     */
    @Override
    public String doPost (HttpServletRequest request)
                       throws HibernateException, NumberFormatException, InvalidAdminException{        
        
        int province = Integer.parseInt(request.getParameter("province"));
        String address = request.getParameter("address");
        String name = request.getParameter("name");
        
        //Create ID club
        int idClub = HibernateUtil.createID(new Clubs(), "idClub");
        JSONObject result = new JSONObject();
        //Begin transaction
        session.beginTransaction();
        Clubs club = (Clubs) session.get(Clubs.class, idClub);
        try{
            //Create object Club to set information data for Club
            
            if(club == null){
                club = new Clubs();
                club.setIdClub(idClub);
                club.setProvinces(new Provinces(province));
                club.setAddress(address);
                club.setName(name);
            }           
            //Save object
            session.save(club);
            
            result.put("result", club.getIdClub());
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
			session.clear();
        }
        
        return club.getIdClub() + ""; 
    }
    
    /**
     * Service find club, this services for admin
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of admin
     *                  - parameter: id club or address which want to find
     * @return an instance of java.lang.String, return JSONArray result with content club information
     * information need to find
     */
    @Override
    public String doGet (HttpServletRequest request)
                          throws HibernateException, InvalidAdminException{
        
        //Get data input
        String username = request.getHeader("username");
        
        String parameter = request.getParameter("parameter");       
        
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
                
        //Begin transaction
        session.beginTransaction();
        
        try{
//            //Checking account is belong to admin or not
//            User admin = (User) session.get(User.class, username);
//                if(!admin.getTypeOfUser().equals("admin"))
//                    throw new InvalidAdminException("Must use account admin to do this");
//          //Create object JSON to return
            JSONObject result = new JSONObject();  
            int idClub = Integer.parseInt(parameter);
            
            if(idClub == 0){
                result.put("result", "can not find");
            }
            else{
                // Create Criteria have function filter data in Clubs
                Criteria cr = session.createCriteria(Clubs.class);

                Criterion cr_idClub = Restrictions.eq("idClub", idClub);

                cr.add(cr_idClub);

                List<Clubs> listClub = (List<Clubs>) cr.list();

                Iterator<Clubs> iterator = (Iterator<Clubs>) listClub.iterator();

                //Get each user information with username was found
                
                Clubs clubs = iterator.next();
                    result.put("idClub", clubs.getIdClub());
                    result.put("province", clubs.getProvinces().getProvince());
                    result.put("provinceID", clubs.getProvinces().getIdProvince());
                    result.put("address", clubs.getAddress());
                    result.put("name", clubs.getName());
            }  
            
            arr_listResult.put(result);
        }
        catch(NumberFormatException e){
                if(parameter != ""){
			Base64.Decoder dec= Base64.getDecoder();
			byte[] strdec=dec.decode(parameter); 
                    try {
                        parameter = new String(strdec , "UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(ServiceClubs.class.getName()).log(Level.SEVERE, null, ex);
                    }
		}
                // Create Criteria have function filter data in Clubs
                Criteria cr = session.createCriteria(Clubs.class);
                Criterion cr_idClub1 = Restrictions.like("name", "%" + parameter + "%");
                cr.add(cr_idClub1);

                Iterator<Clubs> listClub = cr.list().iterator();

                while(listClub.hasNext()){
                    
                    //Create object JSON to return
                    JSONObject result = new JSONObject();
                    //Get each user information with username was found
                    Clubs clubs = listClub.next();
                    Criteria cr1 = session.createCriteria(User.class)
                            .add(Restrictions.eq("typeOfUser", "manager"))
                            .add(Restrictions.eq("clubs", clubs))
                            .setMaxResults(1);
                    List<User> user = cr1.list();
                        result.put("idClub", clubs.getIdClub());
                        result.put("province", clubs.getProvinces().getProvince());
                        result.put("provinceID", clubs.getProvinces().getIdProvince());
                        result.put("address", clubs.getAddress());
                        result.put("name", clubs.getName());    
                        result.put("manager", user.get(0).getUsername());
                        result.put("managerName", user.get(0).getName());
                        arr_listResult.put(result);
                } 
        }finally{
            //Close transaction 
            session.getTransaction().commit();
        }    
        return arr_listResult.toString();
    }
    /**
     * Service delete club, this services for admin
     *
     * @param body
     * Data input include:
     *                  - idClub
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of admin
     * @return an instance of java.lang.String, return JSONArray result with content success
     */
    @Override
    public String doDelete (@RequestBody MultiValueMap<String, String> body, 
                             HttpServletRequest request)throws HibernateException, 
                                            NumberFormatException, InvalidAdminException{
                
        //Convert data input.Get data in List MultiValueMap
        String username = request.getHeader("username");
        
        String idClub = body.getFirst("idClub");
                
        //Begin session
        session.beginTransaction();
        
        try{
            
            //Get data from Club table by idClub
            Clubs club = (Clubs) session.get(Clubs.class, Integer.parseInt(idClub)); 
            Criteria cri = session.createCriteria(User.class)
                    .add(Restrictions.eq("typeOfUser", "member"));
            Iterator<User> user = cri.list().iterator();
            while(user.hasNext()){
                Member username1 = user.next().getMember();
                if(username1 != null){
                    Member member = (Member)session.get(Member.class, username1.getUsername());
                    if(member != null) {
                        session.delete(member);                    
                    }   
                }
            }
            session.flush();
            Criteria cri1 = session.createCriteria(User.class)
                    .add(Restrictions.eq("typeOfUser", "employee"));
            Iterator<User> user1 = cri1.list().iterator();       
            while(user1.hasNext()){
                Employee username1 = user1.next().getEmployee();
                if(username1 != null){
                    Employee member = (Employee)session.get(Employee.class, username1.getUsername());
                    if(member != null) {
                        session.delete(member);                    
                    }   
                }               
            }
            session.flush();
                //---- Delete data Club ----//
            session.delete(club);                 
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }
}
