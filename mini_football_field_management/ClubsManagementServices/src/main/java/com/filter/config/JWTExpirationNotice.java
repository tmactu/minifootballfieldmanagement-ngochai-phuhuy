/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filter.config;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Hai
 */

@Path("/reset")
public class JWTExpirationNotice {
    
    @GET
    @POST
    @PUT
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public String resetGet() {
        return "JWT Expiration";
    }
}
