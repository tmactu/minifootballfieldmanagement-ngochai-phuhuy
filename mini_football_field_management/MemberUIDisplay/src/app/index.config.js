(function ()
{
    'use strict';

    angular
        .module('fuse')
        .config(config);

    /** @ngInject */
    function config($httpProvider) {  //$httpProvider
      // Put your custom configurations here

      var cookieStore;
      angular.injector(['ngCookies']).invoke(['$cookieStore', function(_$cookieStore_) {
        cookieStore = _$cookieStore_;
      }]);
      $httpProvider.defaults.headers.common = {
        'jwt': cookieStore.get('jwt'),
        'username': cookieStore.get('username'),
        'club': cookieStore.get('club'),
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      $.ajaxSetup({
        beforeSend(xhr){
          xhr.setRequestHeader('jwt', cookieStore.get('jwt'));
          xhr.setRequestHeader('username', cookieStore.get('username'));
          xhr.setRequestHeader('club', cookieStore.get('club'));
        }
      });
    }
})();
