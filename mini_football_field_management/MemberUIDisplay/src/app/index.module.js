(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [

            // Core
            'app.core',

            // Toolbar
            'app.toolbar',

            // Apps
            'app.dashboards'



        ]);
})();
