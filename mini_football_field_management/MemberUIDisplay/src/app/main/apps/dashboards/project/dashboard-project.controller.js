(function ()
{
    'use strict';

    angular
        .module('app.dashboards.project')
        .controller('DashboardProjectController', DashboardProjectController);

    /** @ngInject */
    function DashboardProjectController($scope, $interval, $mdDialog, $mdSidenav, $cookieStore, DashboardData)
    {
        var vm = this;

        // Data
        vm.dashboardData = DashboardData;
        vm.projects = vm.dashboardData.projects;

        vm.selectedProject = vm.projects[0];

        //CONTACT
        vm.dataContact = vm.dashboardData.dataContact;

        // Widget 1
        vm.widget1 = vm.dashboardData.widget1;

        // Widget 2
        vm.widget2 = vm.dashboardData.widget2;

        // Widget 3
        vm.widget3 = vm.dashboardData.widget3;

        // Widget 4
        vm.widget4 = vm.dashboardData.widget4;

        // Widget 5
        vm.widget5 = {
            title       : vm.dashboardData.widget5.title,
            nameClub    : vm.dashboardData.widget5.nameClub,
            address     : vm.dashboardData.widget5.address,
            content     : vm.dashboardData.widget5.content,
            typeMember  : vm.dashboardData.widget5.typeMember,
            typeContent : vm.dashboardData.widget5.typeContent
        };

        // Widget 6
        vm.widget6 = {
            title       : vm.dashboardData.widget6.title,
            refresh  : vm.dashboardData.widget6.refresh,
            datePost : vm.dashboardData.widget6.datePost,
            showContent      : vm.dashboardData.widget6.showContent,
            newNews : vm.dashboardData.widget6.newNews,
            oldNews      : vm.dashboardData.widget6.oldNews

        };

        // Widget 7
        vm.widget7 = {
            title       : vm.dashboardData.widget7.title,
            content      : vm.dashboardData.widget7.content,
            chooseField    : vm.dashboardData.widget7.chooseField,
            timeStart       : vm.dashboardData.widget7.timeStart,
            timeEnd      : vm.dashboardData.widget7.timeEnd,
            findField    : vm.dashboardData.widget7.findField,
            fieldList       : vm.dashboardData.widget7.fieldList,
            field      : vm.dashboardData.widget7.field,
            rentField    : vm.dashboardData.widget7.rentField
        };

      // Widget 8
      vm.widget8 = vm.dashboardData.widget8;

      // Widget 9
      vm.widget9 = vm.dashboardData.widget9;

      // Widget 4
      vm.widget10 = vm.dashboardData.widget10;

        // Now widget
        vm.nowWidget = {
            now   : {
                second: '',
                minute: '',
                hour  : '',
                day   : '',
                month : '',
                year  : ''
            },
            ticker: function ()
            {
                var now = moment();
                vm.nowWidget.now = {
                    second : now.format('ss'),
                    minute : now.format('mm'),
                    hour   : now.format('HH'),
                    day    : now.format('D'),
                    weekDay: now.format('dddd'),
                    month  : now.format('MMMM'),
                    year   : now.format('YYYY')
                };
            }
        };

        // Weather widget
        vm.weatherWidget = vm.dashboardData.weatherWidget;

        // Methods
        vm.toggleSidenav = toggleSidenav;
        vm.selectProject = selectProject;

        //////////
        vm.selectedProject = vm.projects[0];

        // Now widget ticker
        vm.nowWidget.ticker();

        var nowWidgetTicker = $interval(vm.nowWidget.ticker, 1000);

        $scope.$on('$destroy', function ()
        {
            $interval.cancel(nowWidgetTicker);
        });

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

        /**
         * Select project
         */
        function selectProject(project)
        {
            vm.selectedProject = project;
        }


        // Check account
        //Data

        //Method
        vm.checkMember = checkMember;

        function checkMember() {
          $.ajax({
            type: 'GET',
            url: 'http://localhost:8010/account/checkMember',
            contentType : 'application/x-www-form-urlencoded',
            success:function (result) {
              $.ajax({
                type : 'GET',
                url: "http://localhost:8010/member/getMember",
                contentType : 'application/x-www-form-urlencoded',
                success: function(result){
                  vm.dataResult = result;
                  vm.memberUsername = vm.dataResult.username;
                  vm.memberIdClub = vm.dataResult.club;
                  vm.dataResult.birthdate = new Date(vm.dataResult.birthdate);
                  vm.gender = vm.dataResult.gender;
                  vm.dataMember = {
                    typeOfMember: vm.dataResult.typeOfMember,
                    totalHour: vm.dataResult.totalHour,
                    totalPayment: vm.dataResult.totalPayment
                  }
                  vm.dataUpdate={
                    username: vm.memberUsername,
                    name: vm.dataResult.name,
                    birthdate: vm.dataResult.birthdate,
                    id: vm.dataResult.id,
                    gender: vm.dataResult.gender,
                    address: vm.dataResult.address,
                    email: vm.dataResult.email,
                    numberPhone: vm.dataResult.numberPhone
                  };
                  //Get notification
                  $.ajax({
                    type : 'GET',
                    url: "http://localhost:8016/notification/getNotification?club=" + vm.memberIdClub,
                    contentType : 'application/x-www-form-urlencoded',
                    success: function(result){
                      vm.dataNotification = result;

                      vm.allCount = vm.dataNotification.allCount;
                      vm.countFirst = vm.dataNotification.countFirst;
                      vm.countLast = vm.dataNotification.countLast;
                      vm.disabledNew = vm.dataNotification.disabledNew;
                      vm.disabledOld = vm.dataNotification.disabledOld;
                      vm.disabledPrevious = vm.dataNotification.disabledPrevious;
                      vm.listNotification = vm.dataNotification.listNotification;
                      vm.listNotification.startDate = new Date(vm.listNotification.startDate);
                      vm.listNotification.startDate = vm.listNotification.startDate.getDate() + '-' + vm.listNotification.startDate.getMonth() + '-' + vm.listNotification.startDate.getFullYear();
                    },
                    error:function (error) {
                      alert(error);
                    }});
                  //Get empty field
                  $.ajax({
                    type : 'GET',
                    url: "http://localhost:8012/?idClub=" + vm.memberIdClub,
                    contentType : 'application/x-www-form-urlencoded',
                    success: function(result){
                      vm.dataEmptyField = angular.fromJson(result);
                      vm.field5x5 = {
                        sum: vm.dataEmptyField.sum[1],
                        typeOfField: "SÂN"
                      }
                      vm.field7x7 = {
                        sum: vm.dataEmptyField.sum[2],
                        typeOfField: "SÂN"
                      }
                      vm.field11x11 = {
                        sum: vm.dataEmptyField.sum[0],
                        typeOfField: "SÂN"
                      }
                    },
                    error:function (error) {
                      alert(error);
                    }});
                  //Get information club
                  $.ajax({
                    type : 'GET',
                    url: "http://localhost:8015/club/GetTypeMemberOfClub?club=" + vm.memberIdClub,
                    contentType : 'application/x-www-form-urlencoded',
                    success: function(result){
                      vm.dataTypeOfMember = result;
                      vm.typeMember = vm.dataTypeOfMember.typeMember;

                    },
                    error:function (error) {
                      alert(error);
                    }});
                },
                error:function (error) {
                  alert(error);
                }
              });
            },
            error:function (error) {
              $cookieStore.remove('jwt');
              $cookieStore.remove('username');
              $cookieStore.remove('club');
              window.location = "http://localhost:8000/main";
            }
          });
        }

        //Main page
        // Data

        // Method
          vm.GetTypeMemberOfClub = GetTypeMemberOfClub;
          vm.getClubForId = getClubForId;
          vm.getFieldEmpty = getFieldEmpty;
          vm.getNotification = getNotification;
          vm.getOldNotification = getOldNotification;
          vm.getNewNotification = getNewNotification;
          vm.dialogNew = dialogNew;

        //Get information of club
        function GetTypeMemberOfClub(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8015/club/GetTypeMemberOfClub?club=" + vm.memberIdClub,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.dataTypeOfMember = result;
              vm.typeMember = vm.dataTypeOfMember.typeMember;

            },
            error:function (error) {
              alert(error);
            }});
        }

        //Get price field
        function getClubForId(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8015/club/getClubForId?club=" + vm.memberIdClub,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.infomationClub = result;
            },
            error:function (error) {
              alert(error);
            }});
        };

        function getNotification(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8080/notification/getNotification?club=" + vm.memberIdClub,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.dataNotification = result;

              vm.allCount = vm.dataNotification.allCount;
              vm.countFirst = vm.dataNotification.countFirst;
              vm.countLast = vm.dataNotification.countLast;
              vm.disabledNew = vm.dataNotification.disabledNew;
              vm.disabledOld = vm.dataNotification.disabledOld;
              vm.disabledPrevious = vm.dataNotification.disabledPrevious;
              vm.listNotification = vm.dataNotification.listNotification;
              vm.listNotification.startDate = new Date(vm.listNotification.startDate);
              vm.listNotification.startDate = vm.listNotification.startDate.getDate() + '-' + vm.listNotification.startDate.getMonth() + '-' + vm.listNotification.startDate.getFullYear();
            },
            error:function (error) {
              alert(error);
            }});
        }

        function getNewNotification(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8080/notification/getNewNotification?club=" + vm.memberIdClub + "&allCount=" + vm.allCount + "&countFirst=" + vm.countFirst,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.dataNotification = result;

              vm.allCount = vm.dataNotification.allCount;
              vm.countFirst = vm.dataNotification.countFirst;
              vm.countLast = vm.dataNotification.countLast;
              vm.disabledNew = vm.dataNotification.disabledNew;
              vm.disabledOld = vm.dataNotification.disabledOld;
              vm.listNotification = vm.dataNotification.listNotification;
              vm.listNotification.startDate = new Date(vm.listNotification.startDate);
              vm.listNotification.startDate = vm.listNotification.startDate.getDate() + '-' + vm.listNotification.startDate.getMonth() + '-' + vm.listNotification.startDate.getFullYear();
            },
            error:function (error) {
              alert(error);
            }});
        }
        function getOldNotification(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8080/notification/getOldNotification?club=" + vm.memberIdClub + "&allCount=" + vm.allCount + "&countLast=" + vm.countLast,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.dataNotification = result;

              vm.allCount = vm.dataNotification.allCount;
              vm.countFirst = vm.dataNotification.countFirst;
              vm.countLast = vm.dataNotification.countLast;
              vm.disabledNew = vm.dataNotification.disabledNew;
              vm.disabledOld = vm.dataNotification.disabledOld;
              vm.disabledPrevious = vm.dataNotification.disabledPrevious;
              vm.listNotification = vm.dataNotification.listNotification;
              vm.listNotification.startDate = new Date(vm.listNotification.startDate);
              vm.listNotification.startDate = vm.listNotification.startDate.getDate() + '-' + vm.listNotification.startDate.getMonth() + '-' + vm.listNotification.startDate.getFullYear();
            },
            error:function (error) {
              alert(error);
            }});
        }

        function getFieldEmpty(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8012/?idClub=" + vm.memberIdClub,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.dataEmptyField = angular.fromJson(result);
              vm.field5x5 = {
                  sum: vm.dataEmptyField.sum[0],
                  typeOfField: "SÂN"
              }
              vm.field7x7 = {
                sum: vm.dataEmptyField.sum[1],
                typeOfField: "SÂN"
              }
              vm.field11x11 = {
                sum: vm.dataEmptyField.sum[2],
                typeOfField: "SÂN"
              }
            },
            error:function (error) {
              alert(error);
            }});
        }

        function dialogNew(ev, dataTitle, dataContent){
          $mdDialog.show({
            controller         : function ($scope, $mdDialog)
            {
              $scope.dataDialog = {
                dialogTitle: dataTitle,
                dialogContent: dataContent,
                dialogClose: "ĐÓNG"
              };
              $scope.closeDialog = function ()
              {
                $mdDialog.hide();
              }
            },
            templateUrl           : 'app/main/apps/dashboards/project/main_page/dialog/dialogNews.html',
            parent             : angular.element('body'),
            targetEvent        : ev,
            clickOutsideToClose: false
          })
        }



      // Information
      //Data
      vm.disableName = true;
      vm.dataInformation = vm.dashboardData.dataInformation;
      vm.basicForm = {};
      vm.dataResult = {};
      vm.idClub = '';
      vm.provinces = {};
      vm.dataProvince = '1';
      vm.clubs = {};
      vm.infomationClub = {};

      //Method
      vm.updateMember = updateMember;
      vm.changeClub = changeClub;
      vm.GetTypeMemberOfMember = GetTypeMemberOfMember;
      vm.cancelUpdateMember = cancelUpdateMember;
      vm.getProvince = getProvince;
      vm.dialogUpdateSuccess = dialogUpdateSuccess;

      function GetTypeMemberOfMember(){
        $.ajax({
          type : 'GET',
          url: "http://localhost:8010/member/GetTypeMemberOfMember?username=" + vm.memberUsername,
          contentType : 'application/x-www-form-urlencoded',
          success: function(result){
            vm.dataMember = result;
          },
          error:function (error) {
            alert(error);
          }});
      }

      function updateMember(){
        var date = new Date(vm.dataUpdate.birthdate);
        vm.dataUpdate.birthdate = date.getFullYear() + '-' + (date.getMonth() + 1)+ '-' + date.getDate();
        // You can do an API call here to send the form to your server
        $.ajax({
          type : 'PUT',
          url: "http://localhost:8010/member/update",
          contentType : 'application/x-www-form-urlencoded',
          data: vm.dataUpdate,
          success: function(result){
          },
          error:function (error) {
            alert(error);
          }});
      }
      function dialogUpdateSuccess(ev){
        $mdDialog.show({
          controller         : function ($scope, $mdDialog)
          {
            $scope.dataDialog = {
              content: "Bạn đã thay đổi thông tin thành công!!",
              dialogClose: "ĐÓNG"
            };
            $scope.closeDialog = function ()
            {
              $mdDialog.hide();
            }
          },
          templateUrl           :  'app/main/apps/dashboards/project/information/dialog/dialogUpdateMemberSuccess.html',
          parent             : angular.element('body'),
          targetEvent        : ev,
          clickOutsideToClose: false
        })
      }

      function cancelUpdateMember(){
        vm.dataUpdate={
          username: vm.memberUsername,
          name: vm.dataResult.name,
          birthdate: vm.dataResult.birthdate,
          id: vm.dataResult.id,
          gender: vm.dataResult.gender,
          address: vm.dataResult.address,
          email: vm.dataResult.email,
          numberPhone: vm.dataResult.numberPhone
        };
      }

      //Use get all province in database
      function getProvince(){
        $.ajax({
          type : 'GET',
          url: "http://localhost:8013/rest/provinces",
          contentType : 'application/x-www-form-urlencoded',
          success: function(result){
            vm.provinces = result;
          },
          error:function (error) {
            alert(error);
          }});
      }
      //Watch what province selected and show clubs belong to this province
      $scope.$watch("vm.dataProvince", function(Value){
        $.ajax({
          type : 'GET',
          url: "http://localhost:8015/club/getAllClubForProvince?province=" + Value,
          contentType : 'application/x-www-form-urlencoded',
          success: function(result){
            vm.clubs = result;
          },
          error:function (error) {
            alert(error);
          }});
      })

      function changeClub(ev){
        vm.dataChange = {
          username: vm.memberUsername,
          idClub: vm.idClub
        }
        $mdDialog.show({
          controller         : function ($scope, $mdDialog, $cookieStore)
          {
            $scope.dataDialog = {
              dialogQuestion: "Bạn có chắc là muốn thay đổi câu lạc bộ không?",
              dialogAgree: "ĐỒNG Ý",
              dialogCancel: "HỦY"
            };
            $scope.closeDialog = function ()
            {
              $mdDialog.hide();
            }
            $scope.changeClub = function(){
              $.ajax({
                type : 'PUT',
                url: "http://localhost:8010/member/changeClub",
                contentType : 'application/x-www-form-urlencoded',
                data: vm.dataChange,
                success: function(result){
                  $cookieStore.remove('jwt');
                  $cookieStore.remove('username');
                  $cookieStore.remove('club');
                  window.location = "http://localhost:8000/main";
                },
                error:function (error) {
                  alert(error);
                }});
            }
            $scope.dialogSuccess = function(ev){
              $mdDialog.show({
                controller         : function ($scope, $mdDialog)
                {
                  $scope.dataDialog = {
                    content: "Bạn đã thay đổi câu lạc bộ thành công!!",
                    dialogClose: "ĐÓNG"
                  };
                  $scope.closeDialog = function ()
                  {
                    $mdDialog.hide();
                  }
                },
                templateUrl           :  'app/main/apps/dashboards/project/information/dialog/dialogChangeClubSuccess.html',
                parent             : angular.element('body'),
                targetEvent        : ev,
                clickOutsideToClose: false
              })
            }
          },
          templateUrl           :  'app/main/apps/dashboards/project/information/dialog/dialogChangeClubQuestion.html',
          parent             : angular.element('body'),
          targetEvent        : ev,
          clickOutsideToClose: false
        })
      }


      //Rent football
      //Data
      vm.dataFieldInput = {};
      vm.Time = {
        hour: [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
      };
      vm.flagField5x5 = false;
      vm.flagField7x7 = false;
      vm.flagField11x11 =false;
      vm.flagErrorRentFootball = true;
      vm.flagFieldList = true;

      //Method
      vm.getReservation = getReservation;
      vm.getTypeFieldOfClub = getTypeFieldOfClub;
      vm.reservationField = reservationField;
      vm.dialogReservationFieldSuccess = dialogReservationFieldSuccess;
      vm.checkInformationRentFootball = checkInformationRentFootball;


      function getReservation(){
        $.ajax({
          type : 'GET',
          url: "http://localhost:8012/member?username=" + vm.memberUsername,
          contentType : 'application/x-www-form-urlencoded',
          success: function(result){
            vm.dataBooking = angular.fromJson(result);
          },
          error:function (error) {
            alert(error);
          }});
      }

      //Get type field club has
      function getTypeFieldOfClub(){
        $.ajax({
          type : 'GET',
          url: "http://localhost:8015/club/getTypeFieldOfClub?club=" + vm.memberIdClub,
          contentType : 'application/x-www-form-urlencoded',
          success: function(result){
            vm.dataTypeField = result;
          },
          error:function (error) {
            alert(error);
          }});
      }

      function reservationField(idField){
        var dateReservation = new Date();
        vm.dataReservation = {
          dateValue : dateReservation.getDate() + '/' + (dateReservation.getMonth() + 1) + '/' + dateReservation.getFullYear(),
          username : $cookieStore.get('username'),
          startTimeValue : vm.dataFieldInput.startHour + ':' + vm.dataFieldInput.startMinute + ":00",
          endTimeValue : vm.dataFieldInput.endHour + ':' + vm.dataFieldInput.endMinute + ":00",
          field : idField
        }
          $.ajax({
            type: 'POST',
            url: 'http://localhost:8012/member',
            contentType: 'application/x-www-form-urlencoded',
            data: vm.dataReservation,
            success: function(result){
            },
            error: function(error){
              alert(error);
            }
          })

      }

      function dialogReservationFieldSuccess(ev){
        $mdDialog.show({
          controller         : function ($scope, $mdDialog)
          {
            $scope.dataDialog = {
              content: "Bạn đã đặt sân thành công!!",
              dialogClose: "ĐÓNG"
            };
            $scope.closeDialog = function ()
            {
              $mdDialog.hide();
            }
          },
          templateUrl           :  'app/main/apps/dashboards/project/rent_football/dialog/dialogReservationSuccess.html',
          parent             : angular.element('body'),
          targetEvent        : ev,
          clickOutsideToClose: false
        })
      }

      //Get list field empty in club
      function checkInformationRentFootball(type, startHour, startMinute, endHour, endMinute){
        if((type == undefined) || (startHour == undefined) || (startMinute == undefined) || (endHour == undefined) || (endMinute == undefined)){
          vm.flagErrorRentFootball = false;
          vm.flagFieldList = false
          vm.errorRentFootball = "Vui lòng nhập đầy đủ thông tin!!";
        }
        else if((startHour - endHour) >= 0){
          vm.flagErrorRentFootball = false;
          vm.flagFieldList = true
          vm.errorRentFootball = "Nhập thời gian không đúng."
        }
        else if( ((startMinute == endMinute) && ((endHour - startHour) < 3)) || ((startMinute != endMinute) && ((endHour - startHour) < 2)) ){
          vm.flagErrorRentFootball = true;
          vm.flagFieldList = false;
          vm.FieldOfClub = {};
          vm.startTime = vm.dataFieldInput.startHour + ':' + vm.dataFieldInput.startMinute + ":00";
          vm.endTime = vm.dataFieldInput.endHour + ':' + vm.dataFieldInput.endMinute + ":00";
          $.ajax({
            type:'GET',
            url: "http://localhost:8012/member/findField?typeOfField=" + vm.dataFieldInput.typeField + '&startTimeValue=' + vm.startTime + '&endTimeValue=' + vm.endTime,
            contentType: 'application/x-www-form-urlencoded',
            success: function(result){
              vm.FieldOfClub = angular.fromJson(result);
            },
            error:function (error) {
              alert(error);
            }});
        }
        else{
          vm.flagErrorRentFootball = false;
          vm.flagFieldList = true
          vm.errorRentFootball = "Không thể đặt quá 2 tiếng."
        }
      }


      //Booking schedule
      //Data

      //Method
      vm.dialogCancelReservation = dialogCancelReservation;

      function dialogCancelReservation(ev,Value){
        vm.dataCancel = {
          idReservation : Value
        }
        $mdDialog.show({
          controller         : function ($scope, $mdDialog)
          {
            $scope.dataDialog = {
              dialogQuestion: "Bạn có thực sự muốn hủy đặt sân này?",
              dialogAgree: "ĐỒNG Ý",
              dialogCancel: "KHÔNG"
            };

            $scope.closeDialog = function (){
              $mdDialog.hide();
            };

            $scope.cancelReservation = function(){
              $.ajax({
                type: 'DELETE',
                url: 'http://localhost:8012/member',
                contentType: 'application/x-www-form-urlencoded',
                data: vm.dataCancel,
                success: function(result){
                  $.ajax({
                    type : 'GET',
                    url: "http://localhost:8012/member?username=" + vm.memberUsername,
                    contentType : 'application/x-www-form-urlencoded',
                    success: function(result){
                      vm.dataBooking = angular.fromJson(result);
                    },
                    error:function (error) {
                      alert(error);
                    }});
                },
                error: function(error){
                  alert(error);
                }
              })
            };

            $scope.dialogCancelReservationSuccess = function(ev){
              $mdDialog.show({
                controller         : function ($scope, $mdDialog)
                {
                  $scope.dataDialog = {
                    content: "Bạn đã hủy đặt sân thành công!!",
                    dialogClose: "ĐÓNG"
                  };
                  $scope.closeDialog = function ()
                  {
                    $mdDialog.hide();
                  }
                },
                templateUrl           :  'app/main/apps/dashboards/project/booking_schedule/dialog/dialogCancelReservationSuccess.html',
                parent             : angular.element('body'),
                targetEvent        : ev,
                clickOutsideToClose: false
              })
            }
          },
          templateUrl           :  'app/main/apps/dashboards/project/booking_schedule/dialog/dialogCancelReservationQuestion.html',
          parent             : angular.element('body'),
          targetEvent        : ev,
          clickOutsideToClose: false
        })

      }


    }

})();
