(function () {
  'use strict';

  angular
    .module('app.toolbar')
    .controller('TbChangePassword', TbChangePassword);

  function TbChangePassword($http, $mdDialog){
    var cp = this;

    //Data
    cp.changePass = {
      requiredPassword: "Mật khẩu có 6 - 30 ký tự",
      requiredAgain: "Mật khẩu nhập lại không đúng.",
      buttonChange: "THAY ĐỔI",
      buttonCancel: "HỦY",
      logoutQuestion: "Bạn có chắc chắn là muốn đăng xuất?",
      buttonAgree: "ĐỒNG Ý"
    };
    cp.dataChange = {};
    cp.pass='';

    //Method
    cp.changePasswordAccount = changePasswordAccount;
    cp.closeDialog = closeDialog;

    function changePasswordAccount() {
      cp.dataChange.username = 'buigiachau1503';
      // You can do an API call here to send the form to your server
      $.ajax({
        type : 'PUT',
        url: "http://localhost:8010/account",
        contentType : 'application/x-www-form-urlencoded',
        data: cp.dataChange,
        success: function(result){
          cp.dataResult = result;
        },
        error:function (error) {
          alert(error);
        }});
    }

    function closeDialog() {
      $mdDialog.hide();
    }
  }


})();
