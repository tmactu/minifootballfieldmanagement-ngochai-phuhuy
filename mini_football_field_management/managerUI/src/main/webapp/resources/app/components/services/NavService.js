﻿(function () {
    'use strict';

    angular.module('app')
            .service('navService', [
                '$q',
                navService
            ]);

    function navService($q) {
        var menuItems = [
            {
                name: 'Tổng Quan',
                icon: 'resources/app/icon/ic_dashboard_black_48px.svg',
                sref: '.dashboard'
            },
            {
                name: 'Quản Lý Thành Viên',
                icon: 'resources/app/icon/ic_supervisor_account_black_48px.svg',
                sref: '.profile'
            },
            {
                name: 'Quản Lý Đặt Sân',
                icon: 'resources/app/icon/ic_playlist_add_check_black_48px.svg',
                sref: '.table'
            },
            {
                name: 'Quản Lý Câu Lạc Bộ',
                icon: 'resources/app/icon/ic_group_work_black_48px.svg',
                sref: '.manager'
            },
            {
                name: 'Quản Lý Nhân Viên',
                icon: 'resources/app/icon/ic_work_black_48px.svg',
                sref: '.data-table'
            }
        ];

        return {
            loadAllItems: function () {
                return $q.when(menuItems);
            }
        };
    }

})();
