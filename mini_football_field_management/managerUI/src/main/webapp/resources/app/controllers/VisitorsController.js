(function () {
    angular
        .module('app')
        .controller('VisitorsController', [ '$http',
            VisitorsController
        ]);

    function VisitorsController($http) {
        var vm = this;
        vm.visitorsChartData = [];
        vm.chartOptions = [];
        vm.loadNumber = function() {
            $http({
                method: 'GET',
                url: 'http://localhost:8015/club/getSumOfField'
            }).then(function successCallback(response) {
                vm.visitorsChartData = [
                    {
                        'key' : 'Sân 5x5',
                        'sum' : response.data[0].s5x5
                    },
                    {
                        'key' : 'Sân 7x7',
                        'sum' : response.data[0].s7x7
                    },
                    {
                        'key' : 'Sân 11x11',
                        'sum' : response.data[0].s11x11
                    }
                ];
                vm.chartOptions.chart.title = Number(response.data[0].s5x5) + Number(response.data[0].s7x7) + Number(response.data[0].s11x11);
            }, function errorCallback(response) {
                alert(response.statusText);
            });

        };
        // TODO: move data to the service
        vm.chartOptions = {
            chart: {
                type: 'pieChart',
                height: 210,
                donut: true,
                x: function (d) { return d.key; },
                y: function (d) { return d.sum; },
                valueFormat: (d3.format(".0f")),
                color: ['#0BEEF6', '#0BF679', '#E75753'],
                showLabels: false,
                showLegend: true,
                margin: { top: 0 }
            }
        };
    }
})();
