(function () {

    angular
        .module('app')
        .controller('TableController', ['$http', '$mdDialog', '$scope', '$interval',
            TableController
        ]);

    function TableController($http, $mdDialog, $scope, $interval) {
        var vm = this;
        vm.reservation = [];
        vm.reservationActive = [];
        vm.indexField = '';
        vm.deleteReservationOld = deleteReservationOld;
        vm.find = find;
        vm.deleted = deleted;
        vm.recieve = recieve;
        vm.pay = pay;
        vm.setButton = setButton;
        vm.setColor = setColor;
        vm.alertDialog = alertDialog;
        vm.alertDialogNoEvent = alertDialogNoEvent;
        vm.confirmDialog = confirmDialog;
        vm.computeHour = computeHour;

        function computeHour(milisec, type) {
            if(milisec < 0) return '';
            var hour = Math.floor(milisec / 3600);
            var minute = Math.floor((milisec % 3600) / 60)
            if(type == 'hour') return hour;
            if(type == 'minute') return minute;
            if(type == 'second') return milisec - hour * 3600 - minute * 60;
        }

        function find() {
            $http({
                        method: 'GET',
                        url: 'http://localhost:8012/manager'
                    }).then(function successCallback(response) {
                        vm.reservation = response.data;
                        vm.reservationActive = vm.reservation.filter(function (p1) {
                            return p1.timeline > 0;
                        })
                    }, function errorCallback(response) {
                        vm.alertDialog('Error', response.statusText);
                    });
        }

        function deleteReservationOld() {
//            $.ajax({
//                type: 'DELETE',
//                data: {
//                    idReservation: ''
//                },
//                contentType: 'application/x-www-form-urlencoded',
//                url: "http://localhost:8012/manager/pay",
//                success: function (result) {
                    vm.find();
                    

//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                    vm.alertDialog( 'Error', textStatus);
//                }
//            });
        }

        function deleted(id) {
            $.ajax({
                type: 'DELETE',
                data: {
                    idReservation: id
                },
                contentType: 'application/x-www-form-urlencoded',
                url: "http://localhost:8012/manager",
                success: function (result) {
                    vm.find();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    vm.alertDialog( 'Error', textStatus);
                }
            });
        }

        function recieve(id) {
            $.ajax({
                type: 'PUT',
                data: {
                    idReservation: id
                },
                contentType: 'application/x-www-form-urlencoded',
                url: "http://localhost:8012/manager",
                success: function (result) {
                    vm.find();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    vm.alertDialog('Error', textStatus);
                }
            });
        }

        function pay(id) {
            $.ajax({
                type: 'PUT',
                data: {
                    idReservation: id
                },
                contentType: 'application/x-www-form-urlencoded',
                url: "http://localhost:8012/manager/pay",
                success: function (result) {
                    vm.find();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    vm.alertDialog( 'Error', textStatus);
                }
            });
        }

        function setButton(value) {
            if (value == 'used') {
                return 'resources/app/icon/ic_alarm_off_white_48px.svg';
            } else if (value == 'waiting') {
                return 'resources/app/icon/ic_alarm_on_white_48px.svg';
            } else
                return 'resources/app/icon/ic_alarm_add_black_48px.svg';
        }

        function setColor(value) {
            if (value == 'used') {
                return 'red-text';
            } else if (value == 'waiting') {
                return 'yellow-text';
            } else
                return 'green-text';
        }
        function timeRise() {
            for(var i = 0; i < vm.reservationActive.length; i++) {
                if(vm.reservationActive[i].timeline > 0){
                    vm.reservationActive[i].timeline -= 1;
                }else {
                    $interval.cancel(vm.timer);
                    if(vm.reservationActive[i].status == 'used'){
                        vm.alertDialogNoEvent('Thông báo','Sân số ' + vm.reservationActive[i].fields + ' đã hết thời gian sử dụng. Phí sân ' + vm.reservationActive[i].rates + '.000 đồng.', vm.reservationActive[i].idReservation);
                    } else {
                        vm.confirmDialog('Thông báo','Sân số ' + vm.reservationActive[i].fields + ' đã đến thời gian nhận.', vm.reservationActive[i].idReservation);
                    }
                    break;
                }
            }
        }

        //auto load
        vm.timer = $interval(timeRise, 1000);

        function confirmDialog(title, content, id) {
            var confirm = $mdDialog.confirm()
                .title(title)
                .textContent(content)
                .ok("Nhận")
                .cancel("Hủy đặt");
            $mdDialog.show(confirm).then(function () {
                vm.recieve(id);
                vm.timer = $interval(timeRise, 1000);
            }, function () {
                vm.deleted(id);
                vm.timer = $interval(timeRise, 1000);
            });
        }

        function alertDialog(title, comment) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(comment)
                    .ok('OK')
            );
        }

        function alertDialogNoEvent(title, comment, id) {
            var confirm = $mdDialog.confirm()
                .title(title)
                .textContent(comment)
                .ok('OK');
            $mdDialog.show(confirm).then(function () {
                vm.pay(id);
                vm.timer = $interval(timeRise, 1000);
            });
        }

        vm.showConfirm = function (ev, index) {
            vm.indexField = index;
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'resources/app/views/partials/FieldDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            })
                .then(function (answer) {

                }, function () {

                });
        };
        
        vm.showAdvanced = function(ev) {
            $mdDialog.show({
              controller: DialogController,
              templateUrl: 'resources/app/views/partials/use-reservation.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: true // Only for -xs, -sm breakpoints.
            })
            .then(function(answer) {
             
            }, function() {
              
            });
          };

        function DialogController($scope, $mdDialog, $http) {
            $scope.reservations = [];
            $scope.field = vm.indexField;
            $scope.showAdd = false;
            $scope.hourStart = '';
            $scope.minuteStart = '';
            $scope.hourEnd = '';
            $scope.minuteEnd = '';
            $scope.start = {
                hour: [],
                minute: []
            };
            $scope.end = {
                hour: [],
                minute: []
            };

            $scope.loadStartHourTime = function () {
                var time = new Date();
                var start = [];
                for (var i = time.getHours(); i < 22; i++) {
                    start.push(i);
                }
                $scope.start.hour = start;
            };

            $scope.loadStartMinuteTime = function () {
                var time = new Date();
                var end = [];
                var str = time.getMinutes();
                if ($scope.hourStart == '') {

                } else if ($scope.hourStart == time.getHours()) {
                    str = (5 - str % 5) + str;
                    for (var i = str; i < 60; i += 5) {
                        end.push(i);
                    }
                    $scope.start.minute = end;
                } else {
                    str = 0;
                    for (var i = str; i <= 60; i += 5) {
                        end.push(i);
                    }
                    $scope.start.minute = end;
                }

            };

            $scope.loadEndHourTime = function () {
                var start = $scope.hourStart + 1;
                var temp = [];
                for (var i = start; i < 23; i++) {
                    temp.push(i);
                }
                $scope.end.hour = temp;
            };

            $scope.loadEndMinuteTime = function () {
                var start = $scope.minuteStart;
                var temp = [];
                for (var i = start; i <= 60; i += 5) {
                    temp.push(i);
                }
                $scope.end.minute = temp;
            };

            $scope.getReservation = function (ev) {
                $http({
                    method: 'GET',
                    url: 'http://localhost:8012/manager/pay?idField=' + vm.indexField
                }).then(function successCallback(response) {
                    $scope.reservations = response.data;
                }, function errorCallback(response) {
                    vm.alertDialog('Error', response.statusText);
                });
            };
            $scope.show = function () {
                $scope.showAdd = true;
            };
            $scope.hide = function () {
                $scope.showAdd = false;
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.deleteReservation = function (ev, value, index) {
                // Appending dialog to document.body to cover sidenav in docs app
                var confirm = $mdDialog.confirm()
                    .title('Bạn có chắc muốn xóa lịch đặt sân?')
                    .textContent('Từ ' + $scope.reservations[index].startTime + ' đến ' + $scope.reservations[index].endTime)
                    .targetEvent(ev)
                    .ok('Đúng')
                    .cancel('Hủy');
                $mdDialog.show(confirm).then(function () {
                    $.ajax({
                        type: 'DELETE',
                        data: {
                            idReservation: value
                        },
                        contentType: 'application/x-www-form-urlencoded',
                        url: "http://localhost:8012/manager",
                        success: function (result) {
                            vm.find();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            vm.alertDialog('Error', textStatus);
                        }
                    });
                }, function () {
                    $mdDialog.close();
                });
            };

            $scope.addReservation = function (ev) {
                var date = new Date();
                date = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
                var start = $scope.hourStart + ':' + $scope.minuteStart + ':00';
                var end = $scope.hourEnd + ':' + $scope.minuteEnd + ':00';

                $.ajax({
                    type: 'POST',
                    url: 'http://localhost:8012/member',
                    data: {
                        username: 'none',
                        field: $scope.field,
                        dateValue: date,
                        startTimeValue: start,
                        endTimeValue: end
                    },
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (result) {
                        if(result.error == 'error'){
                            vm.alertDialog('Thất bại', 'Lịch đã thêm trùng giờ');
                        }else {
                            vm.alertDialog('Thành Công', 'Đã thêm lịch đặt sân ' + $scope.field + ' từ ' + start + ' đến ' + end);
                            vm.find();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        vm.alertDialog( 'Error', textStatus);
                    }
                });
            }
        }
    }
})();
