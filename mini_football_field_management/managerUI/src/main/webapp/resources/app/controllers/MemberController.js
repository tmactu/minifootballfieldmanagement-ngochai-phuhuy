(function () {
    angular
        .module('app')
        .controller('MemberController', [ '$http',
            MemberController
        ]);

    function MemberController($http) {
        var vm = this;
        vm.visitorsChartData = [];
        vm.chartOptions = [];
        vm.loadNumber = function() {
            $http({
                method: 'GET',
                url: 'http://localhost:8010/member/getSumOfMember'
            }).then(function successCallback(response) {
                vm.visitorsChartData = response.data;
                vm.chartOptions.chart.title = Number(response.data[0].number) + Number(response.data[0].number) + Number(response.data[0].number);
            }, function errorCallback(response) {
                alert(response.statusText);
            });

        };
        // TODO: move data to the service
        vm.chartOptions = {
            chart: {
                type: 'pieChart',
                height: 210,
                donut: true,
                x: function (d) { return d.type; },
                y: function (d) { return d.number; },
                valueFormat: (d3.format(".0f")),
                color: ['#0BEEF6', '#0BF679', '#E75753'],
                showLabels: false,
                showLegend: true,
                margin: { top: 0 }
            }
        };
    }
})();
