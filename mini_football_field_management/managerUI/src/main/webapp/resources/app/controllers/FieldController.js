(function(){

  angular
    .module('app')
    .controller('FieldController', [ '$http', '$mdDialog',
        FieldController
    ]);

  function FieldController($http, $mdDialog) {
      var vm = this;
      vm.dataField = [];
      vm.type = '';
      vm.autoLoad = autoLoad;
      vm.alertDialog = alertDialog;
      vm.deleteMember = deleteMember;
      vm.addNewField = addNewField;
      vm.updatePrice = updatePrice;
      vm.createDummyData5 = [];
      vm.createDummyData1 =[];
      vm.createDummyData7 = [];
          function autoLoad(){
          $http({
              method: 'GET',
              url: 'http://localhost:8011/'
          }).then(function successCallback(response) {
              vm.dataField = response.data;
              vm.createDummyData5 = vm.dataField.filter(function (p1) {
                  return p1.typeOfField == '5x5';
              });
              vm.createDummyData1 = vm.dataField.filter(function (p1) {
                  return p1.typeOfField == '11x11';
              });
              vm.createDummyData7 = vm.dataField.filter(function (p1) {
                  return p1.typeOfField == '7x7';
              });
          }, function errorCallback(response) {
              vm.alertDialog('Lỗi', 'Kết nối máy chủ gặp sự cố');
          });
      };

      function deleteMember(available) {
          $mdDialog.show(vm.confirmDialog('Bạn có chắc muốn xóa?', 'Sân số ' + available)).then(function () {
              $.ajax({
                  type: 'DELETE',
                  data: {
                      id: available
                  },
                  contentType: 'application/x-www-form-urlencoded',
                  url: "http://localhost:8011/",
                  success: function (result) {
                      vm.autoLoad();
                      vm.alertDialog('Thông báo', 'Xóa sân thành công');
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      vm.alertDialog('Lỗi', 'Kết nối thất bại, không thể xóa sân');
                  }
              });
          }, function () {
          });
      }

      function addNewField(type) {
          $mdDialog.show(vm.confirmDialog('Bạn có chắc muốn thêm sân?', 'Loại sân ' + type)).then(function () {
              $.ajax({
                  type : 'POST',
                  data: {
                      typeOfField : type
                  },
                  contentType : 'application/x-www-form-urlencoded',
                  url: "http://localhost:8011/",
                  success: function(result){
                      vm.alertDialog('Thông báo', 'Thêm sân thành công');
                      vm.autoLoad();
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      vm.alertDialog('Lỗi', 'Thêm sân thất bại');
                  }});
          }, function () {
          });
      }
      
      function updatePrice(type, price) {
              $.ajax({
                  type : 'PUT',
                  data: {
                      typeOfField : type,
                      price : price
                  },
                  contentType : 'application/x-www-form-urlencoded',
                  url: "http://localhost:8011/",
                  success: function(result){
                      vm.alertDialog('Thông báo', 'Cập nhật giá thành công');
                      if(vm.type == '5x5') {
                        vm.createDummyData5.forEach(function (item, index, arr) {
                            arr[index].price = price;
                        });
                    } else if(vm.type == '7x7') {
                        vm.createDummyData7.forEach(function (item, index, arr) {
                            arr[index].price = price;
                        });
                    } else{
                        vm.createDummyData1.forEach(function (item, index, arr) {
                            arr[index].price = price;
                        });
                    }
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      vm.alertDialog('Lỗi', 'Cập nhật giá thất bại');
                  }});
         
      }
      
      vm.showAdvanced = function(ev, type) {
          vm.type = type;
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'resources/app/views/partials/updatePriceField.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(answer) {

        }, function() {

        });
      };
      function DialogController($scope, $mdDialog) {
          if(vm.type == '5x5') {
              $scope.price = vm.createDummyData5[0].price;
          } else if(vm.type == '7x7') {
              $scope.price = vm.createDummyData7[0].price;
          } else{
              $scope.price = vm.createDummyData1[0].price;
          }
        
        $scope.cancel = function() {
          $mdDialog.cancel();
        };

        $scope.answer = function() {
            vm.updatePrice(vm.type, $scope.price);
        };
      }

      function alertDialog(title, content) {
          $mdDialog.show(
              $mdDialog.alert()
                  .parent(angular.element(document.body))
                  .clickOutsideToClose(true)
                  .title(title)
                  .textContent(content)
                  .ok('Đóng')
          );
      }
      vm.confirmDialog = function (title, content) {
          return $mdDialog.confirm().title(title)
              .textContent(content)
              .ok('OK')
              .cancel('Thoát');
      };
  }
})();
