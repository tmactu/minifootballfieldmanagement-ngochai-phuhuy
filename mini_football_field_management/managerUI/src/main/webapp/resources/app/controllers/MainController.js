(function(){

  angular
       .module('app')
       .controller('MainController', [
          'navService', '$mdSidenav', '$mdBottomSheet', '$log', '$q', '$state', '$mdToast', '$location', '$http', '$window', '$cookieStore', '$mdDialog',
          MainController
       ]);

  function MainController(navService, $mdSidenav, $mdBottomSheet, $log, $q, $state, $mdToast, $location, $http, $window, $cookieStore, $mdDialog) {
    var vm = this;
    vm.alertDialog = alertDialog;
    function alertDialog(title, content) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(content)
                    .ok('Đóng')
            );
        }
    vm.checkForLogin = function(){
        $http({
                method: 'GET',
                url: 'http://localhost:8010/account/checkManager'
            }).then(function successCallback(response) { 
            }, function errorCallback(response) {
                 $window.location.href = 'http://localhost:8000/main';
            }); 
        $http({
                method: 'GET',
                url: 'http://localhost:8015/club/nameAndAdrressClub'
            }).then(function successCallback(response) {
                vm.name = response.data.name;
            }, function errorCallback(response) {
                 $window.location.href = 'http://localhost:8000/main';
        }); 
    };  
     
    vm.logout = function(){
        $cookieStore.remove('jwt');
        $cookieStore.remove('username');
        $cookieStore.remove('club');
        $window.location.href = 'http://localhost:8000/main';
    };
    vm.menuItems = [ ];
    vm.selectItem = selectItem;
    vm.toggleItemsList = toggleItemsList;
    vm.showActions = showActions;
    vm.title = $state.current.data.title;
    vm.showSimpleToast = showSimpleToast;
    vm.toggleRightSidebar = toggleRightSidebar;
    vm.account = {
        infor : 'Thông tin cá nhân',
        password : 'Đổi mật khẩu'
    };
    navService
      .loadAllItems()
      .then(function(menuItems) {
        vm.menuItems = [].concat(menuItems);
      });

    function toggleRightSidebar() {
        $mdSidenav('right').toggle();
    }

    function toggleItemsList() {
      var pending = $mdBottomSheet.hide() || $q.when(true);

      pending.then(function(){
        $mdSidenav('left').toggle();
      });
    }

    function selectItem (item) {
      vm.title = item.name;
      vm.toggleItemsList();
      vm.showSimpleToast(vm.title);
    }

    function showActions($event) {
        $mdBottomSheet.show({
          parent: angular.element(document.getElementById('content')),
          templateUrl: 'resources/app/views/partials/bottomSheet.html',
          controller: [ '$mdBottomSheet', SheetController],
          controllerAs: "vm",
          bindToController : true,
          targetEvent: $event
        }).then(function(clickedItem) {
          clickedItem && $log.debug( clickedItem.name + ' clicked!');
        });

        function SheetController( $mdBottomSheet ) {
          var vm = this;

          vm.actions = [
            { name: 'Share', icon: 'share', url: 'https://twitter.com/intent/tweet?text=Angular%20Material%20Dashboard%20https://github.com/flatlogic/angular-material-dashboard%20via%20@flatlogicinc' },
            { name: 'Star', icon: 'star', url: 'https://github.com/flatlogic/angular-material-dashboard/stargazers' }
          ];

          vm.performAction = function(action) {
            $mdBottomSheet.hide(action);
          };
        }
    }

    function showSimpleToast(title) {
      $mdToast.show(
        $mdToast.simple()
          .content(title)
          .hideDelay(2000)
          .position('bottom right')
      );
    }

    vm.showAdvanced = function(ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'resources/app/views/partials/changePasswordDialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: true // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
      }, function() {
      });
    };

  function DialogController($scope, $mdDialog) {
    $scope.password = {};
    $scope.show = false;
    $scope.showError = false;
    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function() {
        if($scope.password.newPassword != $scope.password.newPasswordAgain){
          $scope.show = true;
        } else {
            $.ajax({
                    type : 'PUT',
                    data: $scope.password,
                    contentType : 'application/x-www-form-urlencoded',
                    url: "http://localhost:8010/account",
                    success: function(result){
                      if(result.result == 'success')
                        vm.alertDialog('Thông báo', 'Thay đổi mật khẩu thành công');
                      else {
                        $scope.showError = true;
                        $scope.show = false;
                      }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        vm.alertDialog('Lỗi', 'Kết nối máy chủ thất bại');
                    }});
        }
    };
  }
  }

})();
