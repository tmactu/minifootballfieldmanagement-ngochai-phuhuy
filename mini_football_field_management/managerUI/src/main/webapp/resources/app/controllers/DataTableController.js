(function(){

  angular
    .module('app')
    .controller('DataTableController', [
      '$scope', '$http', '$mdSidenav', '$mdDialog',
      TableController
      
    ]);

  function TableController($scope, $http, $mdSidenav,  $mdDialog ) {
    var vm = this;
    vm.name = '';
    vm.tableData = [];
    vm.paramater = '';
    vm.totalItems = 0;
    vm.toggleRightSidebar = toggleRightSidebar;
    vm.RightSidebarNewMember = RightSidebarNewMember;
    $scope.close = close;
    vm.find = find;
    vm.updateMember = updateMember;
    vm.addNewMember = addNewMember;
    vm.deleteMember = deleteMember;
    $scope.selected = [];
    vm.isUpdate = false;
    vm.alertDialog = alertDialog;
      vm.newInfor = {
          username: '',
          name: '',
          salary: '',
          numberPhone: '',
          birthdate: new Date(),
          id: '',
          gender: 'Nam',
          address: '',
          email: '',
          startingDate: new Date(),
          work : ''
      };
    vm.query = {
      order: 'name',
      limit: 10,
      page: 1
    };
    $scope.selected = [];
      vm.confirmDialog = function (title, content) {
          return $mdDialog.confirm().title(title)
              .textContent(content)
              .ok('OK')
              .cancel('Thoát');
      };
      function alertDialog(title, content) {
          $mdDialog.show(
              $mdDialog.alert()
                  .parent(angular.element(document.body))
                  .clickOutsideToClose(true)
                  .title(title)
                  .textContent(content)
                  .ok('Đóng')
          );
      }
      function close() {
          vm.falsePassword = false;
          // Component lookup should always be available since we are not using `ng-if`
          $mdSidenav('right').close();
          $mdSidenav('rightAdd').close();
      }

      function toggleRightSidebar(index) {
          vm.indexItem = vm.tableData[index];
          vm.isUpdate = false;
          if(vm.indexItem.gender == 'true'){
              vm.indexItem.gender = 'Nam';
          }else {
              vm.indexItem.gender = 'Nu';
          }
          vm.indexItem.numberPhone = Number(vm.indexItem.numberPhone);
          vm.indexItem.id = Number(vm.indexItem.id);
          vm.indexItem.birthdate = new Date(vm.indexItem.birthdate);
          vm.indexItem.startingDate = new Date(vm.indexItem.startingDate);
          $mdSidenav('right').toggle();
      }

      function RightSidebarNewMember() {
          vm.showAccountUsed = false;
          vm.showErrorIsExist = false;
          vm.showErrorBlank = false;
          $mdSidenav('rightAdd').toggle();
      }

      function find(available) {
          //available =  base64.encode(available);
          $http({
              method: 'GET',
              url: 'http://localhost:8010/employee/find?parameter='+ available
          }).then(function successCallback(response) {
              vm.tableData = '';
              vm.tableData = response.data;
              vm.totalItems = vm.tableData.length;
          }, function errorCallback(response) {
              vm.alertDialog('Lỗi','Tìm kiếm thất bại');
          });
      };

      function updateMember(available) {
          $mdDialog.show(vm.confirmDialog('Bạn có chắc muốn cập nhật?', 'Nhân viên' + vm.indexItem.name)).then(function() {
              vm.tableData[available] = vm.indexItem;
              vm.tableData[available].birthdate = vm.tableData[available].birthdate.getFullYear() +
                  "/" + (vm.tableData[available].birthdate.getMonth() + 1) + "/" +
                  vm.tableData[available].birthdate.getDate();
              vm.tableData[available].startingDate = vm.tableData[available].startingDate.getFullYear() +
                  "/" + (vm.tableData[available].startingDate.getMonth() + 1) + "/" +
                  vm.tableData[available].startingDate.getDate();
              $.ajax({
                  type : 'PUT',
                  data: vm.tableData[available],
                  contentType : 'application/x-www-form-urlencoded',
                  url: "http://localhost:8010/employee",
                  success: function(result){
                      vm.alertDialog('Thông báo', 'Cập nhật thông tin nhân viên thành công');
                      $scope.close();
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      vm.alertDialog('Lỗi', 'Cập nhật nhân viên không thành công');
                  }});
          }, function() {
          });

      };

      function addNewMember() {
          var date = new Date(vm.newInfor.birthdate);
          vm.newInfor.birthdate = date.getDate() + '/' + date.getMonth() + '/' +  date.getFullYear() ;
          var date1 = new Date(vm.newInfor.startingDate);
          vm.newInfor.startingDate = date1.getDate() + '/' + date1.getMonth() + '/' +  date1.getFullYear() ;
              $.ajax({
                  type : 'POST',
                  data: vm.newInfor,
                  contentType : 'application/x-www-form-urlencoded',
                  url: "http://localhost:8010/employee",
                  success: function(result){
                      vm.alertDialog('Thông báo', 'Thêm thành viên thành công');
                      $scope.close();
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      vm.alertDialog('Lỗi', 'Thêm thành viên thất bại');
                  }});
      };

      function deleteMember(available) {
          $mdDialog.show(vm.confirmDialog('Bạn có chắc muốn xóa?', 'Nhân viên ' + vm.tableData[available].name)).then(function() {
              $.ajax({
                  type : 'DELETE',
                  data: {
                      username : vm.tableData[available].username
                  },
                  contentType : 'application/x-www-form-urlencoded',
                  url: "http://localhost:8010/employee",
                  success: function(result){
                      vm.tableData.splice(available,1);
                      vm.alertDialog('Thông báo', 'Xóa thành viên thành công');
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      vm.alertDialog('Lỗi', 'Kết nối thất bại, không thể xóa thành viên');
                  }});
          }, function() {
          });

      };
  }

})();
