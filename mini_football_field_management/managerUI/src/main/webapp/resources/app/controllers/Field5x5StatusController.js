(function () {
    angular
        .module('app')
        .controller('Field5x5Controller', ['$http',
            Field5x5Controller
        ]);

    function Field5x5Controller($http) {
        var vm = this;
        vm.title5x5 = '5x5';
        vm.title7x7 = '7x7';
        vm.title11x11 = '11x11';
        vm.ChartData5x5 = [];
        vm.ChartData7x7 = [];
        vm.ChartData11x11 = [];
        vm.chartOptions = [];
        function check(value){
            if(value != null)
                return value;
            return 0;
        }
        vm.loadNumber = function (type) {
            var idClub = 1;
            $http({
                method: 'GET',
                url: 'http://localhost:8012/?idClub=' + idClub
            }).then(function successCallback(response) {
                response.data.sum[4] = check(response.data.sum[4]);
                response.data.sum[5] = check(response.data.sum[5]);
                response.data.sum[3] = check(response.data.sum[3]);
                vm.ChartData5x5 = [
                    {
                        key: 'Còn trống',
                        value: response.data.sum[1] - response.data.sum[4]
                    },
                    {
                        key: 'Đang sử dụng',
                        value: response.data.sum[4]
                    }
                ];
                vm.ChartData7x7 = [
                    {
                        key: 'Còn trống',
                        value: response.data.sum[2] - response.data.sum[5]
                    },
                    {
                        key: 'Đang sử dụng',
                        value: response.data.sum[5]
                    }
                ];
                vm.ChartData11x11 = [
                    {
                        key: 'Còn trống',
                        value: response.data.sum[0] - response.data.sum[3]
                    },
                    {
                        key: 'Đang sử dụng',
                        value: response.data.sum[3]
                    }
                ];
                vm.chartOptions.chart.title = response.data.sum[type];
            }, function errorCallback(response) {
                alert(response.statusText);
            });

        };
        // TODO: move data to the service
        vm.chartOptions = {
            chart: {
                type: 'pieChart',
                height: 210,
                donut: true,
                x: function (d) {
                    return d.key;
                },
                y: function (d) {
                    return d.value;
                },
                valueFormat: (d3.format(".0f")),
                color: ['#00CC33', '#FF0000'],
                showLabels: false,
                showLegend: true,
                margin: {top: 0}
            }
        };
    }
})();
