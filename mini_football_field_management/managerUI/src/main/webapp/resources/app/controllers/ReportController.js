(function () {
    angular
        .module('app')
        .controller('ReportController', [ '$http', '$mdDialog',
            ReportController
        ]);

    function ReportController($http, $mdDialog) {
        var vm = this;
        vm.month = [1,2,3,4,5,6,7,8,9,10,11,12];
        vm.year = [2014,2015,2016,2017];
        vm.time = {
            month: new Date().getMonth() + 1,
            year: 2017
        }
        vm.showSave = false;
        vm.alertDialog = alertDialog;
//        vm.confirmDialog = confirmDialog;
        vm.data = [];
        vm.load = load;
        vm.greaterThan = function(prop){
            return function(item){
              return item <= new Date().getMonth() + 1;
            }
        }
        
        function load(){
        	$http({
                method: 'GET',
                url: 'http://localhost:8015/club/statistical?startMonth=' + vm.time.month + '&endMonth='+ vm.time.month +'&year=' + vm.time.year
            }).then(function successCallback(response) {               
            	vm.data = response.data;
                vm.showSave = true;
            }, function errorCallback(response) {
                vm.alertDialog('Lỗi', 'Kết nối máy chủ gặp sự cố');
            }); 
        };
        
        function alertDialog(title, content) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(content)
                    .ok('Đóng')
            );
        }
        
        vm.createMoney = function(money){
            if(money == undefined) return 0;
            
            money *= 1000;           
            
            return money;
        }
        
        var doc = new jsPDF();          
        
        
        vm.get = function(){
            $("#reportPDF").printThis({
                debug: false,               // show the iframe for debugging
                importCSS: true,            // import page CSS
                importStyle: false,         // import style tags
                printContainer: true,       // grab outer container as well as the contents of the selector
                loadCSS: "resources/app/stylesheets/table.css",  // path to additional css file - use an array [] for multiple
                
            });
        }
    }
})();
