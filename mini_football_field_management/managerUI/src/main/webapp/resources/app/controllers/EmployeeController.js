(function () {
    angular
        .module('app')
        .controller('EmployeeController', [ '$http',
            EmployeeController
        ]);

    function EmployeeController($http) {
        var vm = this;
        vm.visitorsChartDataEmployee = [];
        vm.visitorsChartDataMember = [];
        vm.chartOptions = [];
        vm.loadNumber = function() {
            $http({
                method: 'GET',
                url: 'http://localhost:8010/employee/getSumOfEmployee'
            }).then(function successCallback(response) {
                vm.visitorsChartDataEmployee = response.data;
                var sum = 0;
                for(var i = 0; i < vm.visitorsChartDataEmployee.length; i++){
                    sum += Number(vm.visitorsChartDataEmployee[i].count);
                }
                vm.chartOptionsEmployee.chart.title = sum;
                
                 $http({
                method: 'GET',
                url: 'http://localhost:8010/member/getSumOfMember'
            }).then(function successCallback(response) {
                vm.visitorsChartDataMember = response.data;
                vm.chartOptionsMember.chart.title = Number(response.data[0].number) + Number(response.data[0].number) + Number(response.data[0].number);
            }, function errorCallback(response) {
                alert(response.statusText);
            });
            }, function errorCallback(response) {
                alert(response.statusText);
            });         
           

        };
        // TODO: move data to the service
        vm.chartOptionsEmployee = {
            chart: {
                type: 'pieChart',
                height: 210,
                donut: true,
                x: function (d) { return d.work; },
                y: function (d) { return d.count; },
                valueFormat: (d3.format(".0f")),
                color: ['#0BEEF6', '#0BF679', '#E75753','#FF3333' , 'red', 'green', 'yellow', '#999900', '#3399FF'],
                showLabels: false,
                showLegend: true,
                margin: { top: 0 }
            }
        };
        
         vm.chartOptionsMember = {
            chart: {
                type: 'pieChart',
                height: 210,
                donut: true,
                x: function (d) { return d.type; },
                y: function (d) { return d.number; },
                valueFormat: (d3.format(".0f")),
                color: ['#0BEEF6', '#0BF679', '#E75753', 'red', 'green', 'yellow', '#999900', '#3399FF'],
                showLabels: false,
                showLegend: true,
                margin: { top: 0 }
            }
        };
    }
})();
