(function () {

    angular
            .module('app')
            .controller('ProfileController', [
                '$scope', '$mdSidenav', '$http', '$mdDialog', 'base64',
                ProfileController

            ]);

    function ProfileController($scope, $mdSidenav, $http, $mdDialog, base64) {
        var vm = this;
        vm.paramater = '';
        vm.falsePassword = false;
        vm.showErrorIsExist = false;
        vm.showAccountUsed = false;
        vm.showErrorBlank = false;
        vm.checkUsernameExist = checkUsernameExist;
        $scope.selected = [];
        vm.autoLoad = autoLoad;
        vm.selectTypeOfMember = '';
        $scope.whiteframeTableIndex = '';
        $scope.whiteframeTableValue = '';
        $scope.getwhiteframeTable = getwhiteframeTable;
        vm.setWhiteframeTable = setWhiteframeTable;
        vm.tableData = [];
        vm.totalItems = 0;
        vm.indexItem = [];
        vm.isUpdate = false;
        vm.toggleRightSidebar = toggleRightSidebar;
        vm.RightSidebarNewMember = RightSidebarNewMember;
        vm.deleteMember = deleteMember;
        vm.updateMember = updateMember;
        vm.addNewMember = addNewMember;
        vm.alertDialog = alertDialog;
        vm.find = find;
        vm.notFind = false;
        vm.findResults = '';
        $scope.partitionColor = partitionColor;
        $scope.close = close;

        $scope.query = {
            order: 'name',
            limit: 8,
            page: 1
        };
        function alertDialog(title, content) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(content)
                    .ok('Đóng')
            );
        }
        vm.confirmDialog = function (title, content) {
            return $mdDialog.confirm().title(title)
                .textContent(content)
                .ok('OK')
                .cancel('Thoát');
        };

        function setWhiteframeTable(value, element) {
            $scope.whiteframeTableIndex = element;
            $scope.whiteframeTableValue = value;
        }
        function getwhiteframeTable(element) {
            if($scope.whiteframeTableIndex == element){
                return $scope.whiteframeTableValue;
            }
        }
        $scope.selected = [];
        var lastQuery = null;
        
        function autoLoad(){
        	$http({
                method: 'GET',
                url: 'http://localhost:8015/typeOfMember'
            }).then(function successCallback(response) {               
            	vm.selectTypeOfMember = response.data;
            }, function errorCallback(response) {
                vm.alertDialog('Lỗi', 'Kết nối máy chủ gặp sự cố');
            }); 
        };
        
        vm.getItems = function () {
            /**
             * I don't know why this function is being called too many times,
             * it supposed to call once per pagination, so the next 3 lines are only to avoid
             * multiple requests. 
             */
            var query1 = JSON.stringify(vm.query);
            if (query1 == lastQuery)
                return;
            lastQuery = query1;
            vm.information;

        };
        vm.newInfor = {
            username: '',
            name: '',
            typeOfMember: '',
            numberPhone: '',
            birthdate: new Date(),
            totalHours: 0,
            totalPayments: 0,
            password: '',
            repassword: '',
            id: '',
            gender: 'Nam',
            address: '',
            email: '',
            lastOrderDate: new Date()
        };

        function find(available) {
            available =  base64.encode(available);
            $http({
                method: 'GET',
                url: 'http://localhost:8010/member?parameter='+ available
            }).then(function successCallback(response) {
                vm.findResults = '';
                vm.findResults = response.data;
                if(response.data.length == 0){
                    vm.notFind = true;
                }
                else vm.notFind = false;
            }, function errorCallback(response) {
                vm.alertDialog('Lỗi','Tìm kiếm thất bại');
            });         
        };
        
        function addNewMember() {
        	var date = new Date(vm.newInfor.birthdate);
        	vm.newInfor.birthdate = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() ;
        	if(vm.newInfor.password == vm.newInfor.repassword) {
                $.ajax({
                    type : 'POST',
                    data: vm.newInfor,
                    contentType : 'application/x-www-form-urlencoded',
                    url: "http://localhost:8010/account/register",
                    success: function(result){
                        vm.alertDialog('Thông báo', 'Thêm thành viên thành công');
                        vm.newInfor = [];
                        $scope.close();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        vm.alertDialog('Lỗi', 'Thêm thành viên thất bại');
                    }});
            }
            else {
                vm.falsePassword = true;
            }
        };
        
        function deleteMember(available) {
            $mdDialog.show(vm.confirmDialog('Bạn có chắc muốn xóa?', 'Thành viên ' + vm.findResults[available].name)).then(function() {
                $.ajax({
                    type : 'DELETE',
                    data: {
                        parameter : vm.findResults[available].username
                    },
                    contentType : 'application/x-www-form-urlencoded',
                    url: "http://localhost:8010/member",
                    success: function(result){
                        vm.find(vm.paramater);
                        vm.alertDialog('Thông báo', 'Xóa thành viên thành công');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        vm.alertDialog('Lỗi', 'Kết nối thất bại, không thể xóa thành viên');
                    }});
            }, function() {
            });

        };
        
        function updateMember(available) {
            $mdDialog.show(vm.confirmDialog('Bạn có chắc muốn cập nhật?', 'Thành viên ' + available.name)).then(function() {
                vm.findResults[available] = vm.indexItem;
                vm.findResults[available].birthdate = vm.findResults[available].birthdate.getFullYear() +
                    "-" + (vm.findResults[available].birthdate.getMonth() + 1) + "-" +
                    vm.findResults[available].birthdate.getDate();
                $.ajax({
                    type : 'PUT',
                    data: vm.findResults[available],
                    contentType : 'application/x-www-form-urlencoded',
                    url: "http://localhost:8010/member/update",
                    success: function(result){
                        vm.find(vm.paramater);
                        vm.alertDialog('Thông báo', 'Cập nhật thông tin thành viên thành công');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        vm.alertDialog('Lỗi', 'Cập nhật thành viên thành công');
                    }});
            }, function() {
            });

        };

        function partitionColor(index) {
            if (index % 2 == 0) {
                return 'partition_color';
            } else {
                return '';
            }
        }
        ;
        function close() {
            vm.falsePassword = false;
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav('right').close();
            $mdSidenav('rightAdd').close();
        }

        function toggleRightSidebar(index) {
            vm.indexItem = angular.copy(vm.findResults[index]);
            vm.isUpdate = false;
            if(vm.indexItem.gender == 'true'){
                vm.indexItem.gender = 'Nam';
            }else {
                vm.indexItem.gender = 'Nu';
            }
            vm.indexItem.numberPhone = Number(vm.findResults[index].numberPhone);
            vm.indexItem.id = Number(vm.findResults[index].id);
            vm.indexItem.birthdate = new Date(vm.findResults[index].birthdate);
            vm.indexItem.lastOrderDate = new Date(vm.findResults[index].lastOrderDate);
            vm.indexItem.typeOfMember = vm.indexItem.typeOfMember + '';
            $mdSidenav('right').toggle();
        }

        function RightSidebarNewMember() {
            vm.showAccountUsed = false;
            vm.showErrorIsExist = false;
            vm.showErrorBlank = false;
        	vm.newInfor.typeOfMember = vm.selectTypeOfMember[0].typeOfMember;
            $mdSidenav('rightAdd').toggle();
        }

        function checkUsernameExist() {
            vm.showAccountUsed = false;
            vm.showErrorIsExist = false;
            vm.showErrorBlank = false;
            if(vm.newInfor.username.search(' ') == -1) {
                $http({
                    method: 'GET',
                    url: 'http://localhost:8010/account?username=' + vm.newInfor.username
                }).then(function successCallback(response) {
                    if (response.data.result == 'true') {
                        vm.showAccountUsed = true;
                    } else {
                        vm.showErrorIsExist = true;
                    }
                }, function errorCallback(response) {
                    vm.alertDialog('Lỗi', 'Không thể kết nối đến máy chủ');
                });
            } else {
                vm.showErrorBlank = true;
            }
        }
    }

})();
