'use strict';

angular.module('angularMaterialAdmin', ['ngAnimate', 'ngCookies', 'ui.router', 'ngMaterial', 'nvd3', 'app', 'md.data.table', 'ab-base64'])
        
        .config(function ($stateProvider, $urlRouterProvider, $mdThemingProvider,
                $mdIconProvider) {
            $stateProvider
                    .state('home', {
                        url: '',
                        templateUrl: 'resources/app/views/main.html',
                        controller: 'MainController',                        
                        controllerAs: 'vm',
                        abstract: true
                    })
                    .state('home.dashboard', {
                        url: '/',
                        templateUrl: 'resources/app/views/dashboard.html',
                        data: {
                            title: 'Dashboard'
                        }
                    })
                    .state('home.profile', {
                        url: '/member',
                        templateUrl: 'resources/app/views/profile.html',
                        controller: 'ProfileController',
                        controllerAs: 'vm',
                        data: {
                            title: 'Profile'
                        }
                    })
                    .state('home.table', {
                        url: '/reservation',
                        controller: 'TableController',
                        controllerAs: 'vm',
                        templateUrl: 'resources/app/views/table.html',
                        data: {
                            title: 'Table'
                        }
                    })
                    .state('home.data-table', {
                        url: '/employee',
                        controller: 'DataTableController',
                        controllerAs: 'vm',
                        templateUrl: 'resources/app/views/data-table.html',
                        data: {
                            title: 'Nhân viên'
                        }
                    })
                    .state('home.manager', {
                        url: '/manager',
                        controller: 'ClubController',
                        controllerAs: 'vm',
                        templateUrl: 'resources/app/views/partials/club.html',
                        data: {
                            title: 'Câu lạc bộ'
                        }
                    });
           
            $urlRouterProvider.otherwise('/');

            $mdThemingProvider
                    .theme('default')
                    .primaryPalette('grey', {
                        'default': '600'
                    })
                    .accentPalette('teal', {
                        'default': '500'
                    })
                    .warnPalette('defaultPrimary');

            $mdThemingProvider.theme('dark', 'default')
                    .primaryPalette('defaultPrimary')
                    .dark();

            $mdThemingProvider.theme('grey', 'default')
                    .primaryPalette('grey');
            
            $mdThemingProvider.theme('green')
                    .primaryPalette('green')
                    .warnPalette('orange');
            
            $mdThemingProvider.theme('custom', 'default')
                    .primaryPalette('defaultPrimary', {
                        'hue-1': '50'
                    });

            $mdThemingProvider.definePalette('defaultPrimary', {
                '50': '#FFFFFF',
                '100': 'rgb(255, 198, 197)',
                '200': '#E75753',
                '300': '#E75753',
                '400': '#E75753',
                '500': '#E75753',
                '600': '#E75753',
                '700': '#E75753',
                '800': '#E75753',
                '900': '#E75753',
                'A100': '#E75753',
                'A200': '#E75753',
                'A400': '#E75753',
                'A700': '#E75753'
            });

            $mdIconProvider.icon('user', 'resources/assets/images/user.svg', 64);
        });
