package com.ui.manager;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller start page index.html
 *
 * @author Tran Ngoc Hai
 */
@Controller
public class Index {

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("test", "day la cai test");
        return "WEB-INF/index.html";
    }
}
