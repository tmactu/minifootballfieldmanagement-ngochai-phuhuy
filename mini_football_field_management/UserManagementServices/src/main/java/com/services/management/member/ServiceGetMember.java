/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.member;

import com.model.hibernate.POJO.Employee;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import static com.services.management.user.rest.restInterface.session;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 16-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/member/getMember")
public class ServiceGetMember implements restInterface{
    
    /**
     * Service get information member
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username
     * @return an instance of java.lang.String, return JSONArray result with content success.
     */
    
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
        
        //Get data innput
        String username = request.getHeader("username");
        
        //Create JSONObject to return
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            User user = (User) session.get(User.class, username);  
                result.put("name", user.getName());
                result.put("birthdate", new SimpleDateFormat("yyyy-MM-dd").format(user.getBirthdate()));
                result.put("id", user.getId());
                result.put("gender", user.getGender());
                result.put("address", user.getAddress());
                result.put("numberPhone", user.getNumberPhone());
                result.put("email", user.getEmail());
                result.put("club", user.getClubs().getIdClub());
                result.put("username", user.getUsername());
                
            Criteria cr = session.createCriteria(Member.class)
                    .add(Restrictions.eq("username", username));
            List list = cr.list();
            Iterator<Member> iterator =  list.iterator();
            Member member = iterator.next();
            
            String typeOfMember = member.getTypeOfMember().getTypeOfMember();
            int index = typeOfMember.indexOf("_");           
            
            result.put("typeOfMember",typeOfMember.substring(0, index));
            result.put("totalHour",member.getTotalHours());
            result.put("totalPayment",member.getTotalPayments());  
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        return result.toString();
    }
}
