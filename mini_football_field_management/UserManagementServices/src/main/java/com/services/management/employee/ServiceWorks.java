///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.services.management.employee;
//
//import com.exception.InvalidManagerException;
//import com.exception.RequestNotFoundException;
//import com.model.hibernate.POJO.User;
//import com.services.management.user.rest.restInterface;
//import static com.services.management.user.rest.restInterface.session;
//import javax.servlet.http.HttpServletRequest;
//import org.hibernate.HibernateException;
//import org.json.JSONArray;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// *
// * @author Phu Huy
// * @since: 14-03-2017
// */
//@RestController
//@RequestMapping(value = "/work")
//public class ServiceWorks implements restInterface{
//     /**
//     * Service add work in club
//     *
//     * @param request
//     * Data input include:
//     *                    - name
//     *                    
//     * @return success string.
//     */ 
//    @Override
//    public String doPost(HttpServletRequest request) throws HibernateException, NumberFormatException,
//                                                    InvalidManagerException, RequestNotFoundException{
//        
//        //Get data input
//        String username = request.getHeader("username");
//        String idClub = request.getHeader("club");
//        
//        String work = request.getParameter("work");
//        String rateOfPay = request.getParameter("rateOfPay");
//        
//        //Create JSONArray to return
//        JSONArray arr_listResult = new JSONArray();
//        
//        //Begin transaction
//        session.beginTransaction();
//        
//        try{
//            //checking username of manager which belong to club or not 
//            User manager = (User) session.get(User.class, username);
//                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
//                    throw new InvalidManagerException("Account manager not belong to club");
//            
//            Salary salary = new Salary();
//                salary.setWork(work);
//                salary.setRateOfPay(Integer.parseInt(rateOfPay));
//            //Save object
//            session.save(salary);
//            
//            arr_listResult.put(SUCCESS);
//        }
//        finally{
//            //Close transaction 
//            session.getTransaction().commit();
//        }
//        return arr_listResult.toString();
//    }
//    
//    /**
//     * Service update information work in club
//     *
//     * @param body
//     * @param request
//     * Data input include:
//     *                    - work
//     *                    - rateOfPay
//     *                    
//     * @return success string.
//     */ 
//    @Override
//    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request) 
//                        throws HibernateException, InvalidManagerException, RequestNotFoundException,
//                                                                                NumberFormatException{
//        
//        //Get data input
//        String username = request.getHeader("username");
//        String idClub = request.getHeader("club");
//        
//        String work = body.getFirst("work");
//        String rateOfPay = body.getFirst("rateOfPay");
//        
//        //Create JSONArray to return
//        JSONArray arr_listResult = new JSONArray();
//        
//        //Begin transaction
//        session.beginTransaction();
//        
//        try{
//            //checking username of manager which belong to club or not 
//            User manager = (User) session.get(User.class, username);
//                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
//                    throw new InvalidManagerException("Account manager not belong to club");
//                
//            Salary salary = (Salary) session.load(Salary.class, work);
//                if(rateOfPay.length() > 0){
//                    salary.setRateOfPay(Integer.parseInt(rateOfPay));
//                }
//            session.update(salary); 
//            
//            arr_listResult.put(SUCCESS);
//        }
//        finally{
//            //Close transaction
//            session.getTransaction().commit();
//        }
//        
//        return arr_listResult.toString();
//    }
//    
//    /**
//     * Service delete work in club
//     *
//     * @param body
//     * @param request
//     * Data input include:
//     *                    - work
//     *                    - rateOfPay
//     *                    
//     * @return success string.
//     */ 
//    @Override
//    public String doDelete (@RequestBody MultiValueMap<String, String> body, 
//            HttpServletRequest request) throws HibernateException, InvalidManagerException,
//                                                                        NumberFormatException{
//        //Get data input
//        String username = request.getHeader("username");
//        String idClub = request.getHeader("club");
//        
//        String work = body.getFirst("work");
//        
//        //Create JSONArray to return
//        JSONArray arr_listResult = new JSONArray();
//        
//        //Begin transaction
//        session.beginTransaction();
//        
//        try{
//            //checking username of manager which belong to club or not 
//            User manager = (User) session.get(User.class, username);
//                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
//                    throw new InvalidManagerException("Account manager not belong to club");
//            
//            Salary salary = (Salary) session.load(Salary.class, work);
//            //Delete object
//            session.delete(salary);
//            
//            arr_listResult.put(SUCCESS);
//        }
//        finally{
//            //Close transaction
//            session.getTransaction().commit();
//        }
//        
//        return arr_listResult.toString();
//    }
//    
////    @Override
////    public String doGet (HttpServletRequest request) throws HibernateException, RequestNotFoundException,
////                                InvalidManagerException, NumberFormatException{
////        
////        //Get data input
////        String username = request.getHeader("username");
////        String idClub = request.getHeader("club");
////        
////        //Create object JSONArray to return
////        JSONArray arr_listResult = new JSONArray();
////         
////        //Begin transaction
////        session.beginTransaction();
////        
////        try{
////            //checking username of manager which belong to club or not 
////            User manager = (User) session.get(User.class, username);
////                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
////                    throw new InvalidManagerException("Account manager not belong to club");
////            
////            //Create criteria
////            Criteria cr = session.createCriteria(Salary)
////        }
////        finally{
////            //Close transaction
////            session.getTransaction().commit();
////        }
////        
////        return arr_listResult.toString();
////    }
//}
