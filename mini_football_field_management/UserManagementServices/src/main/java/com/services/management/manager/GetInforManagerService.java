/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.manager;

import com.exception.InvalidAdminException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import static com.services.management.user.rest.restInterface.session;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hai
 */
@RestController
@RequestMapping(value = "/admin")
public class GetInforManagerService implements restInterface{
    @Override
    public String doGet (HttpServletRequest request)
                  throws HibernateException, InvalidAdminException{
        
        //Get data input
        String username = request.getHeader("username");
        int club = Integer.parseInt(request.getParameter("club"));
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
                
        // begin transaction
        session.beginTransaction();
        
        try{                           
            // Create Criteria have function filter data in User
            Criteria cr = session.createCriteria(User.class);            
            //Check type of user is MEMBER or not
            cr.add(Restrictions.eq("typeOfUser", "manager"));
            cr.add(Restrictions.eq("clubs", new Clubs(club)));
            
            List<User> user = (List<User>) cr.list();
            Iterator<User> iterator =  user.iterator();
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                //Get each user information with username was found
                User usr = iterator.next();
                    result.put("username", usr.getUsername());
                    result.put("club", usr.getClubs().getIdClub());
                    result.put("name", usr.getName());
                    result.put("birthdate", usr.getBirthdate().toString());
                    result.put("id", usr.getId());
                    result.put("gender", usr.getGender().toString());
                    result.put("address", usr.getAddress());
                    result.put("numberPhone", usr.getNumberPhone());
                    result.put("email", usr.getEmail());
                    
                arr_listResult.put(result);
            }
        }
        finally{
            // close transaction
            session.getTransaction().commit();
        }
            
        return arr_listResult.toString();
    }
}
