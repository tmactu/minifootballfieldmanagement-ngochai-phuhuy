/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.employee;

import com.exception.InvalidManagerException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.POJO.Employee;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 13-03-2017
 */
@RestController
@RequestMapping(value = "/employee/getSumOfEmployee")        
public class ServiceGetSumOfEmployee implements restInterface{
    /**
     * Service get sum of employee in club
     *
     * @param request
     * @return an instance of java.lang.String result return is count employee in club
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, NumberFormatException,
                                                InvalidManagerException, RequestNotFoundException{
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        //Create JSONObject to return
        
        JSONArray arr_listResult = new JSONArray();
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                                
              Criteria cr = session.createCriteria(Employee.class, "e")
                                .createAlias("user", "user")
                .add(Restrictions.eq("user.clubs.idClub", Integer.parseInt(idClub)))
                //Create projections
                .setProjection(Projections.projectionList()
                        // Using groupProperty() same GROUP BY in sql
                        .add(Projections.alias(Projections.groupProperty("e.work"), "work"))
                        //Using alias() same AS in sql and using countDistinct like count() DISTINCT
                        .add(Projections.alias(Projections.countDistinct("e.work"), "count"))
                 //Cast data into Statistical
                ).setResultTransformer(Transformers.aliasToBean(CountEmployee.class));
                      
              List<CountEmployee> list = cr.list();
              Iterator<CountEmployee> iterator = list.iterator();
              
              while(iterator.hasNext()){
                  JSONObject result = new JSONObject();  
                  CountEmployee countEmployee = iterator.next();
                  result.put("work", countEmployee.getWork().toString());
                  result.put("count", countEmployee.getCount().toString());
                  
                  arr_listResult.put(result);
              }
        }
        finally{
            //Close transaction 
            session.getTransaction().commit();
        }
        
        return arr_listResult.toString();
    }
}
