package com.services.management.user;

import com.exception.InvalidAccountException;
import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.services.config.configAccount;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.HibernateUtil;
import com.services.management.user.rest.restInterface;
import static com.services.management.user.rest.restInterface.session;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Web Service management account
 *
 * @author Phu Huy
 * @since 20-02-2017
 */
@RestController
@RequestMapping(value = "/account")
public class ServiceUsers implements restInterface{
    
    //key use to encode JWT for each user.
    private final String ENCODE_MEMBER = "heri837rwebf9ru9wbfrir2qg9q2rbweir";
    private final String ENCODE_MANAGER = "jf9fa3w84rujsdfbaslit74sighfw94";
    private final String ENCODE_ADMIN = "8v4u9wrvhwa234cansfbr39wcrc9w";
    //type of user
    private final String MEMBER = "member";
    private final String MANAGER = "manager";
    private final String ADMIN = "admin";
    
    /**
     * Service check for login
     *
     * @param request
     * Data input include:
     *                    - username
     *                    - password
     * @return an instance of java.lang.String return JSONArray result information of account login
     * @throws com.exception.InvalidAccountException
     */
    @Override
    public String doPost(HttpServletRequest request) throws HibernateException, InvalidAccountException{
        
        //Get data input
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        //Create JSONArray to return
        JSONObject result = new JSONObject();
        
        //begin transaction
        session.getTransaction().begin();  
      
        try{
            //Get user from User table by username.
            User user = (User) session.get(User.class, username);  
            //check the result
            //return null when not find results.
            if ((user == null)) {
                throw new InvalidAccountException("Account not exist");
                //return JWT, id, name of user when password check correct by type of user.
            } 
            else if (user.getPassword()
                    .equals(configAccount.encodePasswordToMD5(password,
                     user.getEncodeKey()))) {
                //create jwt by type of user and set value to json object.
                if(user.getTypeOfUser().equals(ADMIN)){
                    result.put("jwt", HibernateUtil.createJWT(user, ENCODE_ADMIN));
                }
                if(user.getTypeOfUser().equals(MANAGER)){
                    result.put("jwt", HibernateUtil.createJWT(user, ENCODE_MANAGER));
                }
                if(user.getTypeOfUser().equals(MEMBER)){
                    result.put("jwt", HibernateUtil.createJWT(user, ENCODE_MEMBER));
                }
                //set name and type of user to json object.
                result.put("username", user.getUsername());
                result.put("club", user.getClubs().getIdClub());
                result.put("type", user.getTypeOfUser());
                
            }
            else {
                throw new InvalidAccountException("Enter username or password wrong");
            }
        }
        finally{
            //close transaction
            session.getTransaction().commit();  
        }
           
            return result.toString();
    }

  /**
     * Service check username. Used in case, account is exist when user register new account
     *
     * @param request
     * Data input include:
     *                    - username
     * @return : an instance of java.lang.String result true or false
     */ 
    @Override
    public String doGet (HttpServletRequest request) throws HibernateException{
        
        //Get data input
        String username = request.getParameter("username");
        
        //Create JSON to return 
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            Criteria cr = session.createCriteria(User.class)
                    .add(Restrictions.eq("username", username));
            List list = cr.list();
            Iterator iterator = list.iterator();
            
            if(iterator.hasNext()){
                result.put("result", "false");
                result.put("pattern", "_");
                //Using hide to turn on/off message
                result.put("hide", true);
                result.put("contentPattern", "Tên đăng nhập đã tồn tại.");
            }
            else{
                result.put("result", "true");
                result.put("pattern", "");
                result.put("hide", false);
            }
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return result.toString();
    }
    
    /**
     * Service change password account
     * 
     * @param request
     * @param body
     * Data input include:
     *                    - username
     *                    - oldPassword
     *                    - newPassword
     * @return an instance of java.lang.String, return JSONArray with content success or change fail
     */
    @Override
    public String doPut (@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
                                                                            throws HibernateException{
               
        //Get data input
        String username = request.getHeader("username");
        String oldPassword = body.getFirst("oldPassword");
        String newPassword = body.getFirst("newPassword");
        
        //Create JSONArray to return
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        try {
            //Get user from User table by username.
            User user = (User) session.load(User.class, username);

            if (user.getPassword().equals(configAccount
                    .encodePasswordToMD5(oldPassword, user.getEncodeKey()))) {

                //random new password encryption key
                String encode = UUID.randomUUID().toString();

                //encode new password 
                newPassword = configAccount.encodePasswordToMD5(newPassword, encode);

                //Input data into user
                user.setPassword(newPassword);
                user.setEncodeKey(encode);

                result.put("result", "success");
            } else {
                result.put("result", "error");
            }
        }  
        
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
    return result.toString();
    }
}