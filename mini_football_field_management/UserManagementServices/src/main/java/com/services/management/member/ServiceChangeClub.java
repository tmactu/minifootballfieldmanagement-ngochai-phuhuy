/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.member;

import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.TypeOfMember;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 01-03-2017
 */
@RestController
@RequestMapping(value = "/member/changeClub")
public class ServiceChangeClub implements restInterface{
    
    /**
     * Service change club account member
     * 
     * @param body
     * @param request
     * Data input include:
     *                    - username
     *                    - idClub
     * @return an instance of java.lang.String describe JSON result return is success
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body,
            HttpServletRequest request) throws HibernateException, NumberFormatException{
        
        //Get data input
        String username = body.getFirst("username");
        String idClub = body.getFirst("idClub");
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Get user from User table by username.
            User user = (User) session.load(User.class, username);
                user.setClubs(new Clubs(Integer.parseInt(idClub)));
            
            // Save object
            session.update(user);
            //close transaction
            session.getTransaction().commit();
            
            //Begin transaction again 
            session.beginTransaction();
            //Refesh table User
            session.refresh(user); 
            
           //create object Member to set information data for Member
            Member member = (Member) session.load(Member.class, username);
                member.setTypeOfMember(new TypeOfMember("Normal_" + idClub));
                member.setTotalHours(0);
                member.setTotalPayments(0);
                
            Criteria cr = session.createCriteria(Reservation.class)
                    .add(Restrictions.eq("member.username", username));
            List list = cr.list();
            Iterator<Reservation> iterator = list.iterator();
            while(iterator.hasNext()){
                Reservation reservation = iterator.next();
                session.delete(reservation); 
            }
                
            //Save object
            session.update(member);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }
}
