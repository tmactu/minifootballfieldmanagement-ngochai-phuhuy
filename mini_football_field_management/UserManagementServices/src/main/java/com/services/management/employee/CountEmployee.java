/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.employee;

/**
 *
 * @author Phu Huy
 */
public class CountEmployee {
    Object work;
    Object count;

    public CountEmployee() {
    }

    public CountEmployee(Object work, Object count) {
        this.work = work;
        this.count = count;
    }

    public Object getWork() {
        return work;
    }

    public Object getCount() {
        return count;
    }

    public void setCount(Object count) {
        this.count = count;
    }

    public void setWork(Object work) {
        this.work = work;
    }
}
