/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.member;

import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import static com.services.management.user.rest.restInterface.SUCCESS;
import static com.services.management.user.rest.restInterface.session;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 04-03-2017
 */
@RestController
@RequestMapping(value = "/member/update")
public class ServiceUpdateMember implements restInterface{
    
    /**
     * Service update account member
     *
     * @param request
     * @param body
     * Data input include:
     *                    - username
     *                    - name
     *                    - birthdate
     *                    - id
     *                    - gender
     *                    - address
     *                    - numberPhone
     *                    - email
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws java.text.ParseException
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request) 
                                                            throws ParseException, HibernateException{

        //Get data input
        String username = body.getFirst("username");
        String name = body.getFirst("name");
        String birthdate = body.getFirst("birthdate");
        String id = body.getFirst("id");
        String gender = body.getFirst("gender");
        String address = body.getFirst("address");
        String numberPhone = body.getFirst("numberPhone");
        String email = body.getFirst("email");
        
        //begin transaction
        session.beginTransaction();
        
        try{
            //Get data from User table by username
            User user = (User) session.get(User.class, username);
            //convert the value of valid data birthdate
            Date d_birthdate = new SimpleDateFormat("yyyy-MM-dd").parse(birthdate);
            //convert the value of valid data boolean
            boolean boo_gender = false;
                if(gender.equals("Nam")){
                    boo_gender = true;
                } 
                
            user.setName(name);
            user.setBirthdate(d_birthdate);
            user.setId(id);
            user.setGender(boo_gender);
            user.setAddress(address);
            user.setNumberPhone(numberPhone);
            user.setEmail(email);
            
            //Update object user
            session.update(user);
        }
        finally{
            //close transaction
            session.getTransaction().commit();
            session.clear();
        }
                    
    return SUCCESS.toString();
    } 
}
