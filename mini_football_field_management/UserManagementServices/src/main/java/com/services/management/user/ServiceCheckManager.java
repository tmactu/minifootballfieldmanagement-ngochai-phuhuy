/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.user;

import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.services.management.user.rest.restInterface;
import static com.services.management.user.rest.restInterface.SUCCESS;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @sine: 13-03-2017
 */
@RestController
@RequestMapping(value = "account/checkManager")
public class ServiceCheckManager implements restInterface{
    /**
     * Service check account manager
     *
     * @param request
     * @return an instance of java.lang.String, return JSONArray result with content success.
     **/
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
        
        return SUCCESS.toString();
    }
}
