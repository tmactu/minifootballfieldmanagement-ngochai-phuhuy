/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.employee;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Employee;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 27-02-2017
 */
@RestController
@RequestMapping(value = "/employee")
public class ServiceEmployees implements restInterface{
    
    /**
     * Service add information employee in club
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager in club
     *                    - club: id club of manager manage
     *                    - name
     *                    - birthdate
     *                    - id
     *                    - gender
     *                    - address
     *                    - numberPhone
     *                    - email
     *                    - work
     *                    - salary
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws java.text.ParseException
     */    
    @Override
    public String doPost(HttpServletRequest request) throws ParseException, HibernateException,
                                                    NumberFormatException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String name = request.getParameter("name");
        String birthdate = request.getParameter("birthdate");
        String id = request.getParameter("id");
        String gender = request.getParameter("gender");
        String address = request.getParameter("address");
        String numberPhone = request.getParameter("numberPhone");
        String email = request.getParameter("email");
        String work = request.getParameter("work");        
        String salary = request.getParameter("salary");        
        
        //Convert data input
        Date d_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(birthdate);
        boolean boo_gender = false;
            if(gender.equals("Nam"))
                boo_gender = true;
        Date d_startDate = new Date();       
        
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            //Create username for employee  
            /*sql =
            select * from employee where `USERNAME` in 
                    (
                        select distinct e.`USERNAME`
                        from employee e, `user` u
                        where (e.`USERNAME` = u.`USERNAME`) and
                        (
                            length(e.`USERNAME`) >=
                            (
                                select distinct max(length(`USERNAME`))
                                from employee where `USERNAME` in
                                ( 
                                    select `USERNAME` from `user` 
                                    where (`ID_CLUB` = 15) and (`TYPE_OF_USER` = 'employee')
                                )
                            )
                        ) 
                    and (u.`ID_CLUB` = 15)
                    ) ORDER BY `USERNAME` desc;*/
            String sql = "SELECT * FROM employee WHERE `USERNAME` IN ( SELECT DISTINCT e.`USERNAME` FROM employee e, `user` u WHERE (e.`USERNAME` = u.`USERNAME`) AND ( LENGTH(e.`USERNAME`) >= ( SELECT DISTINCT MAX(LENGTH(`USERNAME`)) FROM employee WHERE `USERNAME` IN ( SELECT `USERNAME` FROM `user` WHERE (`ID_CLUB` = :idClub) AND (`TYPE_OF_USER` = 'employee') ) ) ) AND (u.`ID_CLUB` = :idClub) ) ORDER BY `USERNAME` DESC";
            SQLQuery query = session.createSQLQuery(sql);
                query.addEntity(Employee.class);
                query.setParameter("idClub", idClub);
            List list = query.list();
            Iterator<Employee> iterator = list.iterator();
            Employee emp = iterator.next();
            String oldUsername = emp.getUsername();
        
            //Cut string username
            int index = oldUsername.indexOf(".");        
            oldUsername = oldUsername.substring(index + 1);

            String usernameEmployee = "NV_" + idClub + "." + (Integer.parseInt(oldUsername) + 1);    
         
            
            //Get data to User table
            User user = new User();
                user.setUsername(usernameEmployee);
                user.setPassword("none");
                user.setEncodeKey("none");
                user.setName(name);
                user.setBirthdate(d_birthdate);
                user.setId(id);
                user.setGender(boo_gender);
                user.setAddress(address);
                user.setNumberPhone(numberPhone);
                user.setEmail(email);
                user.setTypeOfUser("employee");
                user.setClubs(new Clubs(Integer.parseInt(idClub)));
                
            // Save object
            session.save(user);
            //close transaction
            session.getTransaction().commit();

            //Begin transaction again 
            session.beginTransaction();
            //Refesh table User
            session.refresh(user); 
            
            //Get data to Employee table
            Employee employee = new Employee();
                employee.setUser(user);
                employee.setSalary(Integer.parseInt(salary));
                employee.setStartingDate(d_startDate);
                employee.setWork(work);
            // Save object
            session.save(employee);  
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        return SUCCESS.toString();
    }
    /**
     * Service update information employee
     *
     * @param body
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager in club
     *                    - club: id club of manager manage
     *                    - usernameEmployee
     *                    - name
     *                    - birthdate
     *                    - id
     *                    - gender
     *                    - address
     *                    - numberPhone
     *                    - email
     *                    - salary
     *                    - startDate
     *                    - coefficientSalary
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws java.text.ParseException
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request) 
                            throws ParseException, HibernateException, InvalidManagerException{

        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String usernameEmployee = body.getFirst("username");
        String name = body.getFirst("name");
        String birthdate = body.getFirst("birthdate");
        String id = body.getFirst("id");
        String gender = body.getFirst("gender");
        String address = body.getFirst("address");
        String numberPhone = body.getFirst("numberPhone");
        String email = body.getFirst("email");        
        String work = body.getFirst("work");
        String salary = body.getFirst("salary");
        
        //convert the value of valid data birthdate
        Date d_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(birthdate);
        //convert the value of valid data boolean
        boolean boo_gender = false;
        if(gender.equals("Nam")){
            boo_gender = true;
        }    
        
        //begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            //Get data from User table by username
            User user = (User) session.load(User.class, usernameEmployee);
                    user.setName(name);
                    user.setBirthdate(d_birthdate);
                    user.setId(id); 
                    user.setGender(boo_gender);
                    user.setAddress(address);
                    user.setNumberPhone(numberPhone);
                    user.setEmail(email);
                    //Update object user
                    session.update(user);   

            //Get data from Employee table by username
            Employee employee = (Employee) session.get(Employee.class, usernameEmployee);
                    employee.setSalary(Integer.parseInt(salary));
                    employee.setWork(work); 
                    //Update object user
                    session.update(employee);
        }
        finally{
            //close transaction
            session.getTransaction().commit();
        }
                    
        return SUCCESS.toString();
    }     
    /**
     * Service delete employee
     *
     * @param body
     * @param request
     * Data input include:
     *                 - jwt
     *                 - username: username of manager in club
     *                 - club: id club of manager manage
     * @return an instance of java.lang.String, return JSONArray result with content success.
     */
    @Override
    public String doDelete (@RequestBody MultiValueMap<String, String> body, 
                            HttpServletRequest request) throws ObjectNotFoundException, 
                            HibernateException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String usernameEmployee = body.getFirst("username");
        
        //begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            //Get user from User table by username.
            User user = (User) session.load(User.class, usernameEmployee);
            Employee employee = (Employee) session.load(Employee.class, usernameEmployee);
                
            //Delete object 
            session.delete(employee);
            session.delete(user);
        }
        finally{
            //close transaction
            session.getTransaction().commit();
        }
            
        return SUCCESS.toString(); 
    }
    /**
     * Service get information all employee in club
     *
     * @param request
     * Data input include:
     *              - username
     * @return an instance of java.lang.String, return JSONArray result with content is 
     * all information employee in club
     * ?parameter=...
     */
    @Override
    public String doGet (HttpServletRequest request) throws HibernateException, 
                                    ObjectNotFoundException, InvalidManagerException {
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
                
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
        
        // begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            // Create Criteria have function filter data in User
            Criteria cr = session.createCriteria(User.class)
                    .add(Restrictions.ilike("typeOfUser", "emp"))
                    .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)));
                    
            List<User> user = (List<User>) cr.list();
            Iterator<User> iterator = user.iterator();
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                //Get each user information with username was found
                User usr = iterator.next();
                    result.put("username", usr.getUsername());
                    result.put("club", usr.getClubs().getIdClub());
                    result.put("name", usr.getName());
                    result.put("birthdate", usr.getBirthdate().toString());
                    result.put("id", usr.getId());
                    result.put("gender", usr.getGender());
                    result.put("address", usr.getAddress());
                    result.put("numberPhone", usr.getNumberPhone());
                    result.put("email", usr.getEmail());
                
                Employee employee = (Employee) session.get(Employee.class, usr.getUsername());
                    result.put("work", employee.getWork());
                    result.put("salary", employee.getSalary());
                    result.put("startingDate", employee.getStartingDate().toString());
                    
                arr_listResult.put(result);
            }
        }
        finally{
            // close transaction
            session.getTransaction().commit();
        }
        
        return arr_listResult.toString();
    }  
}
