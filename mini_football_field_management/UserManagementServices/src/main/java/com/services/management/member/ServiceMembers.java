/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.member;


import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Web Service management member
 *
 * @author Phu Huy
 * @since 04-02-2017
 */
@RestController
@RequestMapping("/member")
public class ServiceMembers implements restInterface{
    
    /**
     * Service delete account member
     *
     * @param request
     * Data input include:
     *             - parameter: username which want to delete
     * @param body
     * Data input include:
     *             - jwt
     *             - username: username of manager in club
     *             - club: id club of manager manage
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doDelete (@RequestBody MultiValueMap<String, String>  body, 
            HttpServletRequest request) throws HibernateException, ObjectNotFoundException, 
                                                                        InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String parameter = body.getFirst("parameter");
        System.out.println(body.toSingleValueMap().toString());
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
            
        //begin transaction
        session.beginTransaction();
            
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            //Get user from User table by username.
            User user = (User) session.load(User.class, parameter);
            
            //Get member from Member table by username.
            Member member = (Member) session.load(Member.class, parameter);
            
            //Create iterator check reservation belong to username exists or not
            Iterator reservation = member.getReservations().iterator();
            
           //When have foreign key in reservation, update username before delete
            if(reservation.hasNext()){
                //Using loops to update each reservation has a foreign key field in the username
                while(reservation.hasNext()){
                    Reservation rs = (Reservation) reservation.next();
                        rs.setMember(new Member("none"));
                    //Update object
                    session.saveOrUpdate(rs);
                }
                session.flush();
                //Refresh object 
                session.refresh(reservation);
            }
            
            //Delete object 
            session.createSQLQuery("delete from mini_football_field_management.member where USERNAME='" + member.getUsername() + "'").executeUpdate();
            session.createSQLQuery("delete from mini_football_field_management.user where USERNAME='" + member.getUsername() + "'").executeUpdate();
            session.flush();
            arr_listResult.put(SUCCESS);
        }
        finally{
            //close transaction
            session.getTransaction().commit();
            session.clear();
        }    
        
        return arr_listResult.toString();
    }
    /**
     * Service find account member
     *
     * @param request
     * Data input include:
     *              - jwt
     *              - username: username of manager in club
     *              - club: id club of manager manage
     *              - parameter: username or name member need to find
     *  
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * ?parameter=...
     * @throws com.exception.InvalidManagerException
     * @throws UnsupportedEncodingException 
     * @throws JSONException 
     */
    @Override
    public String doGet (HttpServletRequest request)
                                throws HibernateException, InvalidManagerException, JSONException, UnsupportedEncodingException{
        //Get data intput
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String parameter = request.getParameter("parameter");
		if(parameter != ""){
			Base64.Decoder dec= Base64.getDecoder();
			byte[] strdec=dec.decode(parameter); 
			parameter = new String(strdec,"UTF-8");
		}
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
              
        // begin transaction
        session.beginTransaction();
        
        try{   
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            // Create Criteria have function filter data in User
            Criteria cr = session.createCriteria(User.class)
                .createAlias("member", "member")
                .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                .addOrder(Order.desc("member.lastOrderDate")); 
            
            //looking for username
            Criterion cr_username = Restrictions.like("username", "%" + parameter + "%");
            //looking for name
            Criterion cr_name = Restrictions.like("name", "%" + parameter + "%");
            
            //Check data input is username or name
            LogicalExpression orExp = Restrictions.or(cr_username, cr_name);
            
            // To get records matching with OR conditions
            cr.add(orExp);
           
            List<User> user = (List<User>) cr.list();
            Iterator<User> iterator = (Iterator<User>) user.iterator();
            
            while(iterator.hasNext()){
                
                JSONObject result = new JSONObject();
                //Get each user information with username was found
                User usr = iterator.next();
                    result.put("username", usr.getUsername());
                    result.put("club", usr.getClubs().getIdClub());
                    result.put("name", usr.getName());
                    result.put("birthdate", usr.getBirthdate().toString());
                    result.put("id", usr.getId());
                    result.put("gender", usr.getGender().toString());
                    result.put("address", usr.getAddress());
                    result.put("numberPhone", usr.getNumberPhone());
                    result.put("email", usr.getEmail());
                
                Member member = (Member) session.load(Member.class, usr.getUsername());
                String type = member.getTypeOfMember().getTypeOfMember().substring(0, member.getTypeOfMember().getTypeOfMember().indexOf("_"));
                    result.put("totalHours", member.getTotalHours());
                    result.put("totalPayments", member.getTotalPayments());
                    result.put("lastOrderDate", member.getLastOrderDate().toString());
                    result.put("typeOfMember", type);
                System.out.println(result);
                arr_listResult.put(result);
            }
        }
        catch(ObjectNotFoundException e){
            JSONObject result = new JSONObject();
                result.put("result", "No result");
                arr_listResult.put(result);
        }
        finally{       
            // close transaction
            session.getTransaction().commit();
        }
        
    return arr_listResult.toString();
   }
}
