/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.manager;

import com.exception.InvalidAdminException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Employee;
import com.model.hibernate.POJO.User;
import com.services.config.configAccount;
import com.services.management.user.rest.restInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * REST Web Service management account of manager.
 *
 * @author Phu Huy
 * @since 08/02/2017
 */
@RestController
@RequestMapping(value = "/manager")
public class ServiceManagers implements restInterface{
    
    /**
     * Service add manager account, this services for admin
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: account of admin
     *                    - usernameManager
     *                    - password
     *                    - name
     *                    - birthdate
     *                    - id
     *                    - gender
     *                    - address
     *                    - numberPhone
     *                    - email
     *                    - Club
     *                    - startDate
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws java.text.ParseException
     */    
    @Override
    public String doPost (HttpServletRequest request)
           throws HibernateException, ParseException, NumberFormatException, InvalidAdminException{    
        
        String usernameManager = request.getParameter("username");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String birthdate = request.getParameter("birthdate");
        String id = request.getParameter("id");
        String gender = request.getParameter("gender");
        String address = request.getParameter("address");
        String numberPhone = request.getParameter("numberPhone");
        String email = request.getParameter("email");
        String club = request.getParameter("club");
                
        //random password encryption key
        String encode = UUID.randomUUID().toString();
        //encode MD5 for password
        password = configAccount.encodePasswordToMD5(password, encode);
        
        //convert data before input data into User table
        Date d_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(birthdate);
        boolean boo_gender = false;
            if(gender.equals("Nam"))
                boo_gender = true;
        
        //Begin transaction
        session.beginTransaction();
        try{           
            
            //create object User to set information data for User
            User manager = new User();
                manager.setUsername(usernameManager);
                manager.setPassword(password);
                manager.setEncodeKey(encode);
                manager.setName(name);
                manager.setBirthdate(d_birthdate);
                manager.setId(id);
                manager.setGender(boo_gender);
                manager.setAddress(address);
                manager.setNumberPhone(numberPhone);
                manager.setEmail(email);
                manager.setTypeOfUser("manager");
                manager.setClubs(new Clubs(Integer.parseInt(club)));

            // Save object User
            session.save(manager);
        }
        finally{
            //close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }
    
    /**
     * Service update account member
     *
     * @param body
     * Data input include:
     *                    - username
     *                    - name
     *                    - birthdate
     *                    - id
     *                    - gender
     *                    - address
     *                    - numberPhone
     *                    - email
     *                    - club
     * @param request
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws java.text.ParseException
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, 
                        HttpServletRequest request) throws ParseException, HibernateException, 
                                                                            NumberFormatException{
        
        //Get data input
        String username = body.getFirst("username");
        String name = body.getFirst("name");
        String birthdate = body.getFirst("birthdate");
        String id = body.getFirst("id");
        String password = body.getFirst("password");
        String gender = body.getFirst("gender");
        String address = body.getFirst("address");
        String numberPhone = body.getFirst("numberPhone");
        String email = body.getFirst("email");
        String Club = body.getFirst("club");
                
        //begin transaction
        session.beginTransaction();
        
        try{             
            
                    //---- Set data into User table ----//
                    
            //Get data from User table by username
            User manager = (User) session.load(User.class, username);
            //convert the value of valid data birthdate
            Date d_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(birthdate);
            //convert the value of valid data boolean
            boolean boo_gender = false;
                if(gender.equals("Nam")){
                    boo_gender = true;
                }    
            //convert the value of valid data CauLacBo(int)
            int idClub = Integer.parseInt(Club);
            
            manager.setName(name);
            manager.setBirthdate(d_birthdate);
            if (password != null) {
                //random password encryption key
                String encode = UUID.randomUUID().toString();
                //encode MD5 for password
                password = configAccount.encodePasswordToMD5(password, encode);
                manager.setPassword(password);
                manager.setEncodeKey(encode);
            }            
            manager.setId(id);
            manager.setGender(boo_gender);
            manager.setAddress(address);
            manager.setNumberPhone(numberPhone);
            manager.setEmail(email);
            manager.setClubs(new Clubs(idClub));
            
            //Update object user
            session.update(manager);  
        }
        finally{
            //close transaction
            session.getTransaction().commit();
        }    
        
        return SUCCESS.toString();
    } 
    
    /**
     * Service delete account manager, this servicer for admin
     *
     * @param request
     * Data input include:
     *              - jwt
     *              - username: username of admin
     * @param body
     * Data input include:
     *              - usernameManager
     * @return an instance of java.lang.String, return JSONArray result with content success.
     */
    @Override
    public String doDelete (@RequestBody MultiValueMap<String, String>  body, 
                            HttpServletRequest request) throws HibernateException, InvalidAdminException{
            
        //Get data input
        String username = request.getHeader("username");
        String usernameManager = body.getFirst("usernameManager");
                    
        //begin transaction
        session.beginTransaction();
        
        try{
            //Checking account is belong to admin or not
            User admin = (User) session.get(User.class, username);
                if(!admin.getTypeOfUser().equals("admin"))
                    throw new InvalidAdminException("Must use account admin to do this");
            
            //Get user from User table by username.
            User user = (User) session.load(User.class, usernameManager);
            
            //Get member from Member table by username.
            Employee manager = (Employee) session.load(Employee.class, usernameManager);
            
            //Delete object 
            session.delete(manager);
            session.delete(user);
        }
        finally{
            //close transaction
            session.getTransaction().commit();
        }    
        
        return SUCCESS.toString();
    }
    /**
     * Service find account manager, This service for admin
     *
     * @param request
     * Data input include:
     *              - jwt
     *              - username: username of admin
     *              - parameter: username or name which want to find
     * @return an instance of java.lang.String, return JSONArray result with content manager
     * information need to find
     * ?parameter=...
     */
    @Override
    public String doGet (HttpServletRequest request)
                  throws HibernateException, InvalidAdminException{
        
        //Get data input
        String username = request.getHeader("username");
        String parameter = request.getParameter("parameter");
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
                
        // begin transaction
        session.beginTransaction();
        
        try{    
            //Checking account is belong to admin or not
            User admin = (User) session.get(User.class, username);
                if(!admin.getTypeOfUser().equals("admin"))
                    throw new InvalidAdminException("Must use account admin to do this");
            
            // Create Criteria have function filter data in User
            Criteria cr = session.createCriteria(User.class);
            
            Criterion cr_username = Restrictions.ilike("username", "%" + parameter + "%");
            Criterion cr_name = Restrictions.ilike("name", "%" + parameter + "%");
            
            //Check data input is username or name
            LogicalExpression orExp = Restrictions.or(cr_username, cr_name);
            
            // To get records matching with OR conditions
            cr.add(orExp);
            //Check type of user is MEMBER or not
            cr.add(Restrictions.ilike("typeOfUser", "manager"));
            List<User> user = (List<User>) cr.list();
            Iterator<User> iterator =  user.iterator();
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                //Get each user information with username was found
                User usr = iterator.next();
                    result.put("username", usr.getUsername());
                    result.put("club", usr.getClubs().getIdClub());
                    result.put("name", usr.getName());
                    result.put("birthdate", usr.getBirthdate().toString());
                    result.put("id", usr.getId());
                    result.put("gender", usr.getGender().toString());
                    result.put("address", usr.getAddress());
                    result.put("numberPhone", usr.getNumberPhone());
                    result.put("email", usr.getEmail());
                    
                arr_listResult.put(result);
            }
        }
        finally{
            // close transaction
            session.getTransaction().commit();
        }
            
        return arr_listResult.toString();
    }
}
