/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.member;

import com.exception.InvalidManagerException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.TypeOfMember;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 13-03-2017
 */
@RestController
@RequestMapping(value = "/member/getSumOfMember")        
public class ServiceGetSumOfMember implements restInterface{
    /**
     * Service get sum of member in club
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of manager in club
     *                  - club: id club of manager manage
     * @return an instance of java.lang.String, result return is JSONArray result 
     * with content number member in club
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, NumberFormatException,
                                                                                InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        //Create JSONObject to return
        JSONArray arr_listResult = new JSONArray();
      
        //Begin transaction
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                   
            Criteria cr = session.createCriteria(TypeOfMember.class)
                    .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)));
              List list = cr.list();
              Iterator iterator = list.iterator();
              List listTypeOfMember = new ArrayList();
              
                // Get list with data is all type of member in this club 
                while(iterator.hasNext()){
                    TypeOfMember typeOfMember = (TypeOfMember) iterator.next();
                    listTypeOfMember.add(typeOfMember.getTypeOfMember());
                }
                // Count each type of member in this club 
                for(Object type : listTypeOfMember){
                    Criteria cr_TypeOfMember = session.createCriteria(Member.class)
                        .add(Restrictions.eq("typeOfMember.typeOfMember", type.toString())) 
                    //Using like COUNT in sql
                    .setProjection(Projections.rowCount());
                    List listCount = cr_TypeOfMember.list();
                    int index = type.toString().indexOf("_");
                    JSONObject result = new JSONObject();   
                        result.put("type", type.toString().substring(0, index));
                        result.put("number", listCount.get(0));
                                
                        arr_listResult.put(result);
                }
        }
        catch(IndexOutOfBoundsException ex){
                  System.out.println(ex.getMessage());
        }
        finally{
            //Close transaction 
            session.getTransaction().commit();
        }
        
    return arr_listResult.toString();
    }
}
