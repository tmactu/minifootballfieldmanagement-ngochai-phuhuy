/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.user;

import com.exception.InvalidAccountException;
import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.TypeOfMember;
import com.model.hibernate.POJO.User;
import com.services.config.configAccount;
import com.services.management.user.rest.restInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 01-03-2017
 */
@RestController
@RequestMapping(value = "/account/register")
public class ServiceRegister implements restInterface{
    
    /**
     * Service register account member
     *
     * @param request
     * Data input include:
     *                    - username
     *                    - password
     *                    - name
     *                    - birthdate
     *                    - id
     *                    - gender
     *                    - address
     *                    - numberPhone
     *                    - email
     *                    - club
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws java.text.ParseException
     */ 
    @Override
    public String doPost(HttpServletRequest request) throws HibernateException, 
                                            ParseException, InvalidAccountException{
        
        //Get data input
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String birthdate = request.getParameter("birthdate");
        String id = request.getParameter("id");
        String gender = request.getParameter("gender");
        String address = request.getParameter("address");
        String numberPhone = request.getParameter("numberPhone");
        String email = request.getParameter("email");
        String idClub = request.getParameter("club");
        String typeOfMember = request.getParameter("typeOfMember");
        
        //Cheking user using is customer or manager
        if(idClub == null){
            idClub = request.getHeader("club");
        }

        //random password encryption key
        String encode = UUID.randomUUID().toString();
        //encode MD5 for password
        password = configAccount.encodePasswordToMD5(password, encode);

        //convert the value of valid data
//        Date d_birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(birthdate);
        Date d_birthdate = new SimpleDateFormat("yyyy-MM-dd").parse(birthdate);
        boolean boo_gender = false;
            if(gender.equals("Nam"))
                boo_gender = true;
                        
        //begin transaction
        session.beginTransaction();
        
        try{
            //create object User to set information data for User
            User user = new User();
                user.setUsername(username);
                user.setPassword(password);
                user.setEncodeKey(encode);
                user.setName(name);
                user.setBirthdate(d_birthdate);
                user.setId(id);
                user.setGender(boo_gender);
                user.setAddress(address);
                user.setNumberPhone(numberPhone);
                user.setEmail(email);
                user.setTypeOfUser("member");
                user.setClubs(new Clubs(Integer.parseInt(idClub)));
                
            // Save object
            session.save(user);
            //close transaction
            session.getTransaction().commit();
            
            //Begin transaction again 
            session.beginTransaction();
            //Refesh table User
            session.refresh(user); 
            
           //create object Member to set information data for Member
            Member member = new Member();
                member.setUser(user);
                member.setUsername(username);
                member.setTypeOfMember(new TypeOfMember(typeOfMember + "_" + idClub));
                member.setTotalHours(0);
                member.setTotalPayments(0);
                member.setLastOrderDate(new Date());
                
            //Save object
            session.save(member);
        }
        finally{
            //close transaction
            session.getTransaction().commit();
            session.clear();
        }    
           
    return SUCCESS.toString();
    }
}
