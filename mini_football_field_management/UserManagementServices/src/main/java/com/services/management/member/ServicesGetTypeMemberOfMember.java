/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.member;

import com.model.hibernate.POJO.Member;
import com.services.management.user.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since: 16-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/member/GetTypeMemberOfMember")
public class ServicesGetTypeMemberOfMember implements restInterface{
    /**
     * Service get information member such as typeOfmeber, total hour and total payment 
     *
     * @param request
     * @return an instance of java.lang.String, result return JSONObject with content is typeOfmeber, total hour and total payment 
     */
    
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException{
        
        //Get data innput
        String username = request.getParameter("username");
        
        //Create JSONObject to return
        JSONObject result = new JSONObject();
        
        //Begin transaction
        session.beginTransaction();
        
        try{            
            Criteria cr = session.createCriteria(Member.class)
                    .add(Restrictions.eq("username", username));
            List list = cr.list();
            Iterator<Member> iterator =  list.iterator();
            Member member = iterator.next();
            
            String typeOfMember = member.getTypeOfMember().getTypeOfMember();
            int index = typeOfMember.indexOf("_");           
            
            result.put("typeOfMember",typeOfMember.substring(0, index));
            result.put("totalHour",member.getTotalHours());
            result.put("totalPayment",member.getTotalPayments());            
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return result.toString();
    }    
    
}
