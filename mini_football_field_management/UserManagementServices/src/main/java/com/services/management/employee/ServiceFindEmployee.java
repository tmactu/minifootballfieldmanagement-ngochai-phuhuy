/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.employee;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Employee;
import com.model.hibernate.POJO.User;
import com.services.management.user.rest.restInterface;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @since 15-03-2017
 */
@RestController
@RequestMapping(value = "/employee/find")
public class ServiceFindEmployee implements restInterface{
    /**
     * Service find account employee
     *
     * @param request
     * Data input include:
     *              - jwt
     *              - username: username of manager in club
     *              - club: id club of manager manage
     *              - parameter: username or name member need to find
     *  
     * @return an instance of java.lang.String, return JSONArray result with content employee
     * information need to find.
     * ?parameter=...
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doGet (HttpServletRequest request)
                                throws HibernateException, InvalidManagerException{
        
        //Get data intput
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String parameter = request.getParameter("parameter");
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
              
        // begin transaction
        session.beginTransaction();
        
        try{   
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            //Create Criteria have function filter data in User
            Criteria cr = session.createCriteria(User.class)
                .createAlias("employee", "employee")
                .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                .addOrder(Order.desc("employee.startingDate"));
            
            //Looking for username
            Criterion cr_username = Restrictions.ilike("username", "%" + parameter + "%");
            //Looking for name
            Criterion cr_name = Restrictions.ilike("name", "%" + parameter + "%");
            //Check data input is username or name
            LogicalExpression orExp = Restrictions.or(cr_username, cr_name);
            //To get records matching with OR conditions
            cr.add(orExp);
           
            List<User> list = (List<User>) cr.list();
            Iterator<User> iterator = list.iterator();
            while(iterator.hasNext()){
                JSONObject result = new JSONObject();
                //Get each user information with username was found
                User usr = iterator.next();
                    result.put("club", usr.getClubs().getIdClub());
                    result.put("name", usr.getName());
					result.put("username", usr.getUsername());
                    result.put("birthdate", usr.getBirthdate().toString());
                    result.put("id", usr.getId());
                    result.put("gender", usr.getGender().toString());
                    result.put("address", usr.getAddress());
                    result.put("numberPhone", usr.getNumberPhone());
                    result.put("email", usr.getEmail());
                
                Employee employee = (Employee) session.get(Employee.class, usr.getUsername());
                    result.put("work", employee.getWork());
                    result.put("salary", employee.getSalary());
                    result.put("startingDate", employee.getStartingDate().toString());
                    
                arr_listResult.put(result);
            }
        }
        catch(ObjectNotFoundException e){
            JSONObject result = new JSONObject();
                result.put("result", "No result");
            arr_listResult.put(result);
        }
        finally{       
            // close transaction
            session.getTransaction().commit();
        }
        
        return arr_listResult.toString();
   }
}
