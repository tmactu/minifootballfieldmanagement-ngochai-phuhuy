/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.handler.exception;

import com.exception.InvalidAccountException;
import com.exception.InvalidAdminException;
import com.exception.InvalidManagerException;
import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Hai
 */
@ControllerAdvice
public class ExceptionHandlerServices {
    @ExceptionHandler(HibernateException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody String handleHibernateException(HibernateException ex) {
        return new JSONObject().put("error", ex.getMessage()).toString();
    }

    @ExceptionHandler({NumberFormatException.class, ParseException.class})
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    public @ResponseBody String handleFormatException(Exception ex) {
        return new JSONObject().put("error", ex.getMessage()).toString();
    }
    
    @ExceptionHandler(InvalidRequestException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody String handleOwnedException(InvalidRequestException ex) {
        return new JSONObject().put("error", ex.getErrText()).toString();
    }
    
    @ExceptionHandler(RequestNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody String handleRequestException(RequestNotFoundException ex) {
        Logger.getLogger(RequestNotFoundException.class.getName()).log(Level.SEVERE, null, ex);
        return new JSONObject().put("error", "Requested not found").toString();
    }
    
    @ExceptionHandler(InvalidManagerException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody String handleOwnedException(InvalidManagerException ex) {
        Logger.getLogger(InvalidManagerException.class.getName()).log(Level.SEVERE, null, ex);
        return new JSONObject().put("error", ex.getError()).toString();
    }
    
    @ExceptionHandler(InvalidAccountException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody String handleOwnedException(InvalidAccountException ex) {
        Logger.getLogger(InvalidAccountException.class.getName()).log(Level.SEVERE, null, ex);
        return new JSONObject().put("error", ex.getError()).toString();
    }
    
    @ExceptionHandler(InvalidAdminException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody String handleOwnedException(InvalidAdminException ex) {
        Logger.getLogger(InvalidAdminException.class.getName()).log(Level.SEVERE, null, ex);
        return new JSONObject().put("error", ex.getError()).toString();
    }
}
