/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.handler.error.authentication;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *Service return status 401 Unauthorized
 * @author Phu Huy
 */
@RestController
public class AuthenticationError {
    @RequestMapping("/error")
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody String returnError(){
        return new JSONObject().put("error", "Authentication error").toString();
    }
}
