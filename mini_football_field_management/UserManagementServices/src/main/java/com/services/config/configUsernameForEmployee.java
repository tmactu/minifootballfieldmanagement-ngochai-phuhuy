/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.config;

import com.model.hibernate.POJO.Employee;
import com.model.hibernate.connect.HibernateUtil;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

/**
 *
 * @author Phu Huy
 * @since: 27-02-2017
 */
public class configUsernameForEmployee {
    /**
     * Function create max id  for employee of table employee
     *
     * @param className: class name (table name).
     * @param id:  attribute name (column name) need create.
     * @return an instance of java.lang.Integer is max + 1 id of column.
     */
    public static String createUsernameForEmployee(int idClub) throws HibernateException{

        //Get sessionFactory from Hibernate
        SessionFactory factory = HibernateUtil.getSessionFactory();

        Session session = factory.openSession();
            //begin transaction
            session.getTransaction().begin();
            
            /* ---------------------
            
            select * from employee 
                where length(`USERNAME`) >= (select distinct max(length(`USERNAME`)) from employee)
                order by `USERNAME` desc;

            -----------------------*/
            //Create criteria
            
            idClub = 15;
        
            Criteria criteria = session.createCriteria(Employee.class)
                //Using propetyIn() like IN() in sql 
                .add(Restrictions.sqlRestriction(
                        "SELECT distinct e.* " +
                        "FROM employee e, `user` u" +
                        "where (e.`USERNAME` = u.`USERNAME`) and" +
                        "( " +
                        "    length(e.`USERNAME`) >= " +
                        "    ( "+
                        "        select distinct max(length(`USERNAME`)) " +
                        "            from employee where `USERNAME` in "+
                        "            ( " +
                        "               select `USERNAME` from `user` " +
                        "               where (`ID_CLUB` = " + idClub + ") and (`TYPE_OF_USER` = 'employee') " +
                        "            ) " +
                        "    ) " +
                        ") " +
                        "and (u.`ID_CLUB` = " + idClub + ") ORDER BY e.`USERNAME` desc; "     
                ));
        
                List list = criteria.list();
                Iterator<Employee> iterator = list.iterator();

                Employee emp = iterator.next();
            
            String username = emp.getUsername();
            int index = username.indexOf('.');
                //Cut string nv in username
                username = username.substring(index + 1);
            int number = Integer.parseInt(username);
                number++;

             DetachedCriteria subquery = DetachedCriteria.forClass(Employee.class)
                     .setProjection(Projections.max("username"));
            
            //delete buffer
            session.flush();
            //close transaction
            session.getTransaction().commit();
            return "NV_" + idClub + "." + number;
    }
}
