/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exception;

/**
 * Using check username which belong to manager or not
 * @author Phu Huy
 * @since 01-03-2017
 */
public class InvalidManagerException extends Exception{
    private final String error;

    public InvalidManagerException(String error) {
        this.error = error;
    }

    public InvalidManagerException() {
        this.error = null;
    }
    
    public String getError() {
        return error;
    }
    
    
    
}
