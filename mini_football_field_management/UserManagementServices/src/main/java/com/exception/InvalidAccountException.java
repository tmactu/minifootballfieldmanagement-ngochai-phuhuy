/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exception;

/**
 *
 * @author Phu Huy
 */
public class InvalidAccountException extends Exception{
    String error;
   
    public InvalidAccountException(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
