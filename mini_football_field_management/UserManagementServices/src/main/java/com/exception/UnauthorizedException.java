/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exception;

import org.json.JSONObject;

/**
 *
 * @author Hai
 */
public class UnauthorizedException extends Exception {

	private static final long serialVersionUID = 1L;

	private String errCode;

	public UnauthorizedException(String errCode) {
		this.errCode = errCode;
	}
        
        @Override
        public String toString(){
            JSONObject ob = new JSONObject();
            ob.append("Code", errCode);
            return ob.toString();
        }
}
