/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.handler.error.expired.JWT;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hai
 */
@RestController
public class expiredJwt {
    @RequestMapping("/expired")
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public @ResponseBody String returnError(){
        return new JSONObject().put("error", "Expired JWT").toString();
    }
}