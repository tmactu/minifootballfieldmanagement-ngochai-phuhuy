/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.provinces;

import com.model.hibernate.POJO.Provinces;
import com.model.hibernate.connect.HibernateUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hai
 */
@RestController
@RequestMapping("/backup")
public class BackupDataBase {
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String addProvince(HttpServletRequest request) {        
        String executeCmd = "\"C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\mysqldump\" -u root -phai123 mini_football_field_management -r database.sql";
        Process runtimeProcess;
        try { 
            runtimeProcess = Runtime.getRuntime().exec(executeCmd);            
            //check process complete
           InputStream in = runtimeProcess.getErrorStream();
            int c;
            while ((c = in.read()) != -1)
            {

                System.out.print((char)c);
            }
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "[{result : fail}]";
        }

        return "[{result : success}]";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    String downoadFile(HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException {
        
        String filePath = "database.sql";
        File downloadFile = new File(filePath);
        FileInputStream inStream = new FileInputStream(downloadFile);

        String mimeType = "application/octet-stream";      

        // modifies response
        response.setContentType(mimeType);
        response.setContentLength((int) downloadFile.length());

        // forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);

        // obtains response's output stream
        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inStream.close();
        outStream.close();
        return "success";
    }
    
}
