/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.provinces;

import com.model.hibernate.POJO.Provinces;
import com.model.hibernate.connect.HibernateUtil;
import java.util.Iterator;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Web Service management province.
 *
 * @author Tran Ngoc Hai
 * @since 27/02/2017
 */
@RestController
@RequestMapping("/rest")
public class ProvinceServices {

    //Session from Hibernate
    Session session = HibernateUtil.getSessionFactory().openSession();
    //String is returned when processing success.
    final String SUCCESS = new JSONObject().append("result", "success").toString();

    /**
     * Service add new province into database.
     *
     * @param provinceName: province name.
     * @return Json string success.
     */
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String addProvince(@RequestParam String province) {            
        int id = HibernateUtil.createID(new Provinces(), "idProvince");
        session.beginTransaction();
        try {
            Provinces temp = new Provinces(id);
            temp.setProvince(province);
            session.save(province);
        } finally {
            session.getTransaction().commit();
        }
        return SUCCESS;
    }

    /**
     * Service delete province.
     *
     * @param provinceName: province name.
     * @return Json string success.
     */
    @RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String removeProvince(@RequestBody MultiValueMap<String, String> provinceName) {
        session.beginTransaction();
        try {
            Provinces province = (Provinces) session.load(Provinces.class, Integer.parseInt(provinceName.getFirst("province")));
            session.delete(province);
        } finally {
            session.getTransaction().commit();
        }
        return SUCCESS;
    }
    
    /**
     * Service get all infor province.
     *
     * @param provinceName: province name.
     * @return Json string success.
     */
    @RequestMapping(value= "provinces", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody String getProvince() {
        JSONArray arr_resultList = new JSONArray(); 
        session.beginTransaction();
        try {
            Criteria criteria = session.createCriteria(Provinces.class);
            Iterator<Provinces> provinces = criteria.list().iterator();
            while(provinces.hasNext()){
                JSONObject result = new JSONObject();
		Provinces province =  provinces.next();
                result.put("idProvince", province.getIdProvince());
                result.put("province", province.getProvince());
                arr_resultList.put(result);
            }
        } finally {
            session.getTransaction().commit();
        }
        return arr_resultList.toString();
    }
}
