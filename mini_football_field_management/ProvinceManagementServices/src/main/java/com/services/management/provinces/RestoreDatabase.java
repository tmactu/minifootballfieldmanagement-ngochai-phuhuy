/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.provinces;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hai
 */
@RestController
@RequestMapping("/restore")
public class RestoreDatabase {
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    String addProvince(HttpServletRequest request) {        
         String[] executeCmd = new String[]{"\"C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\mysql\"", "--user=" + "root", "--password=" + "hai123", "mini_football_field_management", "-e", "source " + "database.sql"};
 
        Process runtimeProcess;
        try {
            runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
 
            InputStream in = runtimeProcess.getErrorStream();
            int c;
            while ((c = in.read()) != -1)
            {

                System.out.print((char)c);
            }
            in.close();
        } catch (Exception ex) {
           
        }

        return "[{result : success}]";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String downoadFile(@RequestParam("file") MultipartFile file) throws FileNotFoundException, IOException {
        String name = "test11";
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = 
                        new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
                stream.write(bytes);
                stream.close();
                return "You successfully uploaded " + name + " into " + name + "-uploaded !";
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }
    
}
