/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model.hibernate.connect;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.exception.JWTExpiration;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Tran Ngoc Hai
 * @version 1.0
 * @since 2017-01-20
 */
public class HibernateUtil {  

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {

            Properties dbConnectionProperties = new Properties();
            try {
                dbConnectionProperties.load(new FileInputStream("../hibernate.properties"));
            } catch (Exception e) {
                e.printStackTrace();
                
                // Log
            }

            return new AnnotationConfiguration().mergeProperties(dbConnectionProperties).configure("hibernate.cfg.xml").buildSessionFactory();

        } catch (Throwable ex) {
            ex.printStackTrace();
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);

//            throw new ExceptionInInitializerError(ex);
        }
        return null;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    /**
     * Function create max id of table database
     *
     * @param className: class name (table name).
     * @param id:  attribute name (column name) need create.
     * @return an instance of java.lang.Integer is max + 1 id of column.
     */
    public static Integer createID(Object className, String id) throws HibernateException{

        //Get sessionFactory from Hibernate
        SessionFactory factory = HibernateUtil.getSessionFactory();

        Session session = factory.openSession();
            //begin transaction
            session.getTransaction().begin();
            //select max
            int result = (int) session.createCriteria(className.getClass())
                    .setProjection(Projections.max(id))
                    .uniqueResult();
            //delete buffer
            session.flush();
            //close transaction
            session.getTransaction().commit();
            return result + 1;
    }
    
    /**
     * Service check for login
     *
     * @param typeOfUser: type of user.
     * @param encode: Key string used for encryption.
     * @return an instance of java.lang.String describe JSON Web Token (JWT).
     */
    public static String createJWT(User user, String encode) {

        //Get the current date and time, increase by 30 minute
        Date date = new Date();
        date.setHours(date.getHours() + 1);
        //Create Jason Web Token
        try {
            String token = JWT.create()
                    .withExpiresAt(date)
                    .withIssuedAt(new Date())
                    .withIssuer(user.getUsername())
                    .withClaim("clubs", user.getClubs().getIdClub())
                    .withClaim("name", user.getName())
                    .withClaim("typeOfUser", user.getTypeOfUser())
                    .withClaim("birthdate", user.getBirthdate())
                    .withClaim("id", user.getId())
                    .withClaim("gender", user.getGender())
                    .withClaim("address", user.getAddress())
                    .withClaim("numberPhone", user.getNumberPhone())
                    .withClaim("email", user.getEmail())
                    .sign(Algorithm.HMAC256(encode));
            return token;

        } catch (IllegalArgumentException ex) {
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);
            return "error Illegal Argument";

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);
            return "error Encoding";
        }
    }
    
    /**
     * Service check the validity Json web token.
     * @param str_jwt: JWT need check.
     * @param str_checkKey: string use decode.
     * @param username: username of user.
     * @param typeOfUser: users need to check.
     * @return an instance of boolean describe success or fail of check
     */
    public static boolean checkJWT(String str_jwt, String str_checkKey,
            String username, String typeOfUser) throws JWTExpiration{
        try {
            //Reusable verifier instance
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(str_checkKey))
                    .withClaim("typeOfUser", typeOfUser)
                    .withIssuer(username)
                    .build();
            JWT jwt = (JWT) verifier.verify(str_jwt);
            
            //check expiration date (space 30 minute), innitiated date, user.           
            if(jwt.getExpiresAt().getTime() - new Date().getTime() < 600000){
                throw new JWTExpiration();
            }
        } catch (IllegalArgumentException | UnsupportedEncodingException ex) {
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (JWTVerificationException ex){
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
