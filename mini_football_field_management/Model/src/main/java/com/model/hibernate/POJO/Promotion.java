package com.model.hibernate.POJO;
// Generated Feb 24, 2017 9:37:13 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Promotion generated by hbm2java
 */
public class Promotion  implements java.io.Serializable {


     private int idPromotion;
     private Clubs clubs;
     private String information;
     private float percent;
     private Date startDate;
     private Date endDate;

    public Promotion() {
    }

    public Promotion(int idPromotion) {
       this.idPromotion = idPromotion;
    }
    
    public Promotion(int idPromotion, Clubs clubs, float percent, Date startDate, Date endDate) {
       this.idPromotion = idPromotion;
       this.clubs = clubs;
       this.percent = percent;
       this.startDate = startDate;
       this.endDate = endDate;
    }
   
    public int getIdPromotion() {
        return this.idPromotion;
    }
    
    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }
    public Clubs getClubs() {
        return this.clubs;
    }
    
    public void setClubs(Clubs clubs) {
        this.clubs = clubs;
    }
    public float getPercent() {
        return this.percent;
    }
    
    public void setPercent(float percent) {
        this.percent = percent;
    }
    public Date getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getInformation() {
        return information;
    }




}


