package com.model.hibernate.POJO;
// Generated Feb 24, 2017 9:37:13 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Employee generated by hbm2java
 */
public class Employee  implements java.io.Serializable {


     private String username;
     private int salary;
     private User user;
     private Date startingDate;
     private String work;

    public Employee() {
    }

    public Employee(String username) {
       this.username = username;
    }

    public Employee(int salary, User user, Date startingDate, String work) {
       this.salary = salary;
       this.user = user;
       this.startingDate = startingDate;
       this.work = work;
    }
   
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    public int getSalary() {
        return this.salary;
    }
    
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    public Date getStartingDate() {
        return this.startingDate;
    }
    
    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getWork() {
        return work;
    }
    
}


