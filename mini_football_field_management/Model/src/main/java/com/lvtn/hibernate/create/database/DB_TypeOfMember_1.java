///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.lvtn.hibernate.create.database;
//
//import static com.lvtn.hibernate.create.database.random.getRandomNumber;
//import com.model.hibernate.POJO.Clubs;
//import com.model.hibernate.POJO.TypeOfMember;
//import com.model.hibernate.connect.HibernateUtil;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//
///**
// *
// * @author Phu Huy
// */
//public class DB_TypeOfMember_1 {
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        SessionFactory factory = HibernateUtil.getSessionFactory();
//        Session session = factory.openSession();
//        
//        char[] types = {'n', 'b', 's', 'g', 'p', 'd'};
//        String[] content = {"Normal", "Bronze", "Silver", "Gold", "Platium", "Diamond"};
//        short[] percent = {0, 5, 8, 10, 15, 20};
//        int[] hour = {0, 30, 32, 36, 34, 60, 62, 63, 64, 90, 92, 91, 93, 120, 121, 122, 123, 150, 154, 153, 151};
//        int[] payment = {0, 3000, 3200, 3400, 3300, 6000, 6200, 6300, 6400, 9000, 9100, 9200, 9300, 12100, 12400, 12500, 12700,
//                                  15000, 15500, 16000, 15800};
//        String typeContent = "";
//        short percentDiscount;
//        int hourCondition, paymentCondition;
//        
//        int count = 1;
//        
//        
//        for(int i = 1; i < 301; i++){
//            for(char type : types){
//                session.beginTransaction();
//                TypeOfMember typeOfMember = new TypeOfMember();
//                switch(type){
//                    case 'n' :  
//                                typeContent = content[0];
//                                percentDiscount = percent[0];
//                                hourCondition = hour[0];
//                                paymentCondition = payment[0];
//                                
//                                typeOfMember.setTypeOfMember(typeContent + "_" + i); 
//                                typeOfMember.setPercentDiscount(percentDiscount);
//                                typeOfMember.setClubs(new Clubs(i));
//                                typeOfMember.setHourCondition(hourCondition); 
//                                typeOfMember.setPaymentCondition(paymentCondition);
//                                session.save(typeOfMember);
//                                break;
//                    case 'b' :  
//                                typeContent = content[1];
//                                percentDiscount = percent[1];
//                                hourCondition = hour[getRandomNumber(1, 4)];
//                                paymentCondition = payment[getRandomNumber(1, 4)];
//                                
//                                typeOfMember.setTypeOfMember(typeContent + "_" + i); 
//                                typeOfMember.setPercentDiscount(percentDiscount);
//                                typeOfMember.setClubs(new Clubs(i));
//                                typeOfMember.setHourCondition(hourCondition); 
//                                typeOfMember.setPaymentCondition(paymentCondition);
//                                session.save(typeOfMember);
//                                break;
//                    case 's' : 
//                                typeContent = content[2];
//                                percentDiscount = percent[2];
//                                hourCondition = hour[getRandomNumber(5, 8)];
//                                paymentCondition = payment[getRandomNumber(5, 8)];
//                                
//                                typeOfMember.setTypeOfMember(typeContent + "_" + i); 
//                                typeOfMember.setPercentDiscount(percentDiscount);
//                                typeOfMember.setClubs(new Clubs(i));
//                                typeOfMember.setHourCondition(hourCondition); 
//                                typeOfMember.setPaymentCondition(paymentCondition);
//                                session.save(typeOfMember);
//                                break;
//                    case 'g' :  
//                                typeContent = content[3];
//                                percentDiscount = percent[3];
//                                hourCondition = hour[getRandomNumber(9, 12)];
//                                paymentCondition = payment[getRandomNumber(9, 12)];
//                                
//                                typeOfMember.setTypeOfMember(typeContent + "_" + i); 
//                                typeOfMember.setPercentDiscount(percentDiscount);
//                                typeOfMember.setClubs(new Clubs(i));
//                                typeOfMember.setHourCondition(hourCondition); 
//                                typeOfMember.setPaymentCondition(paymentCondition);
//                                session.save(typeOfMember);
//                                break;
//                    case 'p' :  
//                                typeContent = content[4];
//                                percentDiscount = percent[4];
//                                hourCondition = hour[getRandomNumber(13, 16)];
//                                paymentCondition = payment[getRandomNumber(9, 12)];
//                                
//                                typeOfMember.setTypeOfMember(typeContent + "_" + i); 
//                                typeOfMember.setPercentDiscount(percentDiscount);
//                                typeOfMember.setClubs(new Clubs(i));
//                                typeOfMember.setHourCondition(hourCondition); 
//                                typeOfMember.setPaymentCondition(paymentCondition);
//                                session.save(typeOfMember);                                
//                                break;
//                    case 'd' :  
//                                typeContent = content[5];
//                                percentDiscount = percent[5];
//                                hourCondition = hour[getRandomNumber(17, 20)];
//                                paymentCondition = payment[getRandomNumber(17, 20)];
//                                
//                                typeOfMember.setTypeOfMember(typeContent + "_" + i); 
//                                typeOfMember.setPercentDiscount(percentDiscount);
//                                typeOfMember.setClubs(new Clubs(i));
//                                typeOfMember.setHourCondition(hourCondition); 
//                                typeOfMember.setPaymentCondition(paymentCondition);
//                                session.save(typeOfMember);
//                                break;
//                    default:
//                                break;     
//                }
//                session.getTransaction().commit();
//            }  
//        }
//        
//        System.exit(0); 
//    }
//    
//}
