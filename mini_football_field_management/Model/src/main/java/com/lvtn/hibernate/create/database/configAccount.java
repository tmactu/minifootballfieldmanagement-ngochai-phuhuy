/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lvtn.hibernate.create.database;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Hai
 */
public class configAccount {
        /**
     * Encode password use MD5 and secret key
     *
     * @param password : password need to encode
     * @param secretKey: secret key of password, use to encode it
     * @return Encrypted passwords
     */
    public static String encodePasswordToMD5(String password, String secretKey) {
        String result = null;
        byte[] salt = secretKey.getBytes();
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes 
            byte[] bytes = md.digest(password.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            result = sb.toString();
            return result;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

}
