/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lvtn.hibernate.create.database;

import static com.lvtn.hibernate.create.database.random.getRandomNumber;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Employee;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.TypeOfField;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.HibernateUtil;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Phu Huy
 */
public class test {
//    public static String getRandomUsername(int first, int middle, int last) {
//        String[] firstName = {"nguyen", "tran", "ly", "phu", "le", "truong", "la", "ngo", "van", "bui", "vo", "pham", "huynh", "dang", "phan"};
//        String[] middleName = {"ngoc", "kim", "ly", "gia", "khanh", "thuy", "ngoc", "thi", "bao", "minh", "thao", "hoa", "tan", "hoai", "hong", ""};
//        String[] lastName = {"quyen", "tran", "phuong", "thu", "binh", "dung", "anh", "vinh", "bich",
//                             "trang", "hien", "chau", "ngan", "hoang", "kien", "khanh", "thuy", "thanh",
//                             "tien", "truc", "thu", "quynh", "khai", "trung", "vi", "khanh", "hung",
//                             "huy", "dat", "hai", "cuong", "hoang", "an", "giao", "hau", "do", "duy", 
//                             "nghia", "tai", "dua", "huy", "nhan", "thanh", "nhi", "minh"};
//         
//        return firstName[first] + middleName[middle] + lastName[last];
//    }
//    
//    public static String getRandomName(int first, int middle, int last){
//        String[] firstName = {"Nguyễn", "Trần", "Lý", "Phú", "Lê", "Trương", "La", "Ngô", "Văn", "Bùi", "Võ", "Phạm", "Huỳnh", "Đặng", "Phan"};
//        String[] middleName = {" Ngọc", " Kim", " Lý", " Gia", " Khánh", " Thùy", " Ngọc", " Thị" , " Bảo", " Minh", " Thảo", " Hòa", " Tấn", " Hoài", " Hồng", ""};
//        String[] lastName = {" Quyên", " Trân", " Phương", " Thư", " Bính", " Dung", " Anh", " Vinh", " Bích", 
//                             " Trang", " Hiền", " Châu", " Ngân", " Hoàng", " Kiên", " Khanh", " Thủy", " Thanh", 
//                             " Tiên", " Trúc", " Thu", " Quỳnh", " Khải", " Trung", " Vi", " Khanh", " Hùng", 
//                             " Huy", " Đạt", " Hải", " Cường", " Hoàng", " An", " Giao", " Hậu", "Đô", " Duy", 
//                             " Nghĩa", " Tài", " Đức", " Hữu", "Nhân", "Thành", " Nhi", " Minh"};
//                
//        return firstName[first] + middleName[middle] + lastName[last];
//    }
//    public static String getRandomAddress(){
//        String[] streets = {"Trần Văn Hoài", "30/4", "3/2", "Võ Văn Kiệt", " Mậu Thân", "Trần Hoàng Na", 
//                           "Trần Ngọc Quế", "Nam Kỳ Khởi Nghĩa", "Xô Viết Nghệ Tĩnh", "Nguyễn Trãi", 
//                           "Nguyễn Thị Minh Khai", "Trần Văn Khéo", "Lý Tự Trọng", "Trần Hưng Đạo", 
//                           "Võ Văn Tần", "Cao Bá Quát", "Phạm Ngũ Lão", "Đồng Khởi", "Ngô Hữu Hạnh", 
//                           "Đề Thám", "Trương Định", "Phan Đình Phùng"};
//        String[] districts = {"Ninh Kiều", "Bình Thủy", "Phú Nhuận", "Bình Chánh", "Bình Tân", "Cái Răng", "Gò Vấp", "Tân Phú"};
//        String[] cities = {"Cần Thơ", "Bạc Liêu", "Hồ Chí Minh", "Vĩnh Long", "Đà Nẵng", "Vũng Tàu"};
//
//        return "Số " + getRandomNumber(1, 300) + ", Đường " + streets[getRandomNumber(0, 21)] + ", Quận " + districts[getRandomNumber(0, 7)] + ", Thành Phố " + cities[getRandomNumber(0, 5)];
//    }
//    
//    public static String getRandomBirthdate(){
//        return getRandomNumber(0, 2) + "" + getRandomNumber(0, 7) + "/0" + getRandomNumber(1, 9) + "/" + getRandomNumber(1950, 2015);
//    }
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) throws ParseException {
//        SessionFactory factory = HibernateUtil.getSessionFactory();
//        Session session = factory.openSession();
//        
//        int first;
//        int middle;
//        int last;
//        String[] work = {"Bảo vệ", "Dọn dẹp sân", "Bảo trì dụng cụ", "Kế toán", "Trọng tài", "Thủ quỹ"};
//        String[] salary = {"4800", "3500", "3700", "5000", "4000", "4200", "4500", "4700", "4900"};
//        
//        for(int i = 0; i <= 12; i++){
//            first = getRandomNumber(0, 14);
//            middle = getRandomNumber(0, 15);
//            last = getRandomNumber(0, 44);
//            
//            System.out.println("\n-------\n");
//            System.out.println(i);
//            String sql = "SELECT * FROM employee WHERE `USERNAME` IN ( SELECT DISTINCT e.`USERNAME` FROM employee e, `user` u WHERE (e.`USERNAME` = u.`USERNAME`) AND ( LENGTH(e.`USERNAME`) >= ( SELECT DISTINCT MAX(LENGTH(`USERNAME`)) FROM employee WHERE `USERNAME` IN ( SELECT `USERNAME` FROM `user` WHERE (`ID_CLUB` = :idClub) AND (`TYPE_OF_USER` = 'employee') ) ) ) AND (u.`ID_CLUB` = :idClub) ) ORDER BY `USERNAME` DESC";
//            SQLQuery query = session.createSQLQuery(sql);
//                query.addEntity(Employee.class);
//                query.setParameter("idClub", 1);
//            List list = query.list();
//            Iterator<Employee> iterator = list.iterator();
//            Employee emp = iterator.next();
//            String oldUsername = emp.getUsername();
//        
//            //Cut string username
//            int index = oldUsername.indexOf(".");        
//            oldUsername = oldUsername.substring(index + 1);
//
//            String usernameEmployee = "NV_" + 1 + "." + (Integer.parseInt(oldUsername) + 1); 
//            String email = getRandomUsername(first, middle, last) + getRandomNumber(1, 99) + "@gmail.com";
//            
//            //Begin transaction
//            session.beginTransaction();
//            
//            User user = new User();
//                user.setUsername(usernameEmployee); 
//                user.setPassword("none");
//                user.setEncodeKey("none");
//                user.setName(getRandomName(first, middle, last));
//                user.setBirthdate(new SimpleDateFormat("dd/MM/yyyy").parse(getRandomBirthdate()));
//                user.setId(""+getRandomNumber(385000000,385999999));
//                user.setGender(Boolean.FALSE);
//                user.setAddress(getRandomAddress());
//                user.setNumberPhone("0" + getRandomNumber(123000000,999999999));
//                user.setEmail(email);
//                user.setTypeOfUser("employee");
//                user.setClubs(new Clubs(1));
//            
//            //Save object
//            session.save(user);
//            //Close transaction
//            session.getTransaction().commit();
//            
//            //Begin transaction again 
//            session.beginTransaction();
//            //Refesh table User
//            session.refresh(user); 
//            
//            Employee employee = new Employee();
//                employee.setUsername(usernameEmployee);
//                employee.setWork(work[getRandomNumber(0, 5)]);
//                employee.setStartingDate(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900"));
//                employee.setSalary(Integer.parseInt(salary[getRandomNumber(0, 8)]));
//                employee.setUser(user); 
//           
//           //Save object
//           session.save(employee);
//           //Close transaction
//           session.getTransaction().commit();
//        }
//        
//        System.exit(0);  
//    }
    
    //Begin tracsaction
    public static void main(String[] args) throws ParseException {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();
        
        try{
             Date startTime = new SimpleDateFormat("HH:mm:ss").parse("10:00:00");
             Date endTime = new SimpleDateFormat("HH:mm:ss").parse("16:00:00");

            //Create criteria
            DetachedCriteria subquery = DetachedCriteria.forClass(Reservation.class, "r");            
            Criterion cr_startTime = Restrictions.between("r.startTime", startTime, endTime);
            Criterion cr_endTime = Restrictions.between("r.endTime", startTime, endTime);             
            LogicalExpression orExp = Restrictions.or(cr_startTime, cr_endTime);               
            subquery.add(orExp)
                    .setProjection(Projections.property("r.fields.idField"));
            
            Criteria criteria = session.createCriteria(Fields.class, "f")
                            .add(Restrictions.eq("f.clubs.idClub", 1))
                            .add(Restrictions.eq("f.typeOfField.typeOfField", "5x5"))
                            .add(Subqueries.propertyNotIn("f.idField", subquery));           
            
            
            Iterator<Fields> reservations = criteria.list().iterator();
            while (reservations.hasNext()) {
                Fields field = reservations.next();
                //set result into json
                System.out.println(field.getIdField());
                System.out.println(field.getTypeOfField().getTypeOfField());
            }
            
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
    }
        
}
