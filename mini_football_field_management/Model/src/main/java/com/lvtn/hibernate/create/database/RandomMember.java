///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.lvtn.hibernate.create.database;
//
//import static com.lvtn.hibernate.create.database.random.getRandomNumber;
//import com.model.hibernate.POJO.Clubs;
//import com.model.hibernate.POJO.Member;
//import com.model.hibernate.POJO.TypeOfMember;
//import com.model.hibernate.POJO.User;
//import com.model.hibernate.connect.HibernateUtil;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.UUID;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//
///**
// *
// * @author Phu Huy
// */
//public class RandomMember {
//    public static String getRandomUsername(int first, int middle, int last) {
//        String[] firstName = {"nguyen", "tran", "ly", "phu", "le", "truong", "la", "ngo", "van", "bui", "vo", "pham", "huynh", "dang", "phan"};
//        String[] middleName = {"ngoc", "kim", "ly", "gia", "khanh", "thuy", "ngoc", "thi", "bao", "minh", "thao", "hoa", "tan", "hoai", "hong", ""};
//        String[] lastName = {"quyen", "tran", "phuong", "thu", "binh", "dung", "anh", "vinh", "bich",
//                             "trang", "hien", "chau", "ngan", "hoang", "kien", "khanh", "thuy", "thanh",
//                             "tien", "truc", "thu", "quynh", "khai", "trung", "vi", "khanh", "hung",
//                             "huy", "dat", "hai", "cuong", "hoang", "an", "giao", "hau", "do", "duy", 
//                             "nghia", "tai", "dua", "huy", "nhan", "thanh", "nhi", "minh"};
//         
//        return firstName[first] + middleName[middle] + lastName[last];
//    }
//    
//    public static String getRandomName(int first, int middle, int last){
//        String[] firstName = {"Nguyễn", "Trần", "Lý", "Phú", "Lê", "Trương", "La", "Ngô", "Văn", "Bùi", "Võ", "Phạm", "Huỳnh", "Đặng", "Phan"};
//        String[] middleName = {" Ngọc", " Kim", " Lý", " Gia", " Khánh", " Thùy", " Ngọc", " Thị" , " Bảo", " Minh", " Thảo", " Hòa", " Tấn", " Hoài", " Hồng", ""};
//        String[] lastName = {" Quyên", " Trân", " Phương", " Thư", " Bính", " Dung", " Anh", " Vinh", " Bích", 
//                             " Trang", " Hiền", " Châu", " Ngân", " Hoàng", " Kiên", " Khanh", " Thủy", " Thanh", 
//                             " Tiên", " Trúc", " Thu", " Quỳnh", " Khải", " Trung", " Vi", " Khanh", " Hùng", 
//                             " Huy", " Đạt", " Hải", " Cường", " Hoàng", " An", " Giao", " Hậu", "Đô", " Duy", 
//                             " Nghĩa", " Tài", " Đức", " Hữu", "Nhân", "Thành", " Nhi", " Minh"};
//                
//        return firstName[first] + middleName[middle] + lastName[last];
//    }
//    public static String getRandomAddress(){
//        String[] streets = {"Trần Văn Hoài", "30/4", "3/2", "Võ Văn Kiệt", " Mậu Thân", "Trần Hoàng Na", 
//                           "Trần Ngọc Quế", "Nam Kỳ Khởi Nghĩa", "Xô Viết Nghệ Tĩnh", "Nguyễn Trãi", 
//                           "Nguyễn Thị Minh Khai", "Trần Văn Khéo", "Lý Tự Trọng", "Trần Hưng Đạo", 
//                           "Võ Văn Tần", "Cao Bá Quát", "Phạm Ngũ Lão", "Đồng Khởi", "Ngô Hữu Hạnh", 
//                           "Đề Thám", "Trương Định", "Phan Đình Phùng"};
//        String[] districts = {"Ninh Kiều", "Bình Thủy", "Phú Nhuận", "Bình Chánh", "Bình Tân", "Cái Răng", "Gò Vấp", "Tân Phú"};
//        String[] cities = {"Cần Thơ", "Bạc Liêu", "Hồ Chí Minh", "Vĩnh Long", "Đà Nẵng", "Vũng Tàu"};
//
//        return "Số " + getRandomNumber(1, 300) + ", Đường " + streets[getRandomNumber(0, 21)] + ", Quận " + districts[getRandomNumber(0, 7)] + ", Thành Phố " + cities[getRandomNumber(0, 5)];
//    }
//    
//    public static String getRandomBirthdate(){
//        return getRandomNumber(0, 2) + "" + getRandomNumber(0, 7) + "/0" + getRandomNumber(1, 9) + "/" + getRandomNumber(1950, 2015);
//    }
//    /**
//     * @param args the command line arguments
//     * @throws java.text.ParseException
//     */
//    public static void main(String[] args) throws ParseException, StringIndexOutOfBoundsException{
//        
//        SessionFactory factory = HibernateUtil.getSessionFactory();
//        Session session = factory.openSession();
//        
//        String password = "123456";
//        //random password encryption key
//        String encode = UUID.randomUUID().toString();
//        //encode MD5 for password
//        password = configAccount.encodePasswordToMD5(password, encode);
//        
//        int first;
//        int middle;
//        int last;
//        String[] type = {"Normal", "Gold", "Diamond", "Bronze", "Platium", "Silver"};
//        
//        for(int i = 0; i < 1000; i++){
//            session.beginTransaction();
//            first = getRandomNumber(0, 14);
//            middle = getRandomNumber(0, 15);
//            last = getRandomNumber(0, 44);
//            
//            System.out.println("\n-------\n");
//            System.out.println(i);
//            String username = getRandomUsername(first, middle, last);
//            int idClub = getRandomNumber(2, 300);
//                        
//            User user = new User();
//                user.setUsername(username + getRandomNumber(1000, 9999)); 
//                user.setPassword(password);
//                user.setEncodeKey(encode);
//                user.setName(getRandomName(first, middle, last));
//                user.setBirthdate(new SimpleDateFormat("dd/MM/yyyy").parse(getRandomBirthdate()));
//                user.setId(""+getRandomNumber(385000000,385999999));
//                user.setGender(Boolean.FALSE);
//                user.setAddress(getRandomAddress());
//                user.setNumberPhone("0" + getRandomNumber(123000000,999999999));
//                user.setEmail(username + getRandomNumber(1, 99) + "@gmail.com");
//                user.setTypeOfUser("member");
//                user.setClubs(new Clubs(idClub));
//            
//            // Save object
//            session.save(user);
//            //close transaction
//            session.getTransaction().commit();
//            
//            //Begin transaction again 
//            session.beginTransaction();
//            //Refesh table User
//            session.refresh(user); 
//            
//           //create object Member to set information data for Member
//            Member member = new Member();
//                member.setUser(user);
//                member.setUsername(username);
//                member.setTypeOfMember(new TypeOfMember(type[getRandomNumber(0, 5)] + "_" + idClub));
//                member.setTotalHours(0); 
//                member.setTotalPayments(0);
//                member.setLastOrderDate(new SimpleDateFormat("dd/MM/yyyy").parse(getRandomBirthdate()));
//                
//            //Save object
//            session.save(member);
//            session.getTransaction().commit();
//        }
//        
//        System.exit(0); 
//              
//    }
//}
