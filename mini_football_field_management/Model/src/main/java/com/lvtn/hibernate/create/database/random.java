/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lvtn.hibernate.create.database;

import java.util.Random;

/**
 *
 * @author Phu Huy
 */
public class random{
    public static int getRandomNumber(int min, int max){
        try {
            Random rn = new Random();
            int range = max - min + 1;
            int randomNum = min + rn.nextInt(range);
            return randomNum;
        } catch (Exception e) {
            return -1;
        }
    }
}
