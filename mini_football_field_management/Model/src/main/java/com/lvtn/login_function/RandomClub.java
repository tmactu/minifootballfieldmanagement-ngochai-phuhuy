/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lvtn.login_function;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Provinces;
import com.model.hibernate.connect.HibernateUtil;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.hibernate.Criteria;
import org.hibernate.Session;

/**
 *
 * @author Hai
 */
public class RandomClub {
     public static int rand(int min, int max) {
        try {
            Random rn = new Random();
            int range = max - min + 1;
            int randomNum = min + rn.nextInt(range);
            return randomNum;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    public static void main(String[] args) {
       //Session from Hibernate
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        Criteria creat = session.createCriteria(Provinces.class);
        List<Provinces> result = creat.list();
        String[] name = {"Thống Nhất", "Tiểu Ngư", "Kỳ Hòa", "Cty TNHH XDTM-DV OAD",
            "Phú Thọ", "Thành Phát", "Cây Sộp", "Mai Vàng", "Thanh Phát", "Thùy Linh"
        , "Quang Trung", "Lan Anh", "Hoàng Đạt", "Đại Nguyên", "Trung Mỹ Tây", "Minh Trí"
                , "Ngôi Sao", "Nhà Thiếu Nhi Q12", "Khiết Tâm", "Linh Đông", "Quả Bóng Vàng",
                "Trường CĐ Công Nghệ Thủ Đức", "Nhà Thiếu Nhi Quận Thủ Đức", "Thuận Phát", "ViTaWa 1",
                "Phi Long", "Bình Nguyên", "Đông Nhựt", "Bình Chiểu", "Những Người Bạn",
                "SEAGAME", "ZIDAN", "HoSaNa", "Hoàng Thịnh", "Trung tâm TDTT Quận 9", "Lâm Thịnh"
                , "Đại Châu", "Phù Đổng", "Lam Sơn", "Tiến Phát", "Hiệp Phú", "Phương Nam", "Chu Văn An"
                , "Thanh Đa", "Thành Thái", "Thiên Kiều", "Đào Duy Anh", "Nguyễn Oanh", "Thành Lâm", "Đại Nam", "Bình An"};
        for(int i = 1; i <= 300; i++){            
            Clubs club = new Clubs(i);
            club.setName(name[rand(1, 50)]);
            club.setProvinces(result.get(rand(0, 62)));
            club.setAddress("Số " + rand(1, 600) + ", Đường " + name[rand(1, 50)] + " Quận " + name[rand(1, 50)] + " Thành Phố " + name[rand(1, 50)]);
            session.save(club);
        }
        session.getTransaction().commit();
                
    }
    
}
