/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lvtn.login_function;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Notification;
import com.model.hibernate.connect.HibernateUtil;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Phu Huy
 */
public class RandomNotification {
    public static int rand(int min, int max) {
        try {
            Random rn = new Random();
            int range = max - min + 1;
            int randomNum = min + rn.nextInt(range);
            return randomNum;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    public static void main(String[] args) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        String title[] = {
            "Bão trì sân",
            "Tuyển nhân viên",
            "Tuyển nhân viên",
            "Liên hoan với các thành viên",
            "Tổ chức giải đấu",
            "Tổ chức giải đấu",
            "Tổ chức giải đấu",
            "Tổ chức giải đấu",
            "Mở thêm sân",
            "Bão trì sân",
            "Bão trì sân",
            "Kỉ niệm 5 năm hoạt động của câu lạc bộ",
            "Mở lớp dạy đá banh",
            "Tuyển nhân viên"
        };
        
        String content[] = {
            "Toàn bộ sân loại 22 người sẽ tạm ngưng hoạt động vào ngày 20/05/2017 để bảo dưỡng.",
            "Cần tuyển vị trí bảo vệ và lao công làm việc cho câu lạc bộ lương theo thỏa thuận.",
            "Cần tuyển vị trí kĩ thuật, bão trì sân cho câu lạc bộ lương theo thỏa thuận.",
            "Câu lạc bộ sẽ tổ chức 1 buổi liên hoan dành cho các thành viên loại Gold trở lên để cảm ơn vì đã ủng hộ câu lạc bộ.",
            "Tổ chức giải thi đấu bóng đá mini dành cho các thành viên ở câu lạc bộ, liên hệ với anh Minh qua số 0912938293.",
            "Tổ chức giải thi đấu bóng đá mini dành cho các thành viên ở câu lạc bộ, liên hệ với anh Minh qua số 0912938293.",
            "Tổ chức giải thi đấu bóng đá mini dành cho các thành viên ở câu lạc bộ, liên hệ với anh Minh qua số 0912938293.",
            "Tổ chức giải thi đấu bóng đá mini dành cho các thành viên ở câu lạc bộ, liên hệ với anh Minh qua số 0912938293.",
            "Câu lạc bộ vừa mở thêm 4 sân loại 10 người. Các thành viên sẽ không phải lo lấng việc thiếu sân.",
            "Toàn bộ sân loại 14 người sẽ tạm ngưng hoạt động vào ngày 22/05/2017 để bảo dưỡng.",
            "Toàn bộ sân loại 10 người sẽ tạm ngưng hoạt động vào ngày 23/05/2017 để bảo dưỡng.",
            "Câu lạc bộ sẽ tổ chức buổi họp mặt chúc mừng 5 năm hoạt động câu lạc bộ. Cám ơn các thành viên đã ủng hộ nhiệt tình.",
//            "Nhân dịp lễ các thành viên khi đặt sân vào ngày 25/05/2017 đến ngày 26/07/2017 sẽ được khuyến mãi 2 thùng nước đá.",
            "Tổ chức khóa dạy đá banh dành cho các em thiếu nhi từ 9 đến 13 tuổi. Thời gian mở lớp vào ngày 24/05/2017.",
            "Tuyển vị trí kế toán cho câu lạc bộ lương theo thỏa thuận.",
        };
        
        session.beginTransaction();
        
//            for(int i = 1; i <=300; i++){
                for(int j = 1; j <= 14; j++){
                    int index = rand(0, 13);
                    int idNotification = j;
                    Date dateStart = new Date();            
                    Calendar calendar = Calendar.getInstance(); 
                        calendar.setTime(dateStart); 
                        calendar.add(Calendar.DATE, rand(-1, -10));
                    dateStart = calendar.getTime();
                    
                    Notification notification = new Notification();
                        notification.setClubs(new Clubs(1));
                        notification.setIdNotification(idNotification);
                        notification.setTitle(title[index]);
                        notification.setContent(content[index]);
                        notification.setStartDate(dateStart);
                        
                    session.save(notification);
                }
//            }
        
        session.getTransaction().commit();
    }
}
