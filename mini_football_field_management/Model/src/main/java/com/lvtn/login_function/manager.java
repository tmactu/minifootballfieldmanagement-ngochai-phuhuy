/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lvtn.login_function;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.model.hibernate.connect.HibernateUtil;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hai
 */
public class manager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String typeOfUser = "manager";
        String encode = "jf9fa3w84rujsdfbaslit74sighfw94";
        String username = "manager1";
        //Get the current date and time, increase by 30 minute
        Date date = new Date();
        date.setHours(date.getHours() + 1);
        //Create Jason Web Token
        try {
            String token = JWT.create()
                    .withExpiresAt(date)
                    .withIssuer(username)
                    .withIssuedAt(new Date())
                    .withClaim("typeOfUser", typeOfUser)
                    .sign(Algorithm.HMAC256(encode));
            System.out.println(token);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, ex);
 
        }
    }
    
}
