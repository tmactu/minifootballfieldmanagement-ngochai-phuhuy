/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lvtn.login_function;

import com.model.hibernate.POJO.Bill;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.connect.HibernateUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Hai
 */
public class RandomBill {

    public static int rand(int min, int max) {
	        try {
	            Random rn = new Random();
	            int range = max - min + 1;
	            int randomNum = min + rn.nextInt(range);
	            return randomNum;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return -1;
	        }
	    }
	public static void main(String[] args) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
                session.clear();
		Criteria cri = session.createCriteria(Fields.class)
				.add(Restrictions.eq("clubs", new Clubs(1)));
		List<Fields> list = cri.list();
		for(int i = 0; i < 100; i++){
                    Date date = new Date();
                    date.setDate(rand(1,30));
                    date.setMonth(3);
                    date.setYear(2017);
                    System.out.println(date.getYear());
			Date start = date;
			start.setHours(rand(start.getHours(), 21));
                        start.setMinutes(rand(start.getMinutes(), 59));
                        start.setSeconds(0);
			Date end = date;
			end.setHours(rand(start.getHours() + 1, 22));
                        end.setMinutes(rand(start.getMinutes(), 59));
                        end.setSeconds(0);
			Fields field = list.get(rand(0, list.size()-1));			
			
			Bill reser = new Bill(i);
			reser.setDate(date);
			reser.setFields(field);
			reser.setMember(new Member("buigiachau1503"));
			reser.setStartTime(start);
			reser.setEndTime(end);
			reser.setTotalPay(rand(100, 500));
                        
			session.save(reser);
                        session.flush();
		}
		session.getTransaction().commit();
                
	}
    
}
