package com.lvtn.login_function;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Member;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.connect.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RandomReservation {
	 public static int rand(int min, int max) {
	        try {
	            Random rn = new Random();
	            int range = max - min + 1;
	            int randomNum = min + rn.nextInt(range);
	            return randomNum;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return -1;
	        }
	    }
	public static void main(String[] args) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria cri = session.createCriteria(Fields.class)
				.add(Restrictions.eq("clubs", new Clubs(1)));
		List<Fields> list = cri.list();
		for(int i = 0; i < 50; i++){
			Date start = new Date();
			start.setHours(rand(start.getHours(), 21));
                        start.setMinutes(rand(start.getMinutes() + 10, 59));
                        start.setSeconds(0);
			Date end = new Date();
			end.setHours(rand(start.getHours() + 1, 22));
                        end.setMinutes(rand(start.getMinutes(), 59));
                        end.setSeconds(0);
			Fields field = list.get(rand(0, list.size()-1));
			int rateByHour = (end.getHours() - start.getHours()) * field.getPrice();
	        float rateByMinute = (end.getMinutes() - start.getMinutes()) * field.getPrice() / 60;
	        int rates = rateByHour + (int) rateByMinute;
			
			Reservation reser = new Reservation(i);
			reser.setDate(new Date());
			reser.setFields(field);
			reser.setMember(new Member("buigiachau1503"));
			reser.setStartTime(start);
			reser.setEndTime(end);
			reser.setRates(rates);
			session.save(reser);
		}
		session.getTransaction().commit();

	}

}
