package com.lvtn.login_function;

import java.util.List;
import java.util.Random;

import org.hibernate.Criteria;
import org.hibernate.Session;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Provinces;
import com.model.hibernate.POJO.TypeOfField;
import com.model.hibernate.connect.HibernateUtil;

public class RandomField {
	public static int rand(int min, int max) {
        try {
            Random rn = new Random();
            int range = max - min + 1;
            int randomNum = min + rn.nextInt(range);
            return randomNum;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
	
	 public static void main(String[] args) {
		 	String[] listField = {"5x5","7x7","11x11"};
	       //Session from Hibernate
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.getTransaction().begin();
	        Criteria creat = session.createCriteria(Clubs.class);
	        List<Clubs> result = creat.list();
	        int count = 0;
	        for(Clubs club : result){
                    int[] price = {rand(80,200),rand(80,200), rand(80,200)};
                    
	        	int n = rand(10,20);
	        	for(int i = 0; i < n; i++){
                            int index = rand(0,2);
                            TypeOfField type = new TypeOfField(listField[index]);
	        		Fields field = new Fields();
	        		field.setClubs(club);
	        		field.setIdField(count++);
	        		field.setPrice(price[index]);
	        		field.setTypeOfField(type);
	        		field.setUsed(false);
	        		session.save(field);
	        	}
	        }
	        session.getTransaction().commit();
	                
	    }
}
