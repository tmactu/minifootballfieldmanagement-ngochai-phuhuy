(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [
            // Core
            'app.core',
            // Apps
            'app.dashboards',
            'ngCookies'
        ]);
})();
