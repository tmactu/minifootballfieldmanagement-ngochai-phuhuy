(function ()
{
    'use strict';

    angular
        .module('app.dashboards.project')
        .controller('DashboardProjectController', DashboardProjectController);

    /** @ngInject */
    function DashboardProjectController($scope, $interval, $mdSidenav, $mdDialog, DashboardData)
    {
        var vm = this;

        // Data
        vm.dashboardData = DashboardData;
        vm.projects = vm.dashboardData.projects;


        //CONTACT
        vm.dataContact = vm.dashboardData.dataContact;

        //INTRODUCE
        vm.introduce = vm.dashboardData.introduce;

        //OVERVIEW

        // WIDGET 1 (Member)
        //Data
        vm.widget1 = vm.dashboardData.widget1;
        vm.dataMember = {
            count:''
        };
        //Method
        vm.countMember = countMember;

        function countMember(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8010/account/countMember",
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
                vm.dataMember = result;
            },
            error:function (error) {
              alert(error);
            }});
        };

        // WIDGET 2 (Club)
        //Data
        vm.widget2 = vm.dashboardData.widget2;
        vm.dataClub = {
          count:''
        };
        //Method
        vm.countClub = countClub;

        function countClub(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8015/club/countClub",
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.dataClub = result;
            },
            error:function (error) {
              alert(error);
            }});
        };

        // WIDGET 3 (Field)
        //Data
        vm.widget3 = vm.dashboardData.widget3;
        vm.dataField = {
          result: ''
        }
        //Method
        vm.countField = countField;

        function countField(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8011/total",
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.dataField = result;
            },
            error:function (error) {
              alert(error);
            }});
        }

        // WIDGET 4
        // Data
        vm.provinces = {};
        vm.dataProvince = '1';
        vm.clubs = {};
        vm.infomationClub = {};
        // Method
        vm.getProvince = getProvince;
        vm.getClubForId = getClubForId;

        //Use get all province in database
        function getProvince(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8013/rest/provinces",
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.provinces = result;
            },
            error:function (error) {
              alert(error);
            }});
        }
        //Watch what province selected and show clubs belong to this province
         $scope.$watch("vm.dataProvince", function(Value){
            $.ajax({
              type : 'GET',
              url: "http://localhost:8015/club/getAllClubForProvince?province=" + Value,
              contentType : 'application/x-www-form-urlencoded',
              success: function(result){
                vm.clubs = result;
              },
              error:function (error) {
                alert(error);
              }});
        })

        //Watch what club selected and show clubs belong to this province
        function getClubForId(){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8015/club/getClubForId?club=" + vm.dataIdClub,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.infomationClub = result;
              vm.widget4.mainChart.data = vm.infomationClub.dataField;
              vm.widget4.src = 'app/main/apps/dashboards/project/overview/searchClub.html';
            },
            error:function (error) {
              alert(error);
            }});
        };


        vm.widget4 = {
            title             : vm.dashboardData.widget4.title,
            subtitle          : vm.dashboardData.widget4.subtitle,
            content           : vm.dashboardData.widget4.content,
            foot              : vm.dashboardData.widget4.foot,
            chooseProvince    : vm.dashboardData.widget4.chooseProvince,
            chooseClub        : vm.dashboardData.widget4.chooseClub,
            buttonClick       : vm.dashboardData.widget4.buttonClick,
            nameClub          : vm.dashboardData.widget4.nameClub,
            addressClub       : vm.dashboardData.widget4.addressClub,
            hour              : vm.dashboardData.widget4.hour,
            price5x5          : vm.dashboardData.widget4.price5x5,
            price7x7          : vm.dashboardData.widget4.price7x7,
            price11x11        : vm.dashboardData.widget4.price11x11,
            note              : vm.dashboardData.widget4.note,
            noteContent       : vm.dashboardData.widget4.noteContent,
            noteContent5x5    : vm.dashboardData.widget4.noteContent5x5,
            noteContent7x7    : vm.dashboardData.widget4.noteContent7x7,
            noteContent11x11  : vm.dashboardData.widget4.noteContent11x11,
            src               : '',
            mainChart   : {
              config : {
                refreshDataOnly: true,
                deepWatchData  : true
              },
              options: {
                chart: {
                  type        : 'pieChart',
                  color       : [ '#03a9f4', '#9c27b0', '#558B2F'],
                  height      : 400,
                  margin      : {
                    top   : 0,
                    right : 0,
                    bottom: 0,
                    left  : 0
                  },
                  title       : 'Sân',
                  donut       : true,
                  clipEdge    : true,
                  cornerRadius: 0,
                  labelType   : 'percent',
                  showLegend  : false,
                  padAngle    : 0.02,
                  x           : function (d)
                  {
                    return d.label;
                  },
                  y           : function (d)
                  {
                    return d.countField;
                  },
                  tooltip     : {
                    gravity: 's',
                    classes: 'gravity-s'
                  }
                }
              },
              data   : vm.infomationClub.dataField
            },
            init        : function ()
            {

            }
        };

        // REGISTER
        //Data
        vm.registerAccount = vm.dashboardData.registerAccount;
        vm.basicForm = {};
        vm.formWizard = {};
        vm.formWizard.username = '';
        // Methods
        vm.sendForm = sendForm;
        vm.registerAC = registerAC;


        //Watch what province selected and show clubs belong to this province
        $scope.$watch("vm.formWizard.username", function(Value){
          $.ajax({
            type : 'GET',
            url: "http://localhost:8010/account?username=" + Value,
            contentType : 'application/x-www-form-urlencoded',
            success: function(result){
              vm.resultCheck = result;
              vm.pattern = vm.resultCheck.pattern;
              vm.contentPattern = vm.resultCheck.contentPattern;
              vm.hide = vm.resultCheck.hide;
            },
            error:function (error) {
              alert(error);
            }});
        })

          /**
         * Register form
         */
        function registerAC()
        {
          var birthdate = new Date(vm.formWizard.birthdate);

          vm.formWizard.birthdate = birthdate.getFullYear() + '-' + birthdate.getMonth() + '-' + birthdate.getDate();
          vm.formWizard.typeOfMember = 'Normal_' + vm.formWizard.club;

          // You can do an API call here to send the form to your server
          $.ajax({
            type : 'POST',
            url: "http://localhost:8010/account/register",
            contentType : 'application/x-www-form-urlencoded',
            data: vm.formWizard,
            success: function(result){
            },
            error:function (error) {
              alert(error);
            }});
        }
        function sendForm(ev)
        {
          // You can do an API call here to send the form to your server
          // Show the sent data.. you can delete this safely.

          $mdDialog.show({
            controller         : function ($scope, $mdDialog)
            {
              $scope.dataDialog = {
                dialogContent: "Bạn đã đăng ký thành công!! Bạn vui lòng nhấp vào biểu tượng bên phải để có thể đăng nhập.",
                dialogClose: "ĐÓNG"
              };
              $scope.closeDialog = function ()
              {
                $mdDialog.hide();
              }
            },
            templateUrl        : 'app/main/apps/dashboards/project/register/dialog/dialogRegisterAccountSuccess.html',
            parent             : angular.element('body'),
            targetEvent        : ev,
            clickOutsideToClose: false
          })

          // Clear the form data
          vm.formWizard = {};
        }

        // Now widget
        vm.nowWidget = {
            now   : {
                second: '',
                minute: '',
                hour  : '',
                day   : '',
                month : '',
                year  : ''
            },
            ticker: function ()
            {
                var now = moment();
                vm.nowWidget.now = {
                    second : now.format('ss'),
                    minute : now.format('mm'),
                    hour   : now.format('HH'),
                    day    : now.format('D'),
                    weekDay: now.format('dddd'),
                    month  : now.format('MMMM'),
                    year   : now.format('YYYY')
                };
            }
        };

        // Weather widget
        vm.weatherWidget = vm.dashboardData.weatherWidget;

        // Methods
        vm.toggleSidenav = toggleSidenav;
        vm.selectProject = selectProject;

        //////////
        vm.selectedProject = vm.projects[0];


        // Initialize Widget 4
        vm.widget4.init();

        // Now widget ticker
        vm.nowWidget.ticker();

        var nowWidgetTicker = $interval(vm.nowWidget.ticker, 1000);

        $scope.$on('$destroy', function ()
        {
            $interval.cancel(nowWidgetTicker);
        });

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

        /**
         * Select project
         */
        function selectProject(project)
        {
            vm.selectedProject = project;
        }

    }
})();
