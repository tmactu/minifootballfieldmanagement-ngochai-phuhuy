(function ()
{
  'use strict';

  angular
    .module('app.dashboards.project')
    .config(config);

    function config($mdDateLocaleProvider){
      $mdDateLocaleProvider.formatDate = function(birthdate) {
        return birthdate ? moment(birthdate).format('DD-MM-YYYY') : '';
      };
    }

})();
