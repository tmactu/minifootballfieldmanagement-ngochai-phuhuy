(function () {
  'use strict';

  angular
    .module('app.core')
    .controller('MsLogin', MsLogin);

  function MsLogin($http, $mdDialog, $cookieStore){
    var vm = this;

    //Data
    vm.parameter = {
      username:'',
      password:''
    };
    vm.login = {
      login: "Đăng nhập",
      requiredUsername: "Vui lòng nhập tên người dùng",
      requiredPassword: "Vui lòng nhập mật khẩu",
      forget: "Quên mật khẩu?",
      buttonLogin: "ĐĂNG NHẬP",
      buttonCancel: "HỦY"

    };
    vm.contentErrorLogin = "Tên đăng nhập hoặc mật khẩu không đúng. Vui lòng nhập lại.";
    vm.flagLogin = false;

    //Method
    vm.loginAccount = loginAccount;
    vm.closeDialog = closeDialog;
    vm.dialogForgotPassword = dialogForgotPassword;

    function loginAccount() {

        // $http({
        //   method: 'POST',
        //   url: 'http://localhost:8010/account',
        //   headers:{
        //     'Content-Type': 'application/x-www-form-urlencoded'
        //   },
        //   data: vm.parameter
        // }).then(function successCallback(response) {
        //   alert(response.data);
        // }, function errorCallback(response) {
        //   alert(response.statusText);
        // });

      $.ajax({
        type : 'POST',
        url: "http://localhost:8010/account",
        contentType : 'application/x-www-form-urlencoded',
        data: vm.parameter,
        success: function(result){
            vm.dataLogin = result;

            $cookieStore.put('jwt',vm.dataLogin.jwt);
            $cookieStore.put('username',vm.dataLogin.username);
            $cookieStore.put('club',vm.dataLogin.club);

            if(vm.dataLogin.type == 'member'){
              window.location = "http://localhost:8001/member";
            }else if(vm.dataLogin.type == 'manager'){
              window.location = "http://localhost:8002";
            }else if(vm.dataLogin.type == 'admin'){
              window.location = "http://localhost:8003";
            }
      },
      error:function (error) {
        vm.flagLogin = true;
      }});
    }

    function closeDialog() {
      $mdDialog.hide();
    }

    function dialogForgotPassword (ev){

        // You can do an API call here to send the form to your server
        // Show the sent data.. you can delete this safely.

        $mdDialog.show({
          controller         : function ($scope, $mdDialog)
          {
            $scope.dataDialog = {
              dialogContent: "Bạn vui lòng liên hệ với câu lạc bộ bạn đã đăng ký và trả lời một số câu hỏi liên quan đến tài khoản của bạn từ phía câu lạc bộ. Khi đã xác nhận thông tin là hoàn toàn chính xác, họ sẽ cấp lại mật khẩu mới cho bạn. Cám ơn.",
              dialogClose: "ĐÓNG"
            };
            $scope.closeDialog = function ()
            {
              $mdDialog.hide();
            }
          },
          templateUrl           :  'app/core/theme-options/login/dialog/dialogForgetPassword.html',
          parent             : angular.element('body'),
          targetEvent        : ev,
          clickOutsideToClose: false
        })
    }

  }


})();
