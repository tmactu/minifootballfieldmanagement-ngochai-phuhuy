/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filter.config;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

/**
 * Service return when error
 * @author Tran Ngoc Hai
 * @since 09/02/2017
 */
@Path("/error")
public class ErrorValue {
    @GET
    public String error(){
        return "fail";
    }
    @POST
    public String error1(){
        return "fail";
    }
    @PUT
    public String error2(){
        return "fail";
    }
    @DELETE
    public String error3(){
        return "fail";
    }
}
