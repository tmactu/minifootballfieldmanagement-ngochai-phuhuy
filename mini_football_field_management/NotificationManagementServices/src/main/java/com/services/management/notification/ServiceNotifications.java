package com.services.management.notification;

import com.exception.InvalidManagerException;
import com.exception.InvalidRequestException;
import com.exception.RequestNotFoundException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Notification;
import com.model.hibernate.POJO.User;
import com.model.hibernate.connect.HibernateUtil;
import com.services.management.notification.rest.restInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Web Service management notification
 *
 * @author Phu Huy
 * @since 26-02-2017
 */
@RestController
@RequestMapping(value = "/notification")
public class ServiceNotifications implements restInterface{
    
    /**
     * Service create new notification
     *
     * @param request
     * Data input include:
     *              - jwt
     *              - username: username of manager belong to club
     *              - club: id club
     *              - title
     *              - content
     *              - startDate
     * 
     * @return an instance of java.lang.String, return JSONArray result with content success.
     */
    @Override
    public String doPost(HttpServletRequest request) throws HibernateException, ParseException, 
                                                    NumberFormatException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        String title = request.getParameter("title");
        String content = request.getParameter("content");
        
        //Convert data input
        Date d_startDate = new Date();
        
        //Create ID notification
        int idNotification = HibernateUtil.createID(new Notification(), "idNotification");
                
        //Create transaction 
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                                                        
            Notification notification = new Notification();
                notification.setIdNotification(idNotification);
                notification.setClubs(new Clubs(Integer.parseInt(idClub)));
                notification.setTitle(title);
                notification.setContent(content);
                notification.setStartDate(d_startDate);
                
            //Save object
            session.save(notification);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        return SUCCESS.toString();
    }
    /**
     * Service update notification
     *
     * @param request
     * Data input include:
     *                  - jwt
     *                  - username: username of manager belong to club
     *                  - club: id club
     * @param body
     * Data input include:
     *                    - idNotification
     *                    - title
     *                    - content
     *                    - startDate
     * @return an instance of java.lang.String, return JSONArray result with content success.
     * @throws java.text.ParseException
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
           throws  HibernateException, NumberFormatException, ParseException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String idNotification = body.getFirst("idNotification");
        String title = body.getFirst("title");
        String content = body.getFirst("content");
        String startDate = body.getFirst("startDate");
        
        //Convert data input 
        Date d_startDate = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
        
        //Create transaction 
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
                
            //Load object by database
            Notification notification = (Notification) session.load(Notification.class, Integer.parseInt(idNotification));
                notification.setTitle(title);
                notification.setContent(content);
                notification.setStartDate(d_startDate);
            //Save object
            session.update(notification);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }
    /**
     * Service delete notification
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager belong to club
     *                    - club: id club
     * @param body
     * Data input include:
     *                    - idNotification
     * @throws java.text.ParseException
     * @throws com.exception.InvalidManagerException
     * @return an instance of java.lang.String, return JSONArray result with content success.
     */
    @Override
    public String doDelete(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
             throws HibernateException, ParseException, NumberFormatException, InvalidManagerException{
        
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");
        
        String idNotification = body.getFirst("idNotification");
        
        //Create transaction 
        session.beginTransaction();
        
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            //Load object by database
            Notification notification = (Notification) session.load(Notification.class, Integer.parseInt(idNotification));
            
            //Save object
            session.delete(notification);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return SUCCESS.toString();
    }

    @Override
    public String doGet(HttpServletRequest request) throws InvalidRequestException, ParseException, InvalidManagerException, RequestNotFoundException {
        //Get data input
        String username = request.getHeader("username");
        String idClub = request.getHeader("club");        
        //Create transaction 
        session.beginTransaction();
        JSONArray _result  = new JSONArray();
        try{
            //Checking username of manager which belong to club or not 
            User manager = (User) session.get(User.class, username);
                if(manager.getClubs().getIdClub() != Integer.parseInt(idClub))
                    throw new InvalidManagerException("Account manager not belong to club");
            
            List<Notification> cri = session.createQuery("from Notification where clubs=" + Integer.parseInt(idClub) + " order by startDate desc").list();
                   System.out.println("select * from notification where ID_CLUB=" + Integer.parseInt(idClub) + " order by start_date desc");
            for(int i = 0; i < cri.size(); i++){
                Notification noti = cri.get(i);
                JSONObject result = new JSONObject();
                    result.put("title", noti.getTitle());
                    result.put("content", noti.getContent());
                    result.put("startDate", noti.getStartDate());
                _result.put(result);
            }
         }
        finally{
            //Close transaction
            session.getTransaction().commit();
            session.clear();
        }
        
        return _result.toString();
    }


}
