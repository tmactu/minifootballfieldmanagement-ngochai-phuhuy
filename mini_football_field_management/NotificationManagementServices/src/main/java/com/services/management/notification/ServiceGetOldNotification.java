
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.notification;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Notification;
import com.services.management.notification.rest.restInterface;
import static com.services.management.notification.rest.restInterface.session;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @sinceL 19-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/notification/getOldNotification")
public class ServiceGetOldNotification implements restInterface{
    /**
     * Service get old notification
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager belong to club
     *                    - club: id club
     * @return an instance of java.lang.String, result return JSONArray with content is information
     * of notification
     * @throws java.text.ParseException
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, ParseException, NumberFormatException{
               
        //Get data input 
        String idClub = request.getParameter("club");
        int allCount = Integer.parseInt(request.getParameter("allCount"));
        int countLast = Integer.parseInt(request.getParameter("countLast"));
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
        JSONObject ob = new JSONObject();
        
        //Create transaction 
        session.beginTransaction();
        
        try{    
            Date dateEnd = new Date();
            dateEnd.setDate(dateEnd.getDate() + 1); 
            Date dateStart = new Date();
            Calendar calendar = Calendar.getInstance(); 
            calendar.setTime(dateStart); 
            calendar.add(Calendar.DATE, -30);
            dateStart = calendar.getTime();
            
            //Create criteria
            Criteria cr = session.createCriteria(Notification.class);
                if((allCount - countLast) == 1){
                    ob.put("disabledOld", true);
                    ob.put("countLast", allCount);
                    cr.add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                    .add(Restrictions.between("startDate", dateStart, dateEnd))        
                    .addOrder(Order.desc("startDate"))
                    .setFirstResult(countLast)
                    .setMaxResults(allCount - countLast);
                }
                else if((allCount - countLast) > 7){
                    ob.put("disabledOld", false);
                    ob.put("countLast", countLast + 7);
                    cr.add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                    .add(Restrictions.between("startDate", dateStart, dateEnd))       
                    .addOrder(Order.desc("startDate"))
                    .setFirstResult(countLast + 1)
                    .setMaxResults(7);
                }
                else{
                    ob.put("disabledOld", true);
                    ob.put("countLast", allCount);
                    cr.add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                    .add(Restrictions.between("startDate", dateStart, dateEnd))        
                    .addOrder(Order.desc("startDate"))
                    .setFirstResult(countLast + 1)
                    .setMaxResults(allCount - countLast);
                }
            List listNotification = cr.list();
            Iterator iterator = listNotification.iterator();
            while(iterator.hasNext()){
                Notification notification = (Notification) iterator.next();
                JSONObject result = new JSONObject();
                    result.put("title", notification.getTitle());
                    result.put("content", notification.getContent());
                    result.put("startDate", new SimpleDateFormat("yyyy/MM/dd").format(notification.getStartDate()));
                 
                arr_listResult.put(result);
            }
            ob.put("listNotification", arr_listResult);
            ob.put("allCount", allCount);
            ob.put("countFirst", countLast + 1);
            ob.put("disabledNew", false);
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return ob.toString();
    }
}
