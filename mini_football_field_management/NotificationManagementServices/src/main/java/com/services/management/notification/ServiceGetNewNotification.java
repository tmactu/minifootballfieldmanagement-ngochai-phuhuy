/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services.management.notification;

import com.exception.InvalidManagerException;
import com.model.hibernate.POJO.Notification;
import com.services.management.notification.rest.restInterface;
import static com.services.management.notification.rest.restInterface.session;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phu Huy
 * @sinceL 19-04-2017
 * 
 */
@RestController
@RequestMapping(value = "/notification/getNewNotification")
public class ServiceGetNewNotification implements restInterface{
    /**
     * Service get information notification
     *
     * @param request
     * Data input include:
     *                    - jwt
     *                    - username: username of manager belong to club
     *                    - club: id club
     * @return an instance of java.lang.String, result return JSONArray with content is information
     * of notification
     * @throws java.text.ParseException
     * @throws com.exception.InvalidManagerException
     */
    @Override
    public String doGet(HttpServletRequest request) throws HibernateException, ParseException, NumberFormatException{
               
        //Get data input 
        String idClub = request.getParameter("club");
        int allCount = Integer.parseInt(request.getParameter("allCount"));
        int countFirst = Integer.parseInt(request.getParameter("countFirst"));
        
        //Create JSONArray to return
        JSONArray arr_listResult = new JSONArray();
        JSONObject ob = new JSONObject();
        
        
        //Create transaction 
        session.beginTransaction();
        
        try{ 
            Date dateEnd = new Date();
            dateEnd.setDate(dateEnd.getDate() + 1); 
            Date dateStart = new Date();
            Calendar calendar = Calendar.getInstance(); 
            calendar.setTime(dateStart); 
            calendar.add(Calendar.DATE, -30);
            dateStart = calendar.getTime();
            
            if((countFirst - 7) == 1){
                ob.put("disabledNew", true);
            }
            else{
                ob.put("disabledNew", false);
            }      
            //Create criteria
            Criteria cr = session.createCriteria(Notification.class)
                    .add(Restrictions.eq("clubs.idClub", Integer.parseInt(idClub)))
                    .add(Restrictions.between("startDate", dateStart, dateEnd))
                    .addOrder(Order.desc("startDate"))
                    //Using like TOP in sql
                    .setFirstResult(countFirst - 7)
                    .setMaxResults(7);
                
            List listNotification = cr.list();
            Iterator iterator = listNotification.iterator();
            while(iterator.hasNext()){
                Notification notification = (Notification) iterator.next();
                JSONObject result = new JSONObject();
                    result.put("title", notification.getTitle());
                    result.put("content", notification.getContent());
                    result.put("startDate", new SimpleDateFormat("yyyy/MM/dd").format(notification.getStartDate()));
                 
                arr_listResult.put(result);
            }
            ob.put("listNotification", arr_listResult);
            ob.put("allCount", allCount);
            ob.put("countFirst", countFirst - 7);
            ob.put("countLast", countFirst - 1);
            ob.put("disabledOld", false);           
                  
        }
        finally{
            //Close transaction
            session.getTransaction().commit();
        }
        
        return ob.toString();
    }
}
