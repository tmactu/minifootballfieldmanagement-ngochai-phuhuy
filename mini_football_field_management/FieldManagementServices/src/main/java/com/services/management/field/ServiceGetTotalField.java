package com.services.management.field;

import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.TypeOfField;
import com.services.management.field.rest.RestInterface;

/**
 * REST Web Service management field
 *
 * @author Tran Ngoc Hai
 * @version 1.0
 * @since 2017-01-20
 */
@RestController
@RequestMapping("/total")
public class ServiceGetTotalField implements RestInterface{
	 /**
     * Service find fields of club.
     *
     * @param request
     * @return json success.
     * @throws java.text.ParseException
     */
    @Override
    public String doGet(HttpServletRequest request)
            throws ParseException, HibernateException, NumberFormatException {        
        //Json object contain result.
        JSONObject ob = new JSONObject();
        //Begin transaction
        session.getTransaction().begin();
        try {
            //Get information of fields by id club and type of field.
            Criteria criteria = session.createCriteria(Fields.class);
            criteria.setProjection(Projections.rowCount());
            Long result = (Long)criteria.uniqueResult();
            
            ob.put("result", result);
        } finally {
            //Commit transaction to save config
            session.getTransaction().commit();
        }

        //returns successfully string if the field is saved
        return ob.toString();
    }
}
