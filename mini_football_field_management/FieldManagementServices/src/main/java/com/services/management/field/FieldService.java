package com.services.management.field;

import com.exception.InvalidRequestException;
import com.model.hibernate.POJO.Clubs;
import com.model.hibernate.POJO.Fields;
import com.model.hibernate.POJO.Reservation;
import com.model.hibernate.POJO.TypeOfField;
import com.model.hibernate.connect.HibernateUtil;
import com.services.management.field.rest.RestInterface;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Web Service management account
 *
 * @author Tran Ngoc Hai
 * @version 1.0
 * @since 2017-01-20
 */
@RestController
@RequestMapping("/")
public class FieldService implements RestInterface {

    /**
     * Service create new field.
     *
     * @param request
     * @return json success.
     * @throws java.text.ParseException
     */

    @Override
    public String doPost(HttpServletRequest request)
            throws ParseException, HibernateException, NumberFormatException {
        int id = HibernateUtil.createID(new Fields(), "idField");
        int idOfClub = Integer.parseInt(request.getHeader("club"));
        String type = request.getParameter("typeOfField");
        //Begin transaction
        session.getTransaction().begin();
        try {
            Query query = session.createQuery("from Fields where typeOfField = '" + type + "'");
            query.setMaxResults(1);
            Fields price = (Fields) query.uniqueResult();
            //Create new field
            Fields field = new Fields();
            field.setIdField(id);
            field.setClubs(new Clubs(idOfClub));
            field.setTypeOfField((TypeOfField) session.load(TypeOfField.class, type));
            field.setPrice(price.getPrice());
            session.save(field);
        } finally {
            //Commit transaction to save config
            session.getTransaction().commit();
        }
        //returns successfully string if the field is saved
        return SUCCESS;
    }

    /**
     * Service delete field.
     *
     * @param body
     * @param request
     * @return json success.
     * @throws com.exception.InvalidRequestException
     */
    @Override
    public String doDelete(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request)
            throws InvalidRequestException, HibernateException, NumberFormatException {
        //Get id field need delete.
        int idOfField = Integer.parseInt(body.getFirst("id"));
        //Begin transaction
        session.getTransaction().begin();
        try {
            //Get information field from database.
            Fields field = (Fields) session.load(Fields.class, idOfField);
            //Get information default field from database.
            Fields defaultField = (Fields) session.load(Fields.class, 0);
            //Get reservations of field and change field in each reservation to default field.
            Iterator<Reservation> reservations = field.getReservations().iterator();
            while (reservations.hasNext()) {
                Reservation reservation = reservations.next();
                reservation.setFields(defaultField);
                session.update(reservation);
            }
            //Delete field
            session.delete(field);
        } finally {
            //Commit transaction to save config
            session.getTransaction().commit();
        }
        //returns successfully string if the field is saved
        return SUCCESS;
    }

    /**
     * Service find fields of club.
     *
     * @param request
     * @return json success.
     * @throws java.text.ParseException
     */
    @Override
    public String doGet(HttpServletRequest request)
            throws ParseException, HibernateException, NumberFormatException {
        //Get id club and type of field (if exists) need find .
        int idOfClub = Integer.parseInt(request.getHeader("club"));
        String type = request.getParameter("typeOfField");
        //Json object contain result.
        JSONArray arr_resultList = new JSONArray();
        //Begin transaction
        session.getTransaction().begin();
        try {
            //Get information of fields by id club and type of field.
            Criteria criteria = session.createCriteria(Fields.class);
            criteria.add(Restrictions.eq("clubs", new Clubs(idOfClub)));
            if (type != null) {
                criteria.add(Restrictions.eq("typeOfField", new TypeOfField(type)));
            }
            List<Fields> fields = criteria.list();
            Iterator<Fields> field = fields.iterator();
            //Contain information of fields into json.       
            while (field.hasNext()) {
                JSONObject ob = new JSONObject();
                Fields temp = field.next();
                ob.put("idField", temp.getIdField());
                ob.put("clubs", temp.getClubs().getIdClub());
                ob.put("price", temp.getPrice());
                ob.put("typeOfField", temp.getTypeOfField().getTypeOfField());
                arr_resultList.put(ob);
            }
        } finally {
            //Commit transaction to save config
            session.getTransaction().commit();
        }

        //returns successfully string if the field is saved
        return arr_resultList.toString();
    }

    @Override
    public String doPut(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request) throws InvalidRequestException, NumberFormatException, HibernateException {
        //Get id field need delete.
        int price = Integer.parseInt(body.getFirst("price"));
        int club = Integer.parseInt(request.getHeader("club"));
        String type = body.getFirst("typeOfField");
        //Begin transaction
        session.beginTransaction();
        try {
            String hql = "update Fields set price = " + price + " where type_of_field = '" + type + "' and id_club = " + club;
            Query query = session.createSQLQuery(hql);
            int result = query.executeUpdate();
            System.out.println(result);
        } finally {
            //Commit transaction to save config
            session.getTransaction().commit();
            session.clear();
        }
        //returns successfully string if the field is saved
        return SUCCESS;
    }
    
    

}
