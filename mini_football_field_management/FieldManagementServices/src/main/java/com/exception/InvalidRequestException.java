/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exception;

/**
 *
 * @author Hai
 */
public class InvalidRequestException extends Exception{
    private final String errText;

    public String getErrText() {
        return errText;
    }

    public InvalidRequestException(String errText) {
        this.errText = errText;
    }
    
}
