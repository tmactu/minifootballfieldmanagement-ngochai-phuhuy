(function(app) {
	app.controller('AboutController', ['$scope', '$cookieStore', '$mdDialog', '$http', function($scope, $cookieStore, $mdDialog, $http ) {
               
                $scope.filePath = false;
                $scope.file = [];
                $scope.backup = function(){
                    $http({
                    method: 'POST',
                    data : {},
                    url: 'http://localhost:8013/backup'
                }).then(function successCallback(response) {
                    $scope.filePath = true;
                   
                }, function errorCallback(response) {
                   $scope.filePath = true;
                });
                };
                
                $scope.upload = function(){
                    $http({
                    method: 'POST',
                    data : {
                        'file' : $scope.file
                    },
                    headers : {
                        'Content-Type': 'multipart/form-data'
                    },
                    url: 'http://localhost:8013/restore'
                }).then(function successCallback(response) {
                   
                }, function errorCallback(response) {
                    $scope.alertDialog('Lỗi', 'Kết nối máy chủ gặp sự cố');
                });
                };
                
                $scope.alertDialog = function (title, content) {
                $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title(title)
                        .textContent(content)
                        .ok('Đóng')
                        );
            };
                
        }]);
})(adminUI);
