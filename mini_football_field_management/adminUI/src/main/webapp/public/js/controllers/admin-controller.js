(function(app) {
	app.controller('adminController', ['$scope', '$http', '$window', '$cookieStore', function($scope, $http, $window, $cookieStore) {
           
            $scope.checkForLogin = function () {
                $http({
                    method: 'GET',
                    url: 'http://localhost:8010/account/checkAdmin'
                }).then(function successCallback(response) {
                    $scope.isLogin = true;
                }
                , function errorCallback(response) {
                   $window.location.href = 'http://localhost:8000/main';
                });
               
            };
            
            $scope.logout = function(){
                $cookieStore.remove('jwt');
                $cookieStore.remove('username');
                $cookieStore.remove('club');
                $window.location.href = 'http://localhost:8000/main';
            };
	}]);
})(adminUI);
