(function (app) {
    app.controller('HomeController', ['$http', '$mdEditDialog', '$window', '$mdDialog', '$q', '$timeout', '$scope', 'base64', function ($http, $mdEditDialog, $window, $mdDialog, $q, $timeout, $scope, base64) {
            
            $scope.parameter = '';
            $scope.options = {
                rowSelection: true,
                multiSelect: false,
                autoSelect: true,
                decapitate: false,
                largeEditDialog: false,
                boundaryLinks: false,
                limitSelect: true,
                pageSelect: true
            };
            $scope.adminData = [];
            $scope.alertDialog = function (title, content) {
                $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title(title)
                        .textContent(content)
                        .ok('Đóng')
                        );
            };
            $scope.currentNavItem = 'home';
            $scope.selected = [];
            $scope.find = find;
            $scope.deleteClub = deleteClub;
            $scope.limitOptions = [5, 10, 15, {
                    label: 'All',
                    value: function () {
                        return $scope.desserts ? $scope.desserts.count : 0;
                    }
                }];

            $scope.query = {
                order: 'name',
                limit: 5,
                page: 1
            };

            // for testing ngRepeat
            $scope.columns = [{
                    name: 'Dessert',
                    orderBy: 'name',
                    unit: '100g serving'
                }, {
                    descendFirst: true,
                    name: 'Type',
                    orderBy: 'type'
                }, {
                    name: 'Calories',
                    numeric: true,
                    orderBy: 'calories.value'
                }, {
                    name: 'Fat',
                    numeric: true,
                    orderBy: 'fat.value',
                    unit: 'g'
                }, /* {
                 name: 'Carbs',
                 numeric: true,
                 orderBy: 'carbs.value',
                 unit: 'g'
                 }, */ {
                    name: 'Protein',
                    numeric: true,
                    orderBy: 'protein.value',
                    trim: true,
                    unit: 'g'
                }, /* {
                 name: 'Sodium',
                 numeric: true,
                 orderBy: 'sodium.value',
                 unit: 'mg'
                 }, {
                 name: 'Calcium',
                 numeric: true,
                 orderBy: 'calcium.value',
                 unit: '%'
                 }, */ {
                    name: 'Iron',
                    numeric: true,
                    orderBy: 'iron.value',
                    unit: '%'
                }, {
                    name: 'Comments',
                    orderBy: 'comment'
                }];

            function find() {
                var parameter = base64.encode($scope.parameter);
                $http({
                    method: 'GET',
                    url: 'http://localhost:8015/club?parameter=' + parameter
                }).then(function successCallback(response) {
                    $scope.desserts = response.data;
                    if ($scope.desserts.length == 0)
                        $scope.noData = 'Không tìm thấy kết quả phù hợp';
                    else {
                        $scope.noData = '';
                    }
                }, function errorCallback(response) {
                    $scope.alertDialog('Lỗi', 'Kết nối máy chủ gặp sự cố');
                });
            }
            $scope.editComment = function (event, dessert) {
                event.stopPropagation();

                var dialog = {
                    // messages: {
                    //   test: 'I don\'t like tests!'
                    // },
                    modelValue: dessert.comment,
                    placeholder: 'Add a comment',
                    save: function (input) {
                        dessert.comment = input.$modelValue;
                    },
                    targetEvent: event,
                    title: 'Add a comment',
                    validators: {
                        'md-maxlength': 30
                    }
                };

                var promise = $scope.options.largeEditDialog ? $mdEditDialog.large(dialog) : $mdEditDialog.small(dialog);

                promise.then(function (ctrl) {
                    var input = ctrl.getInput();

                    input.$viewChangeListeners.push(function () {
                        input.$setValidity('test', input.$modelValue !== 'test');
                    });
                });
            };

            $scope.toggleLimitOptions = function () {
                $scope.limitOptions = $scope.limitOptions ? undefined : [5, 10, 15];
            };

            $scope.getTypes = function () {
                return ['Candy', 'Ice cream', 'Other', 'Pastry'];
            };

            $scope.onPaginate = function (page, limit) {
                console.log('Scope Page: ' + $scope.query.page + ' Scope Limit: ' + $scope.query.limit);
                console.log('Page: ' + page + ' Limit: ' + limit);

                $scope.promise = $timeout(function () {

                }, 2000);
            };

            $scope.deselect = function (item) {
                console.log(item.name, 'was deselected');
            };

            $scope.log = function (item) {
                console.log(item.name, 'was selected');
            };

            $scope.loadStuff = function () {
                $scope.promise = $timeout(function () {
                    $scope.find();
                }, 2000);
            };

            $scope.onReorder = function (order) {

                console.log('Scope Order: ' + $scope.query.order);
                console.log('Order: ' + order);

                $scope.promise = $timeout(function () {

                }, 2000);
            };

            $scope.confirmDialog = function (title, content) {
                return $mdDialog.confirm().title(title)
                        .textContent(content)
                        .ok('OK')
                        .cancel('Thoát');
            };

            function deleteFunc(id) {
                $.ajax({
                    type: 'DELETE',
                    data: {
                        idClub: id
                    },
                    contentType: 'application/x-www-form-urlencoded',
                    url: "http://localhost:8015/club",
                    success: function (result) {
                        $scope.selected = [];
                        $scope.find();
                        $scope.alertDialog('Thông báo', 'Xóa câu lạc bộ thành công');
                        return true;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $scope.alertDialog('Lỗi', 'Kết nối thất bại, không thể xóa câu lạc bộ');
                        return false;
                    }});
            }

            function deleteClub() {
                for (var i = 0; i < $scope.selected.length; i++) {
                    $mdDialog.show($scope.confirmDialog('Bạn có chắc muốn xóa?', 'Câu lạc bộ ' + $scope.selected[i].name)).then(function () {
                        var id = $scope.selected[i - 1].idClub;
                        $scope.wait = deleteFunc(id);
                        while ($scope.wait)
                            ;
                    }, function () {
                    });
                }
            }
            ;

            $scope.updateClub = function (ev) {

                $http({
                    method: 'GET',
                    url: 'http://localhost:8010/admin?club=' + $scope.selected[0].idClub
                }).then(function successCallback(response) {
                    $scope.adminData = response.data;

                    $mdDialog.show({
                        locals: {clubData: $scope.selected, adminData: $scope.adminData},
                        controller: UpdateDialogController,
                        templateUrl: 'partials/update-club-dialog.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    });
                }, function errorCallback(response) {
                    $scope.alertDialog('Lỗi', 'Kết nối máy chủ gặp sự cố');
                });



            };

            function UpdateDialogController($scope, $mdDialog, clubData, adminData) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function () {
                    $scope.clubInfor.province = $scope.clubInfor.provinceID;
                    $.ajax({
                        type: 'PUT',
                        data: $scope.clubInfor,
                        contentType: 'application/x-www-form-urlencoded',
                        url: "http://localhost:8015/club/update",
                        success: function (result) {

                            $scope.newInfor.birthdate = $scope.newInfor.birthdate.getDate() + '/' +
                                    $scope.newInfor.birthdate.getMonth() + '/' + $scope.newInfor.birthdate.getFullYear();

                            $.ajax({
                                type: 'PUT',
                                data: $scope.newInfor,
                                contentType: 'application/x-www-form-urlencoded',
                                url: "http://localhost:8010/manager",
                                success: function (result) {
                                    $scope.newInfor = [];
                                    $scope.newInfor.birthdate = new Date();
                                    $scope.clubInfor = [];
                                    $scope.alertDialog('Thành công', 'Cập nhật câu lạc bộ và quản lý.');
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $scope.alertDialog('Lỗi', 'Cập nhật quản lý thất bại');
                                }});

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $scope.alertDialog('Lỗi', 'Cập nhật câu lạc bộ thất bại');
                        }});


                };

                $scope.newInfor = adminData[0];
                $scope.newInfor.gender = ('$scope.newInfor.gender' == 'true') ? 'Nam' : 'Nu';
                $scope.newInfor.id = Number($scope.newInfor.id);
                $scope.newInfor.birthdate = new Date($scope.newInfor.birthdate);
                $scope.clubInfor = clubData[0];
                $scope.alertDialog = function (title, content) {
                    $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title(title)
                            .textContent(content)
                            .ok('Đóng')
                            );
                };
                $scope.checkUsernameExist = function () {
                    $scope.showAccountUsed = false;
                    $scope.showErrorIsExist = false;
                    $scope.showErrorBlank = false;
                    if ($scope.newInfor.username.search(' ') == -1) {
                        $http({
                            method: 'GET',
                            url: 'http://localhost:8010/account?username=' + $scope.newInfor.username
                        }).then(function successCallback(response) {
                            if (response.data.result == 'true') {
                                $scope.showAccountUsed = true;
                            } else {
                                $scope.showErrorIsExist = true;
                            }
                        }, function errorCallback(response) {
                            $scope.alertDialog('Lỗi', 'Không thể kết nối đến máy chủ');
                        });
                    } else {
                        $scope.showErrorBlank = true;
                    }
                }

                $scope.loadProvince = function () {
                    $http({
                        method: 'GET',
                        url: 'http://localhost:8013/rest/provinces'
                    }).then(function successCallback(response) {
                        $scope.provinces = response.data;
                    }, function errorCallback(response) {
                        $scope.alertDialog('Lỗi', 'Không thể kết nối đến máy chủ');
                    });
                }
            }


            $scope.showAdvanced = function (ev) {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'partials/new-club-dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                        .then(function (answer) {
                            $scope.status = 'You said the information was "' + answer + '".';
                        }, function () {
                            $scope.status = 'You cancelled the dialog.';
                        });
            };

            function DialogController($scope, $mdDialog) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function () {

                    $.ajax({
                        type: 'POST',
                        data: $scope.clubInfor,
                        contentType: 'application/x-www-form-urlencoded',
                        url: "http://localhost:8015/club",
                        success: function (result) {

                            $scope.newInfor.birthdate = $scope.newInfor.birthdate.getDate() + '/' +
                                    $scope.newInfor.birthdate.getMonth() + '/' + $scope.newInfor.birthdate.getFullYear();
                            $scope.newInfor.club = result;
                            $.ajax({
                                type: 'POST',
                                data: $scope.newInfor,
                                contentType: 'application/x-www-form-urlencoded',
                                url: "http://localhost:8010/manager",
                                success: function (result) {
                                    $scope.newInfor = [];
                                    $scope.newInfor.birthdate = new Date();
                                    $scope.clubInfor = [];
                                    $scope.alertDialog('Thành công', 'Đã thêm câu lạc bộ và quản lý.');
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $scope.alertDialog('Lỗi', 'Thêm quản lý thất bại');
                                }});

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $scope.alertDialog('Lỗi', 'Thêm câu lạc bộ thất bại');
                        }});


                };

                $scope.newInfor = {
                    username: '',
                    name: '',
                    typeOfMember: '',
                    numberPhone: '',
                    birthdate: new Date(),
                    password: '',
                    repassword: '',
                    id: '',
                    gender: 'Nam',
                    address: '',
                    email: '',
                    club: ''
                };

                $scope.clubInfor = {
                    name: '',
                    address: '',
                    province: ''
                }
                $scope.alertDialog = function (title, content) {
                    $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title(title)
                            .textContent(content)
                            .ok('Đóng')
                            );
                };
                $scope.checkUsernameExist = function () {
                    $scope.showAccountUsed = false;
                    $scope.showErrorIsExist = false;
                    $scope.showErrorBlank = false;
                    if ($scope.newInfor.username.search(' ') == -1) {
                        $http({
                            method: 'GET',
                            url: 'http://localhost:8010/account?username=' + $scope.newInfor.username
                        }).then(function successCallback(response) {
                            if (response.data.result == 'true') {
                                $scope.showAccountUsed = true;
                            } else {
                                $scope.showErrorIsExist = true;
                            }
                        }, function errorCallback(response) {
                            $scope.alertDialog('Lỗi', 'Không thể kết nối đến máy chủ');
                        });
                    } else {
                        $scope.showErrorBlank = true;
                    }
                }

                $scope.loadProvince = function () {
                    $http({
                        method: 'GET',
                        url: 'http://localhost:8013/rest/provinces'
                    }).then(function successCallback(response) {
                        $scope.provinces = response.data;
                    }, function errorCallback(response) {
                        $scope.alertDialog('Lỗi', 'Không thể kết nối đến máy chủ');
                    });
                }
            }

        }]);
})(adminUI);
