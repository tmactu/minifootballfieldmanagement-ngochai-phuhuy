var adminUI = angular.module('adminUI', ['ngMaterial', 'ngAnimate', 'ngMessages', 'ngAria','ngCookies', 'ui.router', 'md.data.table', 'ab-base64']);

(function(app) {
    app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider.state('admin', {
		url: '/admin',
		templateUrl: 'partials/admin-partial.html',
		controller: 'adminController'
	})

	.state('home', {
            url: '/',
            templateUrl: 'partials/home-partial.html',
            controller: 'HomeController'
        })

        .state('about', {
            url: '/about',
            templateUrl: 'partials/about-partial.html',
            controller: 'AboutController'
        });
    }]);

    app.config(['$httpProvider',run]);
        function run($httpProvider) {
            var cookieStore;
            angular.injector(['ngCookies']).invoke(['$cookieStore', function(_$cookieStore_) {
              cookieStore = _$cookieStore_;
              }]);
            $httpProvider.defaults.headers.common = {
                'jwt': cookieStore.get('jwt'),
                'username': cookieStore.get('username'),
                'club': cookieStore.get('club'),
                'Content-Type': 'application/x-www-form-urlencoded'
            };
            $.ajaxSetup({
                beforeSend(xhr){
                    xhr.setRequestHeader('jwt', cookieStore.get('jwt'));
                    xhr.setRequestHeader('username', cookieStore.get('username'));
                    xhr.setRequestHeader('club', cookieStore.get('club'));
                }
            });
        }
})(adminUI);
