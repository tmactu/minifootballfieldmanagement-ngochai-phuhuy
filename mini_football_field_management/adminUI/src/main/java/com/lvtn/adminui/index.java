package com.lvtn.adminui;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Tran Ngoc Hai
 * Controller start page  index.html
 */
@Controller
public class index{
    @RequestMapping("/")
    public String index(Model model){
        return "/dev/index.html";
    }
}
